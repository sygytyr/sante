-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: hs211547.mysql.ukraine.com.ua
-- Время создания: Сен 04 2019 г., 16:19
-- Версия сервера: 5.7.16-10-log
-- Версия PHP: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `hs211547_sante`
--

-- --------------------------------------------------------

--
-- Структура таблицы `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('AdminAccess', '1', 1558786363);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('/*', 2, NULL, NULL, NULL, 1558786363, 1558786363),
('AdminAccess', 1, NULL, NULL, NULL, 1558786363, 1558786363);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('AdminAccess', '/*');

-- --------------------------------------------------------

--
-- Структура таблицы `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `figure_type` int(11) DEFAULT NULL,
  `fon_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `series_id` int(11) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `banner`
--

INSERT INTO `banner` (`id`, `image`, `figure_type`, `fon_color`, `series_id`, `position`, `published`, `created_at`, `updated_at`) VALUES
(1, '/admin/uploads/mrp_a.jpg', 1, '1', 15, 5, 1, 1558882526, 1561628158),
(2, '/admin/uploads/mrp_b.png', 2, '2', 7, 0, 1, 1558882547, 1561628177),
(3, '/admin/uploads/mrp_c.png', 3, '3', 13, 0, 1, 1558882569, 1561628190),
(4, '/admin/uploads/mrp_d.jpg', 4, '4', 4, 0, 1, 1558882591, 1561628200),
(5, '/admin/uploads/mrp_e.jpg', 5, '5', 11, 0, 1, 1558882606, 1561628214);

-- --------------------------------------------------------

--
-- Структура таблицы `banner_lang`
--

CREATE TABLE `banner_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `banner_lang`
--

INSERT INTO `banner_lang` (`model_id`, `language`, `label`) VALUES
(1, 'en', 'Dr. Santé 0%'),
(1, 'ru', 'Dr. Santé 0%'),
(1, 'ua', 'Dr. Santé 0%'),
(2, 'en', 'Cucumber<br> Balance Control'),
(2, 'ru', 'Cucumber<br> Balance Control'),
(2, 'ua', 'Cucumber<br> Balance Control'),
(3, 'en', 'Cannabis Hair<br> Oil Therapy'),
(3, 'ru', 'Cannabis Hair<br> Oil Therapy'),
(3, 'ua', 'Cannabis Hair<br> Oil Therapy'),
(4, 'en', 'Detox Hair'),
(4, 'ru', 'Detox Hair'),
(4, 'ua', 'Detox Hair'),
(5, 'en', 'Aqua Thermal'),
(5, 'ru', 'Aqua Thermal'),
(5, 'ua', 'Aqua Thermal');

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `fon_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `fon_color`, `alias`, `position`, `published`, `created_at`, `updated_at`) VALUES
(3, '#ffffff', 'lico', 3, 1, 1558801052, 1561529421),
(4, '#ffffff', 'volosy', 3, 1, 1558801068, 1560409235),
(5, '#ffffff', 'telo', 2, 1, 1558801079, 1560409244),
(6, '#ffffff', 'dlya-detey', 0, 1, 1558801091, 1560409253);

-- --------------------------------------------------------

--
-- Структура таблицы `category_lang`
--

CREATE TABLE `category_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8_unicode_ci,
  `seo_keywords` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `category_lang`
--

INSERT INTO `category_lang` (`model_id`, `language`, `label`, `content`, `seo_title`, `seo_description`, `seo_keywords`) VALUES
(3, 'ru', 'Лицо', '', '', '', ''),
(4, 'ru', 'Волосы', '', 'Уход за волосами', '', ''),
(5, 'ru', 'Тело', '', 'Уход за телом', '', ''),
(6, 'ru', 'Для детей', '', 'Для детей', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `imagemanager`
--

CREATE TABLE `imagemanager` (
  `id` int(10) UNSIGNED NOT NULL,
  `fileName` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `fileHash` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  `createdBy` int(10) UNSIGNED DEFAULT NULL,
  `modifiedBy` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `imagemanager`
--

INSERT INTO `imagemanager` (`id`, `fileName`, `fileHash`, `created`, `modified`, `createdBy`, `modifiedBy`) VALUES
(1, 'hends-1280.jpg', 'dqYZnPytbFpLbPsSi1IhNMhQdp4kUQh2', '2019-07-01 10:32:19', '2019-07-01 10:32:19', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `influence`
--

CREATE TABLE `influence` (
  `id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `influence`
--

INSERT INTO `influence` (`id`, `position`, `published`, `created_at`, `alias`, `updated_at`) VALUES
(6, 0, 1, 1558805411, 'tonizaciya', 1558805411),
(7, 0, 1, 1558805417, 'matirovanie', 1558805417),
(8, 0, 1, 1558805422, 'vosstanovlenie', 1558805422),
(9, 0, 1, 1558805426, 'demakiyazh', 1558805426),
(10, 0, 1, 1558805431, 'ochischenie', 1558805431),
(11, 0, 1, 1558805435, 'pitanie', 1558805435),
(12, 0, 1, 1558805440, 'uvlazhnenie', 1558805440),
(13, 0, 1, 1558805444, 'antivozrastnoy', 1558805444),
(14, 0, 1, 1561540384, 'vid-vipadinnya', 1561540402),
(17, 0, 1, 1561540536, 'ot-sekuschihsya-konchikov', 1561540536),
(18, 0, 1, 1561540562, 'blesk', 1561540562),
(19, 0, 1, 1561540572, 'obem', 1561540572),
(20, 0, 1, 1561540580, 'rost', 1561540580),
(21, 0, 1, 1561540593, 'zaschita-cveta', 1561540593),
(22, 0, 1, 1561540613, 'ochischenie-22', 1561540613),
(23, 0, 1, 1561540623, 'razglazhivanie', 1561540623),
(24, 0, 1, 1561540639, 'smyagchenie', 1561540639),
(25, 0, 1, 1561540653, 'stayling', 1561540653),
(26, 0, 1, 1561540663, 'ukreplenie', 1561540663),
(27, 0, 1, 1561540677, 'antivozrastnoy-27', 1561540677);

-- --------------------------------------------------------

--
-- Структура таблицы `influence_lang`
--

CREATE TABLE `influence_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `influence_lang`
--

INSERT INTO `influence_lang` (`model_id`, `language`, `label`) VALUES
(6, 'ru', 'Тонизация'),
(7, 'ru', 'Матирование'),
(8, 'ru', 'Восстановление'),
(9, 'ru', 'Демакияж'),
(10, 'ru', 'Очищение'),
(11, 'ru', 'Питание'),
(12, 'ru', 'Увлажнение'),
(13, 'ru', 'Антивозрастной'),
(14, 'ru', 'От выпадания'),
(14, 'ua', 'Від випадіння'),
(17, 'ru', 'От секущихся кончиков'),
(18, 'ru', 'Блеск'),
(19, 'ru', 'Объем'),
(20, 'ru', 'Рост'),
(21, 'ru', 'Защита цвета'),
(22, 'ru', 'Очищение'),
(23, 'ru', 'Разглаживание'),
(24, 'ru', 'Смягчение'),
(25, 'ru', 'Стайлинг'),
(26, 'ru', 'Укрепление'),
(27, 'ru', 'Антивозрастной');

-- --------------------------------------------------------

--
-- Структура таблицы `ingredient`
--

CREATE TABLE `ingredient` (
  `id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL,
  `fon_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `position_home` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) DEFAULT '1',
  `show_on_home` tinyint(1) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `ingredient`
--

INSERT INTO `ingredient` (`id`, `image`, `template_id`, `fon_color`, `color`, `position`, `position_home`, `published`, `show_on_home`, `created_at`, `updated_at`) VALUES
(3, '/admin/uploads/coconut.png', 1, '#E5DEDE', '#C4A0A0', 10, 10, 1, 1, 1558809293, 1561029092),
(5, '/admin/uploads/olive_a.png', 3, '#DEE5DE', '#A3C1A3', 0, 8, 0, 1, 1558809449, 1560410379),
(6, '/admin/uploads/aloevera.png', 2, '#D3E6CA', '#9BB78E', 7, 7, 1, 1, 1558809503, 1560958461),
(7, '/admin/uploads/cucumber.png', 1, '#D2E7B4', '#A1B782', 6, 6, 1, 1, 1558809558, 1560958847),
(8, '/admin/uploads/macadamia.png', 3, '#F8EEDE', '#D6B683', 11, 5, 1, 1, 1558809592, 1561443907),
(9, '/admin/uploads/goji.png', 2, '#FFE0E0', '#CAA2A2', 4, 4, 1, 1, 1558809634, 1560958877),
(10, '/admin/uploads/avocado.png', 1, '#D3E6CA', '#8AA47D', 3, 3, 1, 1, 1558809667, 1560958893),
(11, '/admin/uploads/orange.png', 3, '#FFFCEB', '#ffffff', 8, 0, 1, 0, 1558851312, 1560958235),
(12, '/admin/uploads/arganoil.png', 3, '#E9DBCB', '#ffffff', 2, 0, 1, 0, 1558851642, 1560958909);

-- --------------------------------------------------------

--
-- Структура таблицы `ingredient_images`
--

CREATE TABLE `ingredient_images` (
  `id` int(11) NOT NULL,
  `ingredient_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `ingredient_images`
--

INSERT INTO `ingredient_images` (`id`, `ingredient_id`, `image`, `position`) VALUES
(121, 5, '', 1),
(122, 5, '', 2),
(123, 5, '', 3),
(124, 5, '', 4),
(169, 11, '', 1),
(170, 11, '/admin/uploads/dop_f.png', 2),
(171, 11, '', 3),
(172, 11, '', 4),
(173, 6, '/admin/uploads/dop_g.png', 1),
(174, 6, '', 2),
(175, 6, '', 3),
(176, 6, '/admin/uploads/dop_i.png', 4),
(177, 7, '', 1),
(178, 7, '/admin/uploads/dop_h.png', 2),
(179, 7, '/admin/uploads/dop_n.png', 3),
(180, 7, '/admin/uploads/dop_m.png', 4),
(185, 9, '/admin/uploads/dop_k.png', 1),
(186, 9, '', 2),
(187, 9, '', 3),
(188, 9, '/admin/uploads/dop_j.png', 4),
(189, 10, '', 1),
(190, 10, '', 2),
(191, 10, '/admin/uploads/dop_v.png', 3),
(192, 10, '', 4),
(193, 12, '', 1),
(194, 12, '/admin/uploads/dop_s.png', 2),
(195, 12, '/admin/uploads/dop_x.png', 3),
(196, 12, '', 4),
(197, 3, '/admin/uploads/dop_b.png', 1),
(198, 3, '', 2),
(199, 3, '', 3),
(200, 3, '/admin/uploads/dop_d.png', 4),
(221, 8, '', 1),
(222, 8, '', 2),
(223, 8, '/admin/uploads/dop_l.png', 3),
(224, 8, '', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `ingredient_lang`
--

CREATE TABLE `ingredient_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8_unicode_ci,
  `seo_keywords` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `ingredient_lang`
--

INSERT INTO `ingredient_lang` (`model_id`, `language`, `label`, `content`, `seo_title`, `seo_description`, `seo_keywords`) VALUES
(3, 'ru', 'Кокос', 'Главная особенность кокосового масла — способность глубоко проникать в структуру волоса. Человеческие волосы могут впитать за 1 час объем кокосового масла в количестве до 15 % от своей массы, а за 6 часов — до 20-25 %. Поэтому, выбирая средство по уходу за волосами, обращайте внимание на продукты с содержанием столь драгоценного компонента.', '', '', ''),
(5, 'ru', 'Олива', '', '', '', ''),
(6, 'ru', 'Алоэ вера', 'Гель, хранящийся в листьях растения алоэ вера, содержит гликопротеины, которые резко повышают способность кожи к восстановлению, уменьшая боль и воспаление. Еще одно мощное целительное вещество в алоэ вера — полисахариды, которые стимулируют регенерацию кожи.', '', '', ''),
(7, 'ru', 'Огурец', 'Высокие технологии положены в основу каждой линейки продуктов. Индивидуальный подход к разным типам кожи позволил создать широкий ассортимент для удовлетворения самых требовательных клиентов. Доступная цена является еще одной положительной стороной серий данной марки.', '', '', ''),
(8, 'ru', 'Макадамия', 'Высокие технологии положены в основу каждой линейки продуктов. Индивидуальный подход к разным типам кожи позволил создать широкий ассортимент для удовлетворения самых требовательных клиентов. Доступная цена является еще одной положительной стороной серий данной марки.', NULL, NULL, NULL),
(9, 'ru', 'Ягоды годжи', 'Высокие технологии положены в основу каждой линейки продуктов. Индивидуальный подход к разным типам кожи позволил создать широкий ассортимент для удовлетворения самых требовательных клиентов. Доступная цена является еще одной положительной стороной серий данной марки.', '', '', ''),
(10, 'ru', 'Авокадо', 'Высокие технологии положены в основу каждой линейки продуктов. Индивидуальный подход к разным типам кожи позволил создать широкий ассортимент для удовлетворения самых требовательных клиентов. Доступная цена является еще одной положительной стороной серий данной марки.', '', '', ''),
(11, 'ru', 'Апельсин', 'Издавна оливковое масло применяли для лечения разных заболеваний. Также славится оно и мощным увлажняющим воздействием на кожу и волосы. Масло, полученное из свежих зеленых оливок, богато антиоксидантами, особенно полифенолами. Больше всего их в оливковом масле холодного отжима, именно поэтому оно считается самым полезным.', '', '', ''),
(12, 'ru', 'Масло арганы', 'Высокие технологии положены в основу каждой линейки продуктов. Индивидуальный подход к разным типам кожи позволил создать широкий ассортимент для удовлетворения самых требовательных клиентов. Доступная цена является еще одной положительной стороной серий данной марки.', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `language`
--

CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `short_label` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `default` int(1) DEFAULT '0',
  `active` int(1) DEFAULT '1',
  `sort_order` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `language`
--

INSERT INTO `language` (`id`, `short_label`, `label`, `locale`, `default`, `active`, `sort_order`) VALUES
(1, 'Eng', 'English', 'en', 0, 1, 2),
(2, 'Ru', 'Russian', 'ru', 1, 1, 1),
(3, 'ukr', 'Ukrainian', 'ua', 0, 1, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `route` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `data` blob
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `translation` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `message`
--

INSERT INTO `message` (`id`, `language`, `translation`) VALUES
(1, 'en', 'Категории'),
(1, 'ru', 'Категории'),
(1, 'ua', 'Категории'),
(2, 'en', 'Создать'),
(2, 'ru', 'Создать'),
(2, 'ua', 'Создать'),
(3, 'en', 'Types'),
(3, 'ru', 'Типы'),
(3, 'ua', 'Типи'),
(4, 'en', 'Ингредиенты'),
(4, 'ru', 'Ингредиенты'),
(4, 'ua', 'Ингредиенты'),
(5, 'en', 'Действие'),
(5, 'ru', 'Действие'),
(5, 'ua', 'Действие'),
(6, 'en', 'Продукты'),
(6, 'ru', 'Продукты'),
(6, 'ua', 'Продукты'),
(7, 'en', 'Серии'),
(7, 'ru', 'Серии'),
(7, 'ua', 'Серии'),
(8, 'en', 'Обновить'),
(8, 'ru', 'Обновить'),
(8, 'ua', 'Обновить'),
(9, 'en', 'Philosophy'),
(9, 'ru', 'Философия'),
(9, 'ua', 'Філософія'),
(10, 'en', 'Ingredients'),
(10, 'ru', 'Ингредиенты'),
(10, 'ua', 'Інгредієнти'),
(11, 'en', 'Where to buy'),
(11, 'ru', 'Где купить'),
(11, 'ua', 'Де придбати'),
(12, 'en', 'Магазин'),
(12, 'ru', 'Магазин'),
(12, 'ua', 'Магазин'),
(13, 'en', 'Type'),
(13, 'ru', 'Тип'),
(13, 'ua', 'Тип'),
(14, 'en', 'Influence'),
(14, 'ru', 'Действие'),
(14, 'ua', 'Дія'),
(15, 'en', 'Line'),
(15, 'ru', 'Серия'),
(15, 'ua', 'Серія'),
(16, 'en', 'All products'),
(16, 'ru', 'Все продукты'),
(16, 'ua', 'Всі продукти'),
(17, 'en', 'Настройки'),
(17, 'ru', 'Настройки'),
(17, 'ua', 'Настройки'),
(18, 'en', 'Баннер на главной'),
(18, 'ru', 'Баннер на главной'),
(18, 'ua', 'Баннер на главной'),
(19, 'en', 'Previous'),
(19, 'ru', 'Назад'),
(19, 'ua', 'Назад'),
(20, 'en', 'Next'),
(20, 'ru', 'Вперед'),
(20, 'ua', 'Вперед'),
(21, 'en', 'Scroll to find more'),
(21, 'ru', 'Скрольте, чтобы узнать больше'),
(21, 'ua', 'Скрольте, щоб дізнатися більше'),
(22, 'en', 'Natural components'),
(22, 'ru', 'Натуральные компоненты'),
(22, 'ua', 'Натуральні компоненти'),
(23, 'en', 'We inspire woman to be beautiful'),
(23, 'ru', 'Вдохновляем женщину быть красивой'),
(23, 'ua', 'Надихаємо жінку бути красивою'),
(24, 'en', 'в каждой линейке продуктов Dr. Santé'),
(24, 'ru', 'в каждой линейке продуктов Dr. Santé'),
(24, 'ua', 'в кожній лінійці продуктів Dr. Santé'),
(25, 'en', ''),
(25, 'ru', 'Натуральные<br> компоненты'),
(25, 'ua', 'Natural<br> компоненти'),
(26, 'en', ''),
(26, 'ru', 'Натуральные<br> компоненты'),
(26, 'ua', 'Натуральні<br> компоненти'),
(27, 'en', ''),
(27, 'ru', 'Высокие технологии положены в основу каждой линейки продуктов. Индивидуальный подход к разным типам кожи позволил создать широкий ассортимент для удовлетворения самых требовательных клиентов. Доступная цена является еще одной положительной стороной серий данной марки.'),
(28, 'en', ''),
(28, 'ru', 'Философия<br> Dr. Santé'),
(28, 'ua', 'Main title'),
(29, 'en', ''),
(29, 'ru', 'Высокие технологии положены в основу каждой линейки продуктов. Индивидуальный подход к разным типам кожи позволил создать широкий ассортимент для удовлетворения самых требовательных клиентов. Доступная цена является еще одной положительной стороной серий данной марки.'),
(29, 'ua', 'Main subtitle'),
(30, 'en', ''),
(30, 'ru', 'Что есть Красота? Ее эталоны, критерии и идеалы менялись с течением веков. Поэты и художники воспевали ее в своих произведениях, пытаясь показать в ней гармонию всего Мироздания. Еще Мухаммед Физули (1502-1562 гг.) уверенно заявлял: «Весь мир, в мотылька превратившись, летит на свечу Красоты», подразумевая под Красотой величайшее творенье гармоничного мира, в котором мы живем. Не утратило актуальности это изречение и сегодня.'),
(30, 'ua', 'Subtitle 2'),
(31, 'en', ''),
(31, 'ru', 'Красота – это совершенство. Она не только радует глаз и чарует звуками слух. Это и гениальные музыкальные произведения, талантливые архитектурные ансамбли и художественные полотна, блестящие научные открытия. Красота наполняет гармонией все, что нас окружает. Она внешнее отражение внутреннего здоровья. И пусть в веках остались эпохи совершенствования и украшательства, истинная ценность, со времен древней Греции, осталась прежней – сохранение естественной красоты. '),
(31, 'ua', 'Subtitle 3'),
(32, 'en', ''),
(32, 'ru', 'Красота — это плод свободной души и крепкого здоровья.'),
(32, 'ua', 'Main title 2'),
(33, 'en', ''),
(33, 'ru', 'Шефер Л.'),
(33, 'ua', 'Author'),
(34, 'en', ''),
(34, 'ru', 'Стремительный современный мир создал все условия для созидания, но в тоже время, и разрушения Красоты. Загрязненная окружающая среда, стрессы, некачественные продукты питания и вода, электро-магнитные излучения и т.п. постепенно разрушают данную нам природой Красоту. Поэтому в последние годы, в мире наметился устойчивый тренд «возврата к природе». Использование экологически чистых материалов и технологий, натурального сырья и продуктов приобретают большую популярность. Ведь только находясь в естественной среде, человек сможет оставаться здоровым физически и духовно.'),
(34, 'ua', 'Subtitle 4'),
(35, 'en', ''),
(35, 'ru', 'Среди ценностей Группы компаний «Эльфа» - здоровье, гармония, честность и экологичность, занимают первые позиции. Создавая уникальные рецептуры косметических средств, специалисты научно-исследовательской лаборатории Компании, используют прогрессивные технологии и инновационные ингредиенты, созданные самой Природой. Качественное сырье, уникальная рецептура, высокая культура производства – триада успеха наших косметических средств. Приобретая любой продукт, созданный специалистами нашей компании, можно быть уверенным в получении заявленного эффекта, что подтверждается и многочисленными исследованиями.'),
(35, 'ua', 'Subtitle 5'),
(36, 'en', ''),
(36, 'ru', 'Подчеркните природную красоту и сохраните здоровье кожи и волос с продукцией Группы компаний «Эльфа». '),
(36, 'ua', 'Subtitle 6'),
(37, 'en', ''),
(37, 'ru', 'Где купить'),
(37, 'ua', 'Title'),
(38, 'en', ''),
(38, 'ru', 'Вся наша продукция доступна в интернет-магазине elfashop.ua и магазинах партнеров.'),
(38, 'ua', 'Sub title'),
(39, 'en', ''),
(39, 'ru', 'Перейти'),
(39, 'ua', 'Button'),
(40, 'en', ''),
(40, 'ru', 'Описание'),
(41, 'en', ''),
(41, 'ru', 'Применение'),
(42, 'en', ''),
(42, 'ru', 'Состав'),
(43, 'en', ''),
(43, 'ru', 'Где купить'),
(44, 'en', ''),
(44, 'ru', 'Закрыть'),
(45, 'en', ''),
(45, 'ru', 'Купить в интернет-магазине'),
(46, 'en', ''),
(46, 'ru', 'Присоединяйся<br> к нашему сообществу'),
(46, 'ua', 'Join us title'),
(47, 'en', ''),
(47, 'ru', 'Дружеские советы, конкурсы и общение. Заходи!'),
(47, 'ua', 'Join us subtitle'),
(48, 'en', ''),
(48, 'ru', 'Присоединиться к сообществу на Facebook'),
(48, 'ua', 'Join us facebook'),
(49, 'ru', 'Details'),
(50, 'en', ''),
(50, 'ru', 'Смотреть'),
(51, 'en', ''),
(51, 'ru', 'Новинки'),
(51, 'ua', 'New product'),
(52, 'en', ''),
(52, 'ru', 'Откройте для себя новые продукты Dr. Santé'),
(52, 'ua', 'Open new product'),
(53, 'en', ''),
(53, 'ru', 'Тренды<br> этого года'),
(53, 'ua', 'Trends this year'),
(54, 'en', ''),
(54, 'ru', 'Другие средства<br> из этой линейки'),
(55, 'en', ''),
(55, 'ru', 'Тип средства'),
(56, 'en', ''),
(56, 'ru', 'Действие'),
(57, 'en', ''),
(57, 'ru', 'Снять все'),
(58, 'en', ''),
(58, 'ru', 'Серия'),
(59, 'en', ''),
(59, 'ru', 'Смотреть серию'),
(59, 'ua', 'Look series'),
(60, 'en', 'Seo title'),
(60, 'ru', 'Seo title'),
(60, 'ua', 'Seo title'),
(61, 'en', 'Seo description'),
(61, 'ru', 'Seo description'),
(61, 'ua', 'Seo description'),
(62, 'en', 'Seo keywords'),
(62, 'ru', 'Seo keywords'),
(62, 'ua', 'Seo keywords'),
(63, 'en', 'Seo title'),
(63, 'ru', 'Seo title'),
(64, 'en', 'Seo description'),
(64, 'ru', 'Seo description'),
(65, 'en', 'Seo keywords'),
(65, 'ru', 'Seo keywords'),
(66, 'en', 'Seo title'),
(66, 'ru', 'Seo title'),
(66, 'ua', 'Seo title'),
(67, 'en', 'Seo description'),
(67, 'ru', 'Seo description'),
(67, 'ua', 'Seo description'),
(68, 'en', 'Seo keywords'),
(68, 'ru', 'Seo keywords'),
(68, 'ua', 'Seo keywords'),
(69, 'en', 'Series'),
(69, 'ru', 'Series'),
(70, 'en', 'Next series'),
(70, 'ru', 'Next series'),
(71, 'en', 'Seo title'),
(71, 'ru', 'Seo title'),
(71, 'ua', 'Seo title'),
(72, 'en', 'Seo description'),
(72, 'ru', 'Seo description'),
(72, 'ua', 'Seo description'),
(73, 'en', 'Seo keywords'),
(73, 'ru', 'Seo keywords'),
(73, 'ua', 'Seo keywords'),
(74, 'en', 'No products :('),
(74, 'ru', 'Такого товара у нас нет :('),
(75, 'en', 'We add them soon'),
(75, 'ru', 'Но когда-нибудь мы его добавим'),
(76, 'en', 'Импорт'),
(76, 'ru', 'Импорт'),
(76, 'ua', 'Импорт'),
(77, 'en', 'Drag'),
(77, 'ru', 'Тяните'),
(77, 'ua', 'Тягніть');

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1558786360),
('m130524_201442_init', 1558786362),
('m180823_113212_add_foto_to_uset_table', 1558786362),
('m181021_163944_create_menu_table', 1558786362),
('m181021_164043_create_user', 1558786362),
('m181021_164412_rbac_init', 1558786362),
('m181021_164519_rbac_add_index_on_auth_assignment_user_id', 1558786362),
('m181021_164636_create_ImageManager_table', 1558786362),
('m181021_164707_addBlameableBehavior', 1558786363),
('m181021_165251_create_user_role_add_user', 1558786363),
('m181021_181615_create_translation_table', 1558786363),
('m181021_182507_create_language_table', 1558786363),
('m181105_111430_create_settings_table', 1558786364),
('m181105_135139_insert_default_language', 1558786364),
('m190222_150827_alter_settings_table', 1558786364),
('m190512_162842_create_shop_table', 1558786364),
('m190512_165024_create_type_table', 1558786364),
('m190512_194448_create_category_table', 1558786364),
('m190512_194724_create_influence_table', 1558786365),
('m190512_194936_create_series_table', 1558786365),
('m190512_195126_create_banner_table', 1558786365),
('m190512_195415_create_ingredient_table', 1558786365),
('m190513_141812_add_column_to_shop_table', 1558786365),
('m190513_195058_create_product_table', 1558786366),
('m190525_131148_create_product_to_ingredient_table', 1558790012);

-- --------------------------------------------------------

--
-- Структура таблицы `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entity_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `series_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `position_home` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) DEFAULT '1',
  `show_on_home` tinyint(1) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `product`
--

INSERT INTO `product` (`id`, `image`, `alias`, `entity_id`, `series_id`, `position`, `position_home`, `published`, `show_on_home`, `created_at`, `updated_at`) VALUES
(1, '/admin/uploads/prod_g.png', 'maska-skrab', NULL, 7, 0, 4, 1, 1, 1558857917, 1558867633),
(2, '/admin/uploads/prod_d.png', 'antibakterialnyy-tonik', NULL, 7, 0, 0, 1, 1, 1558868202, 1558868202),
(4, '/admin/uploads/760x940_balm_detox.png', 'antibakterialnyy-tonik-4', NULL, 7, 0, 0, 1, 1, 1558868276, 1561373238),
(5, '/admin/uploads/760x940_1hamp3d.png', 'detox-hair', NULL, 13, 0, 0, 1, 1, 1561542493, 1561542840);

-- --------------------------------------------------------

--
-- Структура таблицы `product_lang`
--

CREATE TABLE `product_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `volume` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `application` text COLLATE utf8_unicode_ci,
  `sklad` text COLLATE utf8_unicode_ci,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8_unicode_ci,
  `seo_keywords` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `product_lang`
--

INSERT INTO `product_lang` (`model_id`, `language`, `label`, `volume`, `content`, `application`, `sklad`, `seo_title`, `seo_description`, `seo_keywords`) VALUES
(1, 'ru', 'Маска-скраб', '75 мл', '#1 Маска-скраб мягко удаляет ороговевшие частицы вместе с излишками жира, предотвращая закупоривание пор. Разглаживает ее тон.', '#2 Маска-скраб мягко удаляет ороговевшие частицы вместе с излишками жира, предотвращая закупоривание пор. Разглаживает ее тон.', '#3 Маска-скраб мягко удаляет ороговевшие частицы вместе с излишками жира, предотвращая закупоривание пор. Разглаживает ее тон.', '', '', ''),
(2, 'ru', 'Антибактериальный тоник', '220 мл', '#1 Маска-скраб мягко удаляет ороговевшие частицы вместе с излишками жира, предотвращая закупоривание пор. Разглаживает ее тон.', '#2 Маска-скраб мягко удаляет ороговевшие частицы вместе с излишками жира, предотвращая закупоривание пор. Разглаживает ее тон.', '#3 Маска-скраб мягко удаляет ороговевшие частицы вместе с излишками жира, предотвращая закупоривание пор. Разглаживает ее тон.', '', '', ''),
(4, 'ru', 'Detox Hair', '220 мл', 'Бальзам увлажняет и разглаживает волосы по всей длине, усиливает их природный блеск и заметно улучшает внешний вид. Препятствует спутыванию, облегчает укладку прически. Волосы становятся послушными, шелковистыми, мягкими на ощупь.', 'Способ применения: нанести необходимое количество бальзама на чистые влажные волосы, равномерно распределить по всей длине, смыть водой. Меры предосторожности: при попадании в глаза промыть водой. Возможна повышенная индивидуальная чувствительность к отдельным компонентам.', 'Aqua,Cetearyl Alcohol,Cocamide MEA,Glycerin,Stearamidopropyl Dimethylamine,Wheat Amino Acids,Soy\r\nAmino Acids,Arginine HCl,Serine, Threonine,Plukenetia Volubilis Seed Oil,Citrus\r\nLimon Peel Oil,Charcoal Powder,Trehalose,Cetrimonium Chloride,Amodimethicone,Trideceth-10,Parfum,\r\nCitric Acid,Phenoxyethanol,Sodium Benzoate,Potassium Sorbate,Benzyl Alcohol', '', '', ''),
(5, 'ru', 'Detox Hair', '250', 'Шампунь интенсивно очищает волосы и кожу головы. Воздушная пена мягко ложится и быстро смывается, оставляя лишь чувство свежести и легкости.', 'Нанести на влажные волосы, вспенить, смыть водой. Меры предосторожности: при попадании в глаза промыть водой. Возможна повышенная индивидуальная чувствительность к отдельным компонентам.', 'Aqua, Sodium Myreth Sulfate, Ammonium Lauryl Sulphate, Cocamidopropyl Betaine,\r\nAcrylates Copolymer, Sodium Chloride, Plukenetia Volubilis Seed Oil, Wheat\r\nAmino Acids, Soy Amino Acids, Arginine HCl, Serine, Threonine, Charcoal Powder,\r\nCitrus Limon Peel Oil, Potassium Olivoyl, Hydrolyzed Wheat Protein, Myristyl Lactate', '', '', ''),
(6, 'ru', 'ewrerwerw', '', '', NULL, NULL, '', '', ''),
(7, 'ru', 'ewrerwerw', '', '', NULL, NULL, '', '', ''),
(8, 'ru', 'ewrerwerw', '', '', NULL, NULL, '', '', ''),
(9, 'ru', 'ewrerwerw', '', '', NULL, NULL, '', '', ''),
(10, 'ru', 'ewrerwerw', '', '', NULL, NULL, '', '', ''),
(11, 'ru', 'ewrerwerw', '', '', NULL, NULL, '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `product_to_category`
--

CREATE TABLE `product_to_category` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `product_to_category`
--

INSERT INTO `product_to_category` (`id`, `product_id`, `category_id`) VALUES
(5, 1, 3),
(6, 2, 3),
(11, 4, 3),
(18, 5, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `product_to_influence`
--

CREATE TABLE `product_to_influence` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `influence_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `product_to_influence`
--

INSERT INTO `product_to_influence` (`id`, `product_id`, `influence_id`) VALUES
(5, 1, 13),
(6, 1, 8),
(7, 1, 7),
(8, 2, 13),
(9, 2, 9),
(10, 2, 10),
(11, 2, 6),
(28, 4, 13),
(29, 4, 9),
(30, 4, 10),
(31, 4, 6),
(48, 5, 10);

-- --------------------------------------------------------

--
-- Структура таблицы `product_to_ingredient`
--

CREATE TABLE `product_to_ingredient` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `ingedient_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `product_to_ingredient`
--

INSERT INTO `product_to_ingredient` (`id`, `product_id`, `ingedient_id`) VALUES
(5, 1, 10),
(6, 2, 11),
(8, 2, 3),
(21, 4, 11),
(23, 4, 3),
(33, 5, 6);

-- --------------------------------------------------------

--
-- Структура таблицы `product_to_shop`
--

CREATE TABLE `product_to_shop` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `product_to_shop`
--

INSERT INTO `product_to_shop` (`id`, `product_id`, `shop_id`, `link`) VALUES
(17, 1, 4, '2'),
(18, 1, 6, '3'),
(19, 1, 5, '4'),
(20, 1, 3, '5'),
(22, 5, 4, 'eva.ua');

-- --------------------------------------------------------

--
-- Структура таблицы `product_to_type`
--

CREATE TABLE `product_to_type` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `product_to_type`
--

INSERT INTO `product_to_type` (`id`, `product_id`, `type_id`) VALUES
(7, 1, 13),
(8, 1, 3),
(9, 1, 10),
(10, 2, 6),
(15, 4, 3),
(23, 5, 16);

-- --------------------------------------------------------

--
-- Структура таблицы `series`
--

CREATE TABLE `series` (
  `id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `figure_type` int(11) DEFAULT NULL,
  `fon_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) DEFAULT '1',
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `series`
--

INSERT INTO `series` (`id`, `image`, `figure_type`, `fon_color`, `position`, `published`, `alias`, `created_at`, `updated_at`) VALUES
(2, '', 1, '#ffffff', 0, 1, 'senskin', 1558805469, 1558805469),
(3, '', 1, '#ffffff', 0, 1, 'oxygen', 1558805475, 1558805475),
(4, '', 1, '#ffffff', 0, 1, 'oily-rich', 1558805486, 1558805486),
(5, '', 1, '#ffffff', 0, 1, 'pure-code', 1558805494, 1558805494),
(6, '', 1, '#ffffff', 0, 1, 'goji-age-control', 1558805507, 1558805507),
(7, '/admin/uploads/inner_img_2.jpg', 1, '2', 0, 1, 'cucumber-balance-control', 1558805520, 1560409915),
(8, '', 1, '#ffffff', 0, 1, 'collagen', 1558805528, 1558805528),
(9, '', 1, '#ffffff', 0, 1, 'cc-cream', 1558805537, 1558805537),
(10, '', 1, '#ffffff', 0, 1, 'bb-cream', 1558805546, 1558805546),
(11, '', 1, '#ffffff', 0, 1, 'aqua-thermal', 1558805556, 1558805556),
(12, '', 1, '#ffffff', 0, 1, 'argan-oil', 1558805564, 1558805564),
(13, '', 1, '#ffffff', 0, 1, 'age-control', 1558805573, 1558805574),
(15, '', 1, '#ffffff', 0, 1, '0-15', 1558805591, 1558805591);

-- --------------------------------------------------------

--
-- Структура таблицы `series_lang`
--

CREATE TABLE `series_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8_unicode_ci,
  `seo_keywords` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `series_lang`
--

INSERT INTO `series_lang` (`model_id`, `language`, `label`, `content`, `seo_title`, `seo_description`, `seo_keywords`) VALUES
(2, 'ru', 'SENSkin', '', '', '', ''),
(3, 'ru', 'OxyGEN', '', '', '', ''),
(4, 'ru', 'Oily Rich', '', '', '', ''),
(5, 'ru', 'Pure code', '', '', '', ''),
(6, 'ru', 'Goji Age Control', '', '', '', ''),
(7, 'ru', 'Cucumber Balance Control', 'some content just for test some content just for test some content just for test some content just for test some content just for test', '', '', ''),
(8, 'ru', 'Collagen', '', '', '', ''),
(9, 'ru', 'CC Cream', '', '', '', ''),
(10, 'ru', 'BB Cream', '', '', '', ''),
(11, 'ru', 'Aqua Thermal', '', '', '', ''),
(12, 'ru', 'Argan Oil', '', '', '', ''),
(13, 'ru', 'Age control', '', '', '', ''),
(15, 'ru', '0%', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `label` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`id`, `key`, `type`, `description`, `created_at`, `updated_at`, `label`) VALUES
(1, 'facebook_link', 1, NULL, 1558805930, 1558805930, ''),
(2, 'philosophy_image', 2, '', 1558852278, 1558852331, '/uploads/philosophy.png'),
(3, 'followus_image', 2, '', 1558862065, 1558862086, '/uploads/followus.png'),
(4, 'main_seo_image', 2, NULL, 1559067834, 1559067834, ''),
(5, 'ingredients_seo_image', 2, NULL, 1559067917, 1559067917, ''),
(6, 'philosophy_seo_image', 2, NULL, 1559067954, 1559067954, ''),
(7, 'fly_img_a', 2, '', 1560407990, 1560408987, '/uploads/fly_img_a.png'),
(8, 'fly_img_b', 2, '', 1560407990, 1560408995, '/uploads/fly_img_b.png');

-- --------------------------------------------------------

--
-- Структура таблицы `settings_lang`
--

CREATE TABLE `settings_lang` (
  `model_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `shop`
--

CREATE TABLE `shop` (
  `id` int(11) NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ecommerce` tinyint(1) DEFAULT '1',
  `position` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `shop`
--

INSERT INTO `shop` (`id`, `logo`, `ecommerce`, `position`, `published`, `created_at`, `updated_at`, `link`) VALUES
(3, '/admin/uploads/store_a.png', 1, 4, 1, 1558853607, 1562071097, '#'),
(4, '/admin/uploads/store_b.png', 1, 3, 1, 1558853635, 1558853635, '#'),
(5, '/admin/uploads/store_c.png', 1, 2, 1, 1558853660, 1558853660, '#'),
(6, '/admin/uploads/store_d.png', 1, 0, 1, 1558853675, 1558853675, '#');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_lang`
--

CREATE TABLE `shop_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8_unicode_ci,
  `seo_keywords` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `shop_lang`
--

INSERT INTO `shop_lang` (`model_id`, `language`, `label`, `seo_title`, `seo_description`, `seo_keywords`) VALUES
(4, 'ru', 'Eva', '', '', ''),
(5, 'ru', 'Parfums', '', '', ''),
(6, 'ru', 'make up', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `source_message`
--

CREATE TABLE `source_message` (
  `id` int(11) NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `source_message`
--

INSERT INTO `source_message` (`id`, `category`, `message`) VALUES
(1, 'back/base', 'Categories'),
(2, 'back/base', 'Create'),
(3, 'back/base', 'Types'),
(4, 'back/base', 'Ingredient'),
(5, 'back/base', 'Influences'),
(6, 'back/base', 'Products'),
(7, 'back/base', 'Series'),
(8, 'back/base', 'Update'),
(9, 'frontend/menu', 'Philosophy'),
(10, 'frontend/menu', 'Ingredients'),
(11, 'frontend/menu', 'Where to buy'),
(12, 'back/base', 'Shop'),
(13, 'frontend/menu', 'Type'),
(14, 'frontend/menu', 'Influence'),
(15, 'frontend/menu', 'Seria'),
(16, 'frontend/menu', 'All products'),
(17, 'back/base', 'Settings'),
(18, 'back/base', 'Banners'),
(19, 'frontend/banner_main', 'Back'),
(20, 'frontend/banner_main', 'Forward'),
(21, 'frontend/banner_main', 'Scroll to find more'),
(22, 'frontent/main_ingredients', 'Natural ingredients'),
(23, 'frontent/main_ingredients', 'Inspire woman to be beautifull'),
(24, 'frontent/main_ingredients', 'Details about ingredients'),
(25, 'frontent/main_ingredients', 'Natural<br> ingredients'),
(26, 'frontent/ingredients', 'Natural<br> ingredients'),
(27, 'frontent/ingredients', 'Content'),
(28, 'frontend/philosophy', 'Main title'),
(29, 'frontend/philosophy', 'Main subtitle'),
(30, 'frontend/philosophy', 'Subtitle 2'),
(31, 'frontend/philosophy', 'Subtitle 3'),
(32, 'frontend/philosophy', 'Main title 2'),
(33, 'frontend/philosophy', 'Author'),
(34, 'frontend/philosophy', 'Subtitle 4'),
(35, 'frontend/philosophy', 'Subtitle 5'),
(36, 'frontend/philosophy', 'Subtitle 6'),
(37, 'frontend/where_to_buy', 'Title'),
(38, 'frontend/where_to_buy', 'Sub title'),
(39, 'frontend/where_to_buy', 'Button'),
(40, 'frontent/product', 'Content'),
(41, 'frontent/product', 'Application'),
(42, 'frontent/product', 'Composition'),
(43, 'frontent/product', 'Where to buy'),
(44, 'frontent/product', 'Close'),
(45, 'frontent/product', 'Buy in store'),
(46, 'frontend/followus', 'Join us title'),
(47, 'frontend/followus', 'Join us subtitle'),
(48, 'frontend/followus', 'Join us facebook'),
(49, 'frontend/product', 'Details'),
(50, 'frontent/product', 'Details'),
(51, 'frontend/main_page', 'New product'),
(52, 'frontend/main_page', 'Open new product'),
(53, 'frontend/main_page', 'Trends this year'),
(54, 'frontent/product', 'Other products'),
(55, 'frontend/category', 'Type'),
(56, 'frontend/category', 'Influence'),
(57, 'frontend/category', 'Unselect all'),
(58, 'frontend/category', 'Series'),
(59, 'frontend/banner_main', 'Look series'),
(60, 'frontend/main', 'Seo title'),
(61, 'frontend/main', 'Seo description'),
(62, 'frontend/main', 'Seo keywords'),
(63, 'frontend/ingredients', 'Seo title'),
(64, 'frontend/ingredients', 'Seo description'),
(65, 'frontend/ingredients', 'Seo keywords'),
(66, 'frontend/philosophy', 'Seo title'),
(67, 'frontend/philosophy', 'Seo description'),
(68, 'frontend/philosophy', 'Seo keywords'),
(69, 'frontend/series', 'Series'),
(70, 'frontend/series', 'Next series'),
(71, 'frontend/where', 'Seo title'),
(72, 'frontend/where', 'Seo description'),
(73, 'frontend/where', 'Seo keywords'),
(74, 'frontent/product', 'No products'),
(75, 'frontent/product', 'We add them soon'),
(76, 'back/base', 'Import'),
(77, 'frontend/banner_main', 'Drag');

-- --------------------------------------------------------

--
-- Структура таблицы `type`
--

CREATE TABLE `type` (
  `id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `figure_type` int(11) DEFAULT NULL,
  `fon_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `type`
--

INSERT INTO `type` (`id`, `image`, `figure_type`, `fon_color`, `alias`, `position`, `published`, `created_at`, `updated_at`) VALUES
(3, '', 1, '#ffffff', 'balzam-dlya-gub', 3, 1, 1558805187, 1562051023),
(4, '', 1, '#ffffff', 'tonik', 1, 1, 1558805259, 1558805259),
(5, '', 1, '#ffffff', 'skrab', 2, 1, 1558805275, 1558805275),
(6, '', 1, '#ffffff', 'molochko', 3, 1, 1558805287, 1558805288),
(7, '', 1, '#ffffff', 'micelyarnyy-gel', 4, 1, 1558805299, 1558805299),
(8, '', 1, '#ffffff', 'micelyarnaya-voda', 5, 1, 1558805312, 1558805312),
(9, '', 1, '#ffffff', 'penka', 6, 1, 1558805322, 1558805322),
(10, '', 1, '#ffffff', 'maska-dlya-lica', 7, 1, 1558805333, 1558805333),
(11, '', 1, '#ffffff', 'loson', 8, 1, 1558805343, 1558805343),
(12, '', 1, '#ffffff', 'gel', 9, 1, 1558805353, 1558805353),
(13, '', 1, '#ffffff', 'antivozrastnoy-krem', 10, 1, 1558805365, 1558805365),
(14, '', 1, '#ffffff', 'nochnoy-krem', 11, 1, 1558805380, 1558805380),
(15, '', 1, '#ffffff', 'dnevnoy-krem', 12, 1, 1558805391, 1558805391),
(16, '', 1, '#000000', 'shampun', 0, 1, 1561372802, 1561372816),
(19, '', NULL, '#ffffff', 'maslo', 0, 1, 1561540772, 1561540772),
(20, '', NULL, '#ffffff', 'syrovatka', 0, 1, 1561540780, 1561540780),
(21, '', NULL, '#ffffff', 'maska-dlya-volos', 0, 1, 1561540791, 1561540791),
(22, '', NULL, '#ffffff', 'sprey', 0, 1, 1561540801, 1561540801),
(23, '', NULL, '#ffffff', 'flyuid', 0, 1, 1561540806, 1561540806),
(24, '', NULL, '#ffffff', 'balzam', 0, 1, 1561540818, 1561540818),
(25, '', NULL, '#ffffff', 'lak', 0, 1, 1561540823, 1561540823),
(26, '', NULL, '#ffffff', 'krem-dlya-ruk', 0, 1, 1561540939, 1561540939),
(27, '', NULL, '#ffffff', 'loson-dlya-tela', 0, 1, 1561540950, 1561540950),
(28, '', NULL, '#ffffff', 'krem-dlya-nog', 0, 1, 1561540958, 1561540958),
(29, '', NULL, '#ffffff', 'gel-dlya-dusha', 0, 1, 1561540964, 1561540964),
(30, '', NULL, '#ffffff', 'gel-dlya-intimnoy-gigieny', 0, 1, 1561540975, 1561540976),
(31, '', NULL, '#ffffff', 'dezodorant', 0, 1, 1561540984, 1561540984),
(32, '', NULL, '#ffffff', 'maska-dlya-volos-32', 0, 1, 1561995388, 1561995388);

-- --------------------------------------------------------

--
-- Структура таблицы `type_lang`
--

CREATE TABLE `type_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `type_lang`
--

INSERT INTO `type_lang` (`model_id`, `language`, `label`, `content`) VALUES
(3, 'ru', 'Бальзам для губ', NULL),
(3, 'ua', 'Бальзам для гуд', NULL),
(4, 'ru', 'Тоник', NULL),
(5, 'ru', 'Скраб', NULL),
(6, 'ru', 'Молочко', NULL),
(7, 'ru', 'Мицелярный гель', NULL),
(8, 'ru', 'Мицелярная вода', NULL),
(9, 'ru', 'Пенка', NULL),
(10, 'ru', 'Маска для лица', NULL),
(11, 'ru', 'Лосьон', NULL),
(12, 'ru', 'Гель', NULL),
(13, 'ru', 'Антивозрастной крем', NULL),
(14, 'ru', 'Ночной крем', NULL),
(15, 'ru', 'Дневной крем', NULL),
(16, 'ru', 'Шампунь', NULL),
(16, 'ua', 'Шампунь', NULL),
(19, 'ru', 'Масло', NULL),
(20, 'ru', 'Сыроватка', NULL),
(21, 'ru', 'Маска для волос', NULL),
(22, 'ru', 'Спрей', NULL),
(23, 'ru', 'Флюид', NULL),
(24, 'ru', 'Бальзам', NULL),
(25, 'ru', 'Лак', NULL),
(26, 'ru', 'Крем для рук', NULL),
(27, 'ru', 'Лосьон для тела', NULL),
(28, 'ru', 'Крем для ног', NULL),
(29, 'ru', 'Гель для душа', NULL),
(30, 'ru', 'Гель для интимной гигиены', NULL),
(31, 'ru', 'Дезодорант', NULL),
(32, 'ru', 'Маска для волос', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `foto_id` int(11) DEFAULT NULL COMMENT 'Foto id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `foto_id`) VALUES
(1, 'admin', '3fCcs6lSno8jhHseibKQHBs8ORWLSp6J', '$2y$13$UZ6xItFWeGaGTVcXVO1tceOq22fY12.gYcumZBWZ5tBs9Vqq8dXtK', NULL, 'sygytyr@gmail.com', 10, 1558786363, 1561966344, 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD KEY `auth_assignment_user_id_idx` (`user_id`);

--
-- Индексы таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Индексы таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Индексы таблицы `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Индексы таблицы `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-banner-series` (`series_id`);

--
-- Индексы таблицы `banner_lang`
--
ALTER TABLE `banner_lang`
  ADD PRIMARY KEY (`model_id`,`language`);

--
-- Индексы таблицы `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`);

--
-- Индексы таблицы `category_lang`
--
ALTER TABLE `category_lang`
  ADD PRIMARY KEY (`model_id`,`language`);

--
-- Индексы таблицы `imagemanager`
--
ALTER TABLE `imagemanager`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `influence`
--
ALTER TABLE `influence`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`);

--
-- Индексы таблицы `influence_lang`
--
ALTER TABLE `influence_lang`
  ADD PRIMARY KEY (`model_id`,`language`);

--
-- Индексы таблицы `ingredient`
--
ALTER TABLE `ingredient`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `ingredient_images`
--
ALTER TABLE `ingredient_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-ingredient_images-ingredient` (`ingredient_id`);

--
-- Индексы таблицы `ingredient_lang`
--
ALTER TABLE `ingredient_lang`
  ADD PRIMARY KEY (`model_id`,`language`);

--
-- Индексы таблицы `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`);

--
-- Индексы таблицы `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_message_language` (`language`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`),
  ADD KEY `fk-product-series` (`series_id`);

--
-- Индексы таблицы `product_lang`
--
ALTER TABLE `product_lang`
  ADD PRIMARY KEY (`model_id`,`language`);

--
-- Индексы таблицы `product_to_category`
--
ALTER TABLE `product_to_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-product_to_category-product` (`product_id`),
  ADD KEY `fk-product_to_category-category` (`category_id`);

--
-- Индексы таблицы `product_to_influence`
--
ALTER TABLE `product_to_influence`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-product_to_influence-product` (`product_id`),
  ADD KEY `fk-product_to_influence-influence` (`influence_id`);

--
-- Индексы таблицы `product_to_ingredient`
--
ALTER TABLE `product_to_ingredient`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-product_to_ingredient-product` (`product_id`),
  ADD KEY `fk-product_to_ingredient-ingredient` (`ingedient_id`);

--
-- Индексы таблицы `product_to_shop`
--
ALTER TABLE `product_to_shop`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-product_to_shop-product` (`product_id`),
  ADD KEY `fk-product_to_shop-shop` (`shop_id`);

--
-- Индексы таблицы `product_to_type`
--
ALTER TABLE `product_to_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-product_to_type-product` (`product_id`),
  ADD KEY `fk-product_to_type-type` (`type_id`);

--
-- Индексы таблицы `series`
--
ALTER TABLE `series`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`);

--
-- Индексы таблицы `series_lang`
--
ALTER TABLE `series_lang`
  ADD PRIMARY KEY (`model_id`,`language`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key`);

--
-- Индексы таблицы `settings_lang`
--
ALTER TABLE `settings_lang`
  ADD PRIMARY KEY (`model_id`,`language`);

--
-- Индексы таблицы `shop`
--
ALTER TABLE `shop`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `shop_lang`
--
ALTER TABLE `shop_lang`
  ADD PRIMARY KEY (`model_id`,`language`);

--
-- Индексы таблицы `source_message`
--
ALTER TABLE `source_message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_source_message_category` (`category`);

--
-- Индексы таблицы `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`);

--
-- Индексы таблицы `type_lang`
--
ALTER TABLE `type_lang`
  ADD PRIMARY KEY (`model_id`,`language`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `imagemanager`
--
ALTER TABLE `imagemanager`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `influence`
--
ALTER TABLE `influence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT для таблицы `ingredient`
--
ALTER TABLE `ingredient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT для таблицы `ingredient_images`
--
ALTER TABLE `ingredient_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=233;

--
-- AUTO_INCREMENT для таблицы `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `product_to_category`
--
ALTER TABLE `product_to_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `product_to_influence`
--
ALTER TABLE `product_to_influence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT для таблицы `product_to_ingredient`
--
ALTER TABLE `product_to_ingredient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT для таблицы `product_to_shop`
--
ALTER TABLE `product_to_shop`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT для таблицы `product_to_type`
--
ALTER TABLE `product_to_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT для таблицы `series`
--
ALTER TABLE `series`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `shop`
--
ALTER TABLE `shop`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `source_message`
--
ALTER TABLE `source_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT для таблицы `type`
--
ALTER TABLE `type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `banner`
--
ALTER TABLE `banner`
  ADD CONSTRAINT `fk-banner-series` FOREIGN KEY (`series_id`) REFERENCES `series` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `banner_lang`
--
ALTER TABLE `banner_lang`
  ADD CONSTRAINT `fk-banner_lang-banner` FOREIGN KEY (`model_id`) REFERENCES `banner` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `category_lang`
--
ALTER TABLE `category_lang`
  ADD CONSTRAINT `fk-category_lang-shop` FOREIGN KEY (`model_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `influence_lang`
--
ALTER TABLE `influence_lang`
  ADD CONSTRAINT `fk-influence_lang-influence` FOREIGN KEY (`model_id`) REFERENCES `influence` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `ingredient_images`
--
ALTER TABLE `ingredient_images`
  ADD CONSTRAINT `fk-ingredient_images-ingredient` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredient` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `ingredient_lang`
--
ALTER TABLE `ingredient_lang`
  ADD CONSTRAINT `fk-ingredient_lang-ingredient` FOREIGN KEY (`model_id`) REFERENCES `ingredient` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `menu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `fk_message_source_message` FOREIGN KEY (`id`) REFERENCES `source_message` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk-product-series` FOREIGN KEY (`series_id`) REFERENCES `series` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `product_lang`
--
ALTER TABLE `product_lang`
  ADD CONSTRAINT `fk-product_lang-product` FOREIGN KEY (`model_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `product_to_category`
--
ALTER TABLE `product_to_category`
  ADD CONSTRAINT `fk-product_to_category-category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk-product_to_category-product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `product_to_influence`
--
ALTER TABLE `product_to_influence`
  ADD CONSTRAINT `fk-product_to_influence-influence` FOREIGN KEY (`influence_id`) REFERENCES `influence` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk-product_to_influence-product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `product_to_ingredient`
--
ALTER TABLE `product_to_ingredient`
  ADD CONSTRAINT `fk-product_to_ingredient-ingredient` FOREIGN KEY (`ingedient_id`) REFERENCES `ingredient` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk-product_to_ingredient-product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `product_to_shop`
--
ALTER TABLE `product_to_shop`
  ADD CONSTRAINT `fk-product_to_shop-product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk-product_to_shop-shop` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `product_to_type`
--
ALTER TABLE `product_to_type`
  ADD CONSTRAINT `fk-product_to_type-product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk-product_to_type-type` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `series_lang`
--
ALTER TABLE `series_lang`
  ADD CONSTRAINT `fk-series_lang-series` FOREIGN KEY (`model_id`) REFERENCES `series` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `settings_lang`
--
ALTER TABLE `settings_lang`
  ADD CONSTRAINT `fk-settings_lang-settings` FOREIGN KEY (`model_id`) REFERENCES `settings` (`key`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `shop_lang`
--
ALTER TABLE `shop_lang`
  ADD CONSTRAINT `fk-shop_lang-shop` FOREIGN KEY (`model_id`) REFERENCES `shop` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `type_lang`
--
ALTER TABLE `type_lang`
  ADD CONSTRAINT `fk-type_lang-shop` FOREIGN KEY (`model_id`) REFERENCES `type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
