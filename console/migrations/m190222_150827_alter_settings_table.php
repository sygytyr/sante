<?php

use yii\db\Migration;

/**
 * Class m190222_150827_alter_settings_table
 */
class m190222_150827_alter_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('settings', 'label', $this->text()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('settings', 'label');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190222_150827_alter_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
