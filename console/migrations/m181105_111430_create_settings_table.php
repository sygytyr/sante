<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings`.
 */
class m181105_111430_create_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'key'=>$this->string()->notNull()->unique(),
            'type'=>$this->integer()->notNull(),
            'description'=>$this->string()->null(),
            'created_at' => $this->integer()->null(),
            'updated_at' => $this->integer()->null()
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->createTable('settings_lang', [
            'model_id' => $this->string()->notNull(),
            'language' => $this->string(4)->notNull(),
            'label' => $this->string()->notNull(),

        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
        $this->addPrimaryKey('pk-settings_lang', 'settings_lang', ['model_id', 'language']);
        $this->addForeignKey('fk-settings_lang-settings', 'settings_lang', 'model_id', 'settings', 'key', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('settings_lang');
        $this->dropTable('settings');
    }
}
