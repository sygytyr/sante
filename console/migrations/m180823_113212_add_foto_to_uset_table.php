<?php

use yii\db\Migration;

/**
 * Class m180823_113212_add_foto_to_uset_table
 */
class m180823_113212_add_foto_to_uset_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'foto_id', $this->integer()->null()->comment('Foto id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'foto_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180823_113212_add_foto_to_uset_table cannot be reverted.\n";

        return false;
    }
    */
}
