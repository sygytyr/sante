<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product_to_ingredient}}`.
 */
class m190525_131148_create_product_to_ingredient_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%product_to_ingredient}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'ingedient_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk-product_to_ingredient-product', 'product_to_ingredient', 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-product_to_ingredient-ingredient', 'product_to_ingredient', 'ingedient_id', 'ingredient', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%product_to_ingredient}}');
    }
}
