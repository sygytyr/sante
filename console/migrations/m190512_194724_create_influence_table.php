<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%influence}}`.
 */
class m190512_194724_create_influence_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%influence}}', [
            'id' => $this->primaryKey(),
            'position'=>$this->integer()->defaultValue(0)->notNull(),
            'published'=>$this->boolean()->defaultValue(true),
            'created_at' => $this->integer()->null(),
            'alias'=>$this->string()->unique()->null(),
            'updated_at' => $this->integer()->null()
        ],'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->createTable('{{%influence_lang}}', [
            'model_id' => $this->integer()->notNull(),
            'language' => $this->string(4)->notNull(),
            'label' => $this->string()->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->addPrimaryKey('pk-influence_lang', 'influence_lang', ['model_id', 'language']);
        $this->addForeignKey('fk-influence_lang-influence', 'influence_lang', 'model_id', 'influence', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%influence_lang}}');
        $this->dropTable('{{%influence}}');
    }
}
