<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product}}`.
 */
class m190513_195058_create_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%product}}', [
            'id' => $this->primaryKey(),
            'image'=>$this->string()->null(),
            'alias'=>$this->string()->unique()->null(),
            'entity_id'=>$this->string()->null(),
            'series_id'=>$this->integer()->notNull(),
            'position'=>$this->integer()->defaultValue(0)->notNull(),
            'position_home'=>$this->integer()->defaultValue(0)->notNull(),
            'published'=>$this->boolean()->defaultValue(true),
            'show_on_home'=>$this->boolean()->defaultValue(false),
            'created_at' => $this->integer()->null(),
            'updated_at' => $this->integer()->null()
        ],'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->addForeignKey('fk-product-series', 'product', 'series_id', 'series', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%product_lang}}', [
            'model_id' => $this->integer()->notNull(),
            'language' => $this->string(4)->notNull(),
            'label' => $this->string()->notNull(),
            'volume' => $this->string()->null(),
            'content' => $this->text()->null(),
            'application' => $this->text()->null(),
            'sklad' => $this->text()->null(),
            'seo_title' => $this->string()->null(),
            'seo_description' => $this->text()->null(),
            'seo_keywords' => $this->text()->null(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->addPrimaryKey('pk-product_lang', 'product_lang', ['model_id', 'language']);
        $this->addForeignKey('fk-product_lang-product', 'product_lang', 'model_id', 'product', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%product_to_shop}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'shop_id' => $this->integer()->notNull(),
            'link'=>$this->string()->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->addForeignKey('fk-product_to_shop-product', 'product_to_shop', 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-product_to_shop-shop', 'product_to_shop', 'shop_id', 'shop', 'id', 'CASCADE', 'CASCADE');


        $this->createTable('{{%product_to_type}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'type_id' => $this->integer()->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->addForeignKey('fk-product_to_type-product', 'product_to_type', 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-product_to_type-type', 'product_to_type', 'type_id', 'type', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%product_to_category}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->addForeignKey('fk-product_to_category-product', 'product_to_category', 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-product_to_category-category', 'product_to_category', 'category_id', 'category', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%product_to_influence}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'influence_id' => $this->integer()->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->addForeignKey('fk-product_to_influence-product', 'product_to_influence', 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-product_to_influence-influence', 'product_to_influence', 'influence_id', 'influence', 'id', 'CASCADE', 'CASCADE');


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%product_to_influence}}');
        $this->dropTable('{{%product_to_category}}');
        $this->dropTable('{{%product_to_type}}');
        $this->dropTable('{{%product_to_shop}}');
        $this->dropTable('{{%product_lang}}');
        $this->dropTable('{{%product}}');
    }
}
