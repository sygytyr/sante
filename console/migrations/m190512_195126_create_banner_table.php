<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%banner}}`.
 */
class m190512_195126_create_banner_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%banner}}', [
            'id' => $this->primaryKey(),
            'image'=>$this->string()->null(),
            'figure_type'=> $this->integer()->null(),
            'fon_color'=>$this->string()->null(),
            'series_id'=> $this->integer()->null(),
            'position'=>$this->integer()->defaultValue(0)->notNull(),
            'published'=>$this->boolean()->defaultValue(true),
            'created_at' => $this->integer()->null(),
            'updated_at' => $this->integer()->null()
        ],'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->addForeignKey('fk-banner-series', 'banner', 'series_id', 'series', 'id', 'SET NULL', 'CASCADE');

        $this->createTable('{{%banner_lang}}', [
            'model_id' => $this->integer()->notNull(),
            'language' => $this->string(4)->notNull(),
            'label' => $this->string()->notNull()
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->addPrimaryKey('pk-banner_lang', 'banner_lang', ['model_id', 'language']);
        $this->addForeignKey('fk-banner_lang-banner', 'banner_lang', 'model_id', 'banner', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%banner_lang}}');
        $this->dropTable('{{%banner}}');
    }
}
