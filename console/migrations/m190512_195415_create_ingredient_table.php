<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ingredient}}`.
 */
class m190512_195415_create_ingredient_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ingredient}}', [
            'id' => $this->primaryKey(),
            'image'=>$this->string()->null(),
            'template_id'=> $this->integer()->null(),
            'fon_color'=>$this->string()->null(),
            'color'=>$this->string()->null(),
            'position'=>$this->integer()->defaultValue(0)->notNull(),
            'position_home'=>$this->integer()->defaultValue(0)->notNull(),
            'published'=>$this->boolean()->defaultValue(true),
            'show_on_home'=>$this->boolean()->defaultValue(false),
            'created_at' => $this->integer()->null(),
            'updated_at' => $this->integer()->null()
        ],'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->createTable('{{%ingredient_lang}}', [
            'model_id' => $this->integer()->notNull(),
            'language' => $this->string(4)->notNull(),
            'label' => $this->string()->notNull(),
            'content' => $this->text()->notNull(),
            'seo_title' => $this->string()->null(),
            'seo_description' => $this->text()->null(),
            'seo_keywords' => $this->text()->null(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->addPrimaryKey('pk-ingredient_lang', 'ingredient_lang', ['model_id', 'language']);
        $this->addForeignKey('fk-ingredient_lang-ingredient', 'ingredient_lang', 'model_id', 'ingredient', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%ingredient_images}}', [
            'id' => $this->primaryKey(),
            'ingredient_id' => $this->integer()->notNull(),
            'image' => $this->string()->null(),
            'position'=> $this->integer()->defaultValue(0)
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->addForeignKey('fk-ingredient_images-ingredient', 'ingredient_images', 'ingredient_id', 'ingredient', 'id', 'CASCADE', 'CASCADE');


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%ingredient_images}}');
        $this->dropTable('{{%ingredient_lang}}');
        $this->dropTable('{{%ingredient}}');
    }
}
