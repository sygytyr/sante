<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%type}}`.
 */
class m190512_165024_create_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%type}}', [
            'id' => $this->primaryKey(),
            'image'=>$this->string()->null(),
            'figure_type'=> $this->integer()->null(),
            'fon_color'=>$this->string()->null(),
            'alias'=>$this->string()->unique()->null(),
            'position'=>$this->integer()->defaultValue(0)->notNull(),
            'published'=>$this->boolean()->defaultValue(true),
            'created_at' => $this->integer()->null(),
            'updated_at' => $this->integer()->null()
        ],'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->createTable('type_lang', [
            'model_id' => $this->integer()->notNull(),
            'language' => $this->string(4)->notNull(),
            'label' => $this->string()->notNull(),
            'content' => $this->text()->null(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->addPrimaryKey('pk-type_lang', 'type_lang', ['model_id', 'language']);
        $this->addForeignKey('fk-type_lang-shop', 'type_lang', 'model_id', 'type', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%type_lang}}');
        $this->dropTable('{{%type}}');
    }
}
