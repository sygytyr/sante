<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%category}}`.
 */
class m190512_194448_create_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%category}}', [
            'id' => $this->primaryKey(),
            'fon_color'=>$this->string()->null(),
            'alias'=>$this->string()->unique()->null(),
            'position'=>$this->integer()->defaultValue(0)->notNull(),
            'published'=>$this->boolean()->defaultValue(true),
            'created_at' => $this->integer()->null(),
            'updated_at' => $this->integer()->null()
        ],'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->createTable('category_lang', [
            'model_id' => $this->integer()->notNull(),
            'language' => $this->string(4)->notNull(),
            'label' => $this->string()->notNull(),
            'content' => $this->text()->null(),
            'seo_title' => $this->string()->null(),
            'seo_description' => $this->text()->null(),
            'seo_keywords' => $this->text()->null(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->addPrimaryKey('pk-category_lang', 'category_lang', ['model_id', 'language']);
        $this->addForeignKey('fk-category_lang-shop', 'category_lang', 'model_id', 'category', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%category_lang}}');
        $this->dropTable('{{%category}}');
    }
}
