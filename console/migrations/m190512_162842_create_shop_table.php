<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%shop}}`.
 */
class m190512_162842_create_shop_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%shop}}', [
            'id' => $this->primaryKey(),
            'logo'=>$this->string()->null(),
            'ecommerce'=>$this->boolean()->defaultValue(true),
            'position'=>$this->integer()->defaultValue(0)->notNull(),
            'published'=>$this->boolean()->defaultValue(true),
            'created_at' => $this->integer()->null(),
            'updated_at' => $this->integer()->null()
        ],'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->createTable('shop_lang', [
            'model_id' => $this->integer()->notNull(),
            'language' => $this->string(4)->notNull(),
            'label' => $this->string()->notNull(),
            'seo_title' => $this->string()->null(),
            'seo_description' => $this->text()->null(),
            'seo_keywords' => $this->text()->null(),

        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
        $this->addPrimaryKey('pk-shop_lang', 'shop_lang', ['model_id', 'language']);
        $this->addForeignKey('fk-shop_lang-shop', 'shop_lang', 'model_id', 'shop', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%shop_lang}}');
        $this->dropTable('{{%shop}}');
    }
}
