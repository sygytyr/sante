<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%series}}`.
 */
class m190512_194936_create_series_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%series}}', [
            'id' => $this->primaryKey(),
            'image'=>$this->string()->null(),
            'figure_type'=> $this->integer()->null(),
            'fon_color'=>$this->string()->null(),
            'position'=>$this->integer()->defaultValue(0)->notNull(),
            'published'=>$this->boolean()->defaultValue(true),
            'alias'=>$this->string()->unique()->null(),
            'created_at' => $this->integer()->null(),
            'updated_at' => $this->integer()->null()
        ],'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->createTable('{{%series_lang}}', [
            'model_id' => $this->integer()->notNull(),
            'language' => $this->string(4)->notNull(),
            'label' => $this->string()->notNull(),
            'content' => $this->text()->null(),
            'seo_title' => $this->string()->null(),
            'seo_description' => $this->text()->null(),
            'seo_keywords' => $this->text()->null(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->addPrimaryKey('pk-series_lang', 'series_lang', ['model_id', 'language']);
        $this->addForeignKey('fk-series_lang-series', 'series_lang', 'model_id', 'series', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%series_lang}}');
        $this->dropTable('{{%series}}');
    }
}
