<?php

use yii\db\Migration;

/**
 * Class m190513_141812_add_column_to_shop_table
 */
class m190513_141812_add_column_to_shop_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%shop}}', 'link', $this->string()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%shop}}', 'link');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190513_141812_add_column_to_shop_table cannot be reverted.\n";

        return false;
    }
    */
}
