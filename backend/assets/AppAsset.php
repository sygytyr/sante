<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/font-awesome.min.css',
        'css/ionicons.min.css',
        'css/AdminLTE.min.css',
        'css/_all-skins.min.css',
        //'css/morris.css',
        //'css/jquery-jvectormap.css',
        //'css/bootstrap3-wysihtml5.min.css',
        'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic',
        'css/site.css',
    ];
    public $js = [
        'js/jquery-ui.min.js',
        //'js/raphael.min.js',
        //'js/morris.min.js',
        //'js/jquery.sparkline.min.js',
        //'js/jquery-jvectormap-1.2.2.min.js',
        //'js/jquery-jvectormap-world-mill-en.min.js',
        //'js/jquery.knob.min.js',
        'js/moment.min.js',
        'js/daterangepicker.js',
        'js/bootstrap-datepicker.min.js',
        //'js/bootstrap3-wysihtml5.all.min.js',
        'js/jquery.slimscroll.min.js',
        //'js/fastclick.js',
        'js/adminlte.js',
        'js/dashboard.js',
        'js/demo.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
