<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\color\ColorInput;

/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\Shop */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shop-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-sm-9">
            <?= $form->field($model->editLang, 'label')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model->editLang, 'content')->textarea(['rows' => 4]) ?>
            <?php foreach ($model->ingredientImages as $key=>$image){ ?>
                <div class="col-sm-3">
                    <div class="image_block">
                        <img src="<?= $image->image ?>" id="main_img" class="<?= !!$image->image ?: 'hidden' ?>" alt=""
                             style="display: block; max-width: 100%; max-height: 100px; margin: 0 auto">
                        <?= $form->field($image, "[$key]image")->widget(\mihaildev\elfinder\InputFile::className(), [
                            'language' => Yii::$app->language,
                            'controller' => 'elfinder',
                            'filter' => 'image',
                            'template' => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                            'options' => ['class' => 'form-control image-change'],
                            'buttonOptions' => ['class' => 'btn btn-default'],
                            'multiple' => false
                        ]) ?>
                    </div>
                    <?= $form->field($image, "[$key]position")->dropDownList(\backend\modules\catalog\models\Ingredient::getIngPositions(), ['disabled' => 'disabled']) ?>
                </div>

            <?php } ?>
           
        </div>


        <div class="col-sm-3">
            <div class="image_block">
                <img src="<?= $model->image ?>" id="main_img" class="<?= !!$model->image ?: 'hidden' ?>" alt=""
                     style="display: block; max-width: 100%; max-height: 100px; margin: 0 auto">
                <?= $form->field($model, 'image')->widget(\mihaildev\elfinder\InputFile::className(), [
                    'language' => Yii::$app->language,
                    'controller' => 'elfinder',
                    'filter' => 'image',
                    'template' => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                    'options' => ['class' => 'form-control image-change'],
                    'buttonOptions' => ['class' => 'btn btn-default'],
                    'multiple' => false
                ]) ?>
            </div>
            <?= $form->field($model, 'template_id')->dropDownList(\backend\modules\catalog\models\Ingredient::getTemplates()) ?>

            <?= $form->field($model, 'fon_color')->widget(ColorInput::class, [
                'options' => ['placeholder' => 'Select fon_color ...'],
            ]); ?>


            <?= $form->field($model, 'color')->widget(ColorInput::class, [
                'options' => ['placeholder' => 'Select color ...'],
            ]); ?>

            <?= $form->field($model, 'position_home')->textInput() ?>
            <?= $form->field($model, 'show_on_home')->checkbox() ?>
            <?= $form->field($model, 'published')->checkbox() ?>
            <?= $form->field($model, 'position')->textInput() ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
