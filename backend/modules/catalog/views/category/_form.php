<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\color\ColorInput;

/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\Shop */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shop-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-sm-9">
            <?= $form->field($model->editLang, 'label')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model->editLang, 'content')->textarea(['rows' => 4]) ?>

            <?= $form->field($model->editLang, 'seo_title')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model->editLang, 'seo_description')->textarea(['rows' => 4]) ?>
            <?= $form->field($model->editLang, 'seo_keywords')->textarea(['rows' => 4]) ?>
        </div>
        <div class="col-sm-3">

            <?= $form->field($model, 'alias')->textInput(['readonly'=>true]) ?>
            <?= $form->field($model, 'position')->textInput() ?>
            <?= $form->field($model, 'published')->checkbox() ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
