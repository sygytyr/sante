<?php
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Html;
?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
<?= $form->field($model, 'language')->dropDownList(\backend\models\Language::getLanguagesList(), ['prompt'=>'']); ?>
<?= $form->field($model, 'file')->widget(FileInput::classname(), [
    'options' => [],
]); ?>

<div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end() ?>
