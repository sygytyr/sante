<?php

use unclead\multipleinput\MultipleInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;


$model->categories = array_keys($model->productToCategories);
$model->influences = array_keys($model->productToInfluences);
$model->types = array_keys($model->productToTypes);
$model->ingredients = array_keys($model->productToIngredients);
$model->shops = $model->getProductToShops()->asArray()->all();


/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\Shop */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shop-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-sm-9">
            <?= $form->field($model->editLang, 'label')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model->editLang, 'content')->textarea(['rows' => 4]) ?>
            <?= $form->field($model->editLang, 'application')->textarea(['rows' => 4]) ?>
            <?= $form->field($model->editLang, 'sklad')->textarea(['rows' => 4]) ?>
            <?= $form->field($model, 'categories')->widget(Select2::class, [
                'data' => \backend\modules\catalog\models\Category::getList(),
                'options' => ['placeholder' => 'Select a category ...', 'multiple' => true],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>

            <?= $form->field($model, 'influences')->widget(Select2::class, [
                'data' => \backend\modules\catalog\models\Influence::getList(),
                'options' => ['placeholder' => 'Select an influences ...', 'multiple' => true],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>

            <?= $form->field($model, 'types')->widget(Select2::class, [
                'data' => \backend\modules\catalog\models\Type::getList(),
                'options' => ['placeholder' => 'Select a types ...', 'multiple' => true],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>

            <?= $form->field($model, 'ingredients')->widget(Select2::class, [
                'data' => \backend\modules\catalog\models\Ingredient::getList(),
                'options' => ['placeholder' => 'Select an ingredients ...', 'multiple' => true],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>
            <?= $form->field($model, 'shops')->widget(MultipleInput::class, [
                'sortable' => true,
                'min'=>0,
                'columns' => [
                    [
                        'name'  => 'shop_id',
                        'type'  => 'dropDownList',
                        'title' => 'Название',
                        'defaultValue' => 1,
                        'items' => \backend\modules\catalog\models\Shop::getList()
                    ],
                    [
                        'name'  => 'link',
                        'title' => 'Ссылка',
                        'enableError' => true,
                        'options' => [
                            'class' => 'input-priority'
                        ]
                    ]
                ]
            ]);
            ?>
            <?= $form->field($model->editLang, 'seo_title')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model->editLang, 'seo_description')->textarea(['rows' => 4]) ?>
            <?= $form->field($model->editLang, 'seo_keywords')->textarea(['rows' => 4]) ?>
        </div>
        <div class="col-sm-3">
            <div class="image_block">
                <img src="<?= $model->image ?>" id="main_img" class="<?= !!$model->image ?: 'hidden' ?>" alt=""
                     style="display: block; max-width: 100%; max-height: 100px; margin: 0 auto">
                <?= $form->field($model, 'image')->widget(\mihaildev\elfinder\InputFile::className(), [
                    'language' => Yii::$app->language,
                    'controller' => 'elfinder',
                    'filter' => 'image',
                    'template' => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                    'options' => ['class' => 'form-control image-change'],
                    'buttonOptions' => ['class' => 'btn btn-default'],
                    'multiple' => false
                ]) ?>
            </div>
            <?= $form->field($model->editLang, 'volume')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'series_id')->dropDownList(\backend\modules\catalog\models\Series::getList()) ?>
            <?= $form->field($model, 'alias')->textInput(['readonly'=> true]) ?>
            <?= $form->field($model, 'position')->textInput() ?>
            <?= $form->field($model, 'position_home')->textInput() ?>

            <?= $form->field($model, 'show_on_home')->checkbox() ?>
            <?= $form->field($model, 'published')->checkbox() ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
