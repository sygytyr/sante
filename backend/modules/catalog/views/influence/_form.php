<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\Shop */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shop-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-sm-9">
            <?= $form->field($model->editLang, 'label')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-3">

            <?= $form->field($model, 'position')->textInput() ?>
            <?= $form->field($model, 'published')->checkbox() ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
