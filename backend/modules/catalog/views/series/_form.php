<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\color\ColorInput;

/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\Shop */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shop-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-sm-9">
            <?= $form->field($model->editLang, 'label')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model->editLang, 'content')->textarea(['rows' => 4]) ?>

            <?= $form->field($model->editLang, 'seo_title')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model->editLang, 'seo_description')->textarea(['rows' => 4]) ?>
            <?= $form->field($model->editLang, 'seo_keywords')->textarea(['rows' => 4]) ?>
        </div>
        <div class="col-sm-3">
            <div class="image_block">
                <img src="<?= $model->image ?>" id="main_img" class="<?= !!$model->image ?: 'hidden' ?>" alt=""
                     style="display: block; max-width: 100%; max-height: 100px; margin: 0 auto">
                <?= $form->field($model, 'image')->widget(\mihaildev\elfinder\InputFile::className(), [
                    'language' => Yii::$app->language,
                    'controller' => 'elfinder',
                    'filter' => 'image',
                    'template' => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                    'options' => ['class' => 'form-control image-change'],
                    'buttonOptions' => ['class' => 'btn btn-default'],
                    'multiple' => false
                ]) ?>
            </div>
            <div class="hidden">
                <?= $form->field($model, 'fon_color')->dropDownList(\common\models\Figures::getSchemas(), ['id'=>'schema']) ?>
            </div>
            <label for="">Color Shema</label>
            <div class="list_schemas">
                <?php foreach (\common\models\Figures::getSchemasFront() as $key=>$index) { ?>
                    <div data-id="<?=$key?>" class="<?=$key == $model->fon_color ? 'active': ''?>">
                        <span style="background-color: <?=$index['fon']?>"></span>
                        <span style="background-color: <?=$index['clausa']?>"></span>
                        <span style="background-color: <?=$index['circle']?>"></span>
                    </div>
                <?php } ?>
            </div>



            <?= $form->field($model, 'position')->textInput() ?>
            <?= $form->field($model, 'published')->checkbox() ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
