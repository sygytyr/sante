<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.05.2019
 * Time: 22:24
 */

namespace backend\modules\catalog\controllers;


use backend\components\BaseController;
use backend\modules\catalog\models\Ingredient;
use backend\modules\catalog\models\IngredientSearch;

class IngredientController extends BaseController
{
    public $model = Ingredient::class;
    public $modelSearch = IngredientSearch::class;
    public $viewPathModule = '@backend/modules/catalog/views/ingredient/';
}