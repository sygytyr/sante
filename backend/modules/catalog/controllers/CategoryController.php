<?php


namespace backend\modules\catalog\controllers;


use backend\components\BaseController;
use backend\modules\catalog\models\Category;
use backend\modules\catalog\models\CategorySearch;

class CategoryController extends BaseController
{
    public $model = Category::class;
    public $modelSearch = CategorySearch::class;
    public $viewPathModule = '@backend/modules/catalog/views/category/';
}
