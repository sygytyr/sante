<?php


namespace backend\modules\catalog\controllers;


use backend\components\BaseController;
use backend\modules\catalog\models\Series;
use backend\modules\catalog\models\SeriesSearch;

class SeriesController extends BaseController
{
    public $model = Series::class;
    public $modelSearch = SeriesSearch::class;
    public $viewPathModule = '@backend/modules/catalog/views/series/';
}
