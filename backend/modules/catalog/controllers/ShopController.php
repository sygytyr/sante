<?php

namespace backend\modules\catalog\controllers;

use backend\components\BaseController;
use backend\modules\catalog\models\Shop;
use backend\modules\catalog\models\ShopSearch;

/**
 * ShopController implements the CRUD actions for Shop model.
 */
class ShopController extends BaseController
{
    public $model = Shop::class;
    public $modelSearch = ShopSearch::class;
    public $viewPathModule = '@backend/modules/catalog/views/shop/';
}
