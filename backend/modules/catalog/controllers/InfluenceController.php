<?php


namespace backend\modules\catalog\controllers;


use backend\components\BaseController;
use backend\modules\catalog\models\Influence;
use backend\modules\catalog\models\InfluenceSearch;

class InfluenceController extends BaseController
{
    public $model = Influence::class;
    public $modelSearch = InfluenceSearch::class;
    public $viewPathModule = '@backend/modules/catalog/views/influence/';
}
