<?php


namespace backend\modules\catalog\controllers;


use backend\components\BaseController;
use backend\modules\catalog\models\ProductSearch;
use backend\modules\catalog\models\Product;
use backend\modules\catalog\models\ProductToCategory;
use backend\modules\catalog\models\ProductToType;
use backend\modules\catalog\models\ProductToShop;
use backend\modules\catalog\models\ProductToInfluence;
use backend\modules\catalog\models\ProductLang;
use backend\modules\catalog\models\UploadForm;
use yii\web\UploadedFile;

class ProductController extends BaseController
{
    public $canImport = true;
    public $model = Product::class;
    public $modelSearch = ProductSearch::class;
    public $viewPathModule = '@backend/modules/catalog/views/product/';
    
    public function actionImport()
    {
        $model = new UploadForm();
        if (\Yii::$app->request->isPost && $model->load(\Yii::$app->request->post())) {
               
            $model->file = UploadedFile::getInstance($model, 'file');
                
                 
            if (!!$model->file) {      
                                
                $model->file->saveAs('backups/uploads/' . $model->file->baseName . '.' . $model->file->extension);
                $objPHPExcel = \PHPExcel_IOFactory::load(\Yii::getAlias('@backend').'/web/backups/uploads/' . $model->file->baseName . '.' . $model->file->extension);
                foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                    $data = $worksheet->toArray();
                    
                                       
                    
                    for ($i =1; i < count($data); $i++) {
                         $row = $data[$i];
                         $productId = (int)$row[0];                         

                         if(!$productId){
                            return $this->redirect('index');
                         }
                         $productName = trim($row[2]);
                         $productSeoT = trim($row[13]);
                         $productSeoD = trim($row[14]);
                         $productSeoK = trim($row[15]);
                         $productcontent = trim($row[3]);
                         $productapplication = trim($row[4]);
                         $productsklad  = trim($row[5]);
                         $productsvolume = trim($row[7]);
                         $productseries = (int)$row[8];
                         $productcategories = explode(',', $row[9]);                        
                         $producttypes = explode(',', $row[10]);
                         $productinfluences = explode(',', $row[11]);
                         $productshops = explode(', ', $row[12]);
                         $image = trim($row[6]);
                         $imagePath = null;
                         

                         if($image) {
                            $file = explode('/', $image);
                            $fileName  = $file[count($file) - 1];
                            if(copy($image, \Yii::getAlias('@frontend').'/web/uploads/'.$fileName)){                               
                                    $imagePath = '/uploads/'.$fileName;
                            }
                         } 
                       
                        if($product = $this->model::findOne($productId)) {
                            $productLang =  ProductLang::findOne(['model_id'=>$productId, 'language'=>$model->language]);
                            if(!$productLang){
                                $productLang = new ProductLang();    
                            }  
                            
                        } else {
                            $product = new  Product();   
                            $product->id  = $productId;
                            $productLang = new ProductLang();  
                        }
                            $product->series_id  = !!$productseries ? $productseries: null;
                            $product->image = $imagePath;
                            if(!$product->save()){
                              continue;
                            }

                            $productLang->label = $productName;
                            $productLang->volume = $productsvolume;
                            $productLang->model_id = $productId;
                            $productLang->language = $model->language;
                            $productLang->content = $productcontent;
                            $productLang->application = $productapplication ;
                            $productLang->sklad = $productsklad;
                            $productLang->save();
                            ProductToCategory::deleteAll(['product_id'=>$product->id]);
                            foreach ($productcategories as $key => $value) {
                                $p2c = new ProductToCategory();
                                $p2c->product_id = $product->id;
                                $p2c->category_id = (int)$value;
                                $p2c->save();
                            }

                            ProductToType::deleteAll(['product_id'=>$product->id]);
                            foreach ($producttypes as $key => $value) {
                                $p2c = new ProductToType();
                                $p2c->product_id = $product->id;
                                $p2c->type_id = (int)$value;
                                $p2c->save();
                            }

                            ProductToInfluence::deleteAll(['product_id'=>$product->id]);
                            foreach ($productinfluences as $key => $value) {
                                $p2c = new ProductToInfluence();
                                $p2c->product_id = $product->id;
                                $p2c->influence_id = (int)$value;
                                $p2c->save();
                            }
                            
                            ProductToShop::deleteAll(['product_id'=>$product->id]);
                            foreach ($producttypes as $key => $value) {
                                $p2c = new ProductToShop();
                                $shopArray = explode(' - ', $value);
                                $p2c->product_id = $product->id;
                                $p2c->shop_id = isset($shopArray[0]) && !!(int)$shopArray[0] ?  (int)$shopArray[0] : null;
                                $p2c->link = isset($shopArray[1]) && !!trim($shopArray[1]) ? trim($shopArray[1]) : null;
                                $p2c->save();
                            }
                        
                        
                        
                        

            
                    
                        
                    }
                   
                }
                return $this->redirect('index');
            }
        }
        return $this->render('import', ['model'=>$model]);
    }
}
