<?php


namespace backend\modules\catalog\controllers;


use backend\components\BaseController;
use backend\modules\catalog\models\Type;
use backend\modules\catalog\models\TypeSearch;

class TypeController extends BaseController
{
    public $model = Type::class;
    public $modelSearch = TypeSearch::class;
    public $viewPathModule = '@backend/modules/catalog/views/type/';
}
