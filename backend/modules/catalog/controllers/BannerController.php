<?php


namespace backend\modules\catalog\controllers;


use backend\components\BaseController;
use backend\modules\catalog\models\Banner;
use backend\modules\catalog\models\BannerSearch;

class BannerController extends BaseController
{
    public $model = Banner::class;
    public $modelSearch = BannerSearch::class;
    public $viewPathModule = '@backend/modules/catalog/views/banner/';
}
