<?php

namespace backend\modules\catalog\models;

use backend\components\behaviors\ManyToManyBehavior;
use backend\components\behaviors\MultipleBehavior;
use common\components\Yiit;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $image
 * @property string $entity_id
 * @property int $series_id
 * @property int $position
 * @property int $position_home
 * @property int $published
 * @property int $show_on_home
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Series $series
 * @property ProductLang[] $productLangs
 * @property ProductToCategory[] $productToCategories
 * @property ProductToInfluence[] $productToInfluences
 * @property ProductToShop[] $productToShops
 * @property ProductToType[] $productToTypes
 */
class Product extends \common\models\base\BaseModel
{
    public $categories;
    public $influences;
    public $types;
    public $ingredients;
    public $shops;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['series_id'], 'required'],
            [
                ['series_id', 'position', 'position_home', 'published', 'show_on_home', 'created_at', 'updated_at'],
                'integer'
            ],
            [['alias'], 'unique'],
            [['categories', 'influences', 'types', 'ingredients', 'shops'], 'safe'],
            [['image', 'entity_id'], 'string', 'max' => 255],
            [
                ['series_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Series::class,
                'targetAttribute' => ['series_id' => 'id']
            ],
        ];
    }

    public function getTitle()
    {
        return Yiit::tr('back/base', 'Products');
    }

    public $langModel = ProductLang::class;

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Картинка',
            'entity_id' => 'Категория',
            'series_id' => 'Серия',
            'position' => 'Позиция',
            'position_home' => 'Позиция на Главной',
            'published' => 'Опубликовано',
            'show_on_home' => 'Отображать на Главной',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
            'categories' => 'Категории',
            'influences' => 'Действие',
            'types' => 'Типы',
            'ingredients' => 'Ингридиенты',
            'shops' => 'Магазины',
        ];
    }

    public function getIndexConfig()
    {
        return [
    //        ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'lang',
                'value' => 'editLang.label',
               
                'label' => 'Название'
            ],
            [
                'attribute' => 'created_at',
                'value' => function ($data) {
                    return Yii::$app->formatter->asDatetime($data->created_at);
                },
                'filter' => false
            ],
            'published:boolean',
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'position',
                'editableOptions' =>
                    [
                        'formOptions' => ['action' => ['editable']],

                    ]
            ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{delete}'],
            [
                'class' => 'lav45\translate\grid\ActionColumn',
                'languages' => \backend\models\Language::getList(),
            ],
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeries()
    {
        return $this->hasOne(Series::class, ['id' => 'series_id'])->with(['lang']);
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'timestamp' => [
                'class' => TimestampBehavior::class,
            ],
            'categories' => [
                'class'=>ManyToManyBehavior::class,
                'modelClass' => ProductToCategory::class,
                'ownerField' => 'product_id',
                'relatedField' => 'category_id',
                'fieldName' => 'categories'
            ],
            'influences' => [
                'class'=>ManyToManyBehavior::class,
                'modelClass' => ProductToInfluence::class,
                'ownerField' => 'product_id',
                'relatedField' => 'influence_id',
                'fieldName' => 'influences'
            ],
            'types' => [
                'class'=>ManyToManyBehavior::class,
                'modelClass' => ProductToType::class,
                'ownerField' => 'product_id',
                'relatedField' => 'type_id',
                'fieldName' => 'types'
            ],
            'ingredients' => [
                'class'=>ManyToManyBehavior::class,
                'modelClass' => ProductToIngredient::class,
                'ownerField' => 'product_id',
                'relatedField' => 'ingedient_id',
                'fieldName' => 'ingredients'
            ],
            'shops'=>[
                'class'=>MultipleBehavior::class,
                'modelClass' => ProductToShop::class,
                'dataName' => 'shops',
                'fieldName' => 'product_id',
            ]
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductLangs()
    {
        return $this->hasMany(ProductLang::class, ['model_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductToCategories()
    {
        return $this->hasMany(ProductToCategory::class, ['product_id' => 'id'])->indexBy('category_id');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductToInfluences()
    {
        return $this->hasMany(ProductToInfluence::class, ['product_id' => 'id'])->indexBy('influence_id');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductToIngredients()
    {
        return $this->hasMany(ProductToIngredient::class, ['product_id' => 'id'])->indexBy('ingedient_id');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductToShops()
    {
        return $this->hasMany(ProductToShop::class, ['product_id' => 'id'])->with(['shop'])->orderBy(['id'=>SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductToTypes()
    {
        return $this->hasMany(ProductToType::class, ['product_id' => 'id'])->indexBy('type_id');
    }
}
