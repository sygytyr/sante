<?php

namespace backend\modules\catalog\models;

use common\components\Yiit;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "type".
 *
 * @property int $id
 * @property string $image
 * @property int $figure_type
 * @property string $fon_color
 * @property int $position
 * @property int $published
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TypeLang[] $typeLangs
 */
class Type extends \common\models\base\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'type';
    }

    public function getTitle()
    {
        return Yiit::tr('back/base', 'Types');
    }

    public $langModel = TypeLang::class;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['figure_type', 'position', 'published', 'created_at', 'updated_at'], 'integer'],
            [['image', 'fon_color'], 'string', 'max' => 255],
            [['published'], 'default', 'value' => true],
            [['alias'], 'unique'],
            [['fon_color'], 'default', 'value' => '#ffffff']
        ];
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'timestamp' => [
                'class' => TimestampBehavior::class,
            ],
        ]);
    }

    public function getIndexConfig()
    {
        return [
    //        ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'editLang.label',
                'label' => 'Label'
            ],
            [
                'attribute' => 'created_at',
                'value' => function ($data) {
                    return Yii::$app->formatter->asDatetime($data->created_at);
                },
                'filter' => false
            ],
            'published:boolean',
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'position',
                'editableOptions' =>
                    [
                        'formOptions' => ['action' => ['editable']],

                    ]
            ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{delete}'],
            [
                'class' => 'lav45\translate\grid\ActionColumn',
                'languages' => \backend\models\Language::getList(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Image',
            'figure_type' => 'Figure Type',
            'fon_color' => 'Fon Color',
            'position' => 'Position',
            'published' => 'Published',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypeLangs()
    {
        return $this->hasMany(TypeLang::class, ['model_id' => 'id']);
    }

    public static function getListFrontend()
    {
        return self::find()->joinWith(['lang'], false)->select(['type_lang.label', 'position', 'alias', 'id'])->andWhere(['published'=> 1])->orderBy(['position'=>SORT_DESC])->asArray()->all();

    }

    public static function getList()
    {
        return self::find()->joinWith('lang')->select('type_lang.label')->indexBy('id')->orderBy(['type_lang.label' => SORT_ASC])->column();
    }
}
