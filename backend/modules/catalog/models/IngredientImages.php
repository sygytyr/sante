<?php

namespace backend\modules\catalog\models;

use Yii;

/**
 * This is the model class for table "ingredient_images".
 *
 * @property int $id
 * @property int $ingredient_id
 * @property string $image
 * @property int $position
 *
 * @property Ingredient $ingredient
 */
class IngredientImages extends \common\models\base\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ingredient_images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ingredient_id'], 'required'],
            [['ingredient_id', 'position'], 'integer'],
            [['image'], 'string', 'max' => 255],
            [['ingredient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ingredient::className(), 'targetAttribute' => ['ingredient_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ingredient_id' => 'ID ингредиента',
            'image' => 'Картинка',
            'position' => 'Позиция',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredient()
    {
        return $this->hasOne(Ingredient::className(), ['id' => 'ingredient_id']);
    }
}
