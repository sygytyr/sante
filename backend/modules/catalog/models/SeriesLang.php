<?php

namespace backend\modules\catalog\models;

use backend\components\behaviors\SlugBehaviors;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "series_lang".
 *
 * @property int $model_id
 * @property string $language
 * @property string $label
 * @property string $content
 *
 * @property Series $model
 */
class SeriesLang extends \common\models\base\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'series_lang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['model_id', 'language', 'label'], 'required'],
            [['model_id'], 'integer'],
            [['content'], 'string'],
            [['language'], 'string', 'max' => 4],
            [['label', 'seo_title'], 'string', 'max' => 255],
            [['seo_keywords', 'seo_description'], 'safe'],
            [['model_id', 'language'], 'unique', 'targetAttribute' => ['model_id', 'language']],
            [['model_id'], 'exist', 'skipOnError' => true, 'targetClass' => Series::className(), 'targetAttribute' => ['model_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'model_id' => 'Model ID',
            'language' => 'Языки',
            'label' => 'Название',
            'content' => 'Описание',
            'seo_title' => 'Seo title',
            'seo_keywords' => 'Seo keywords',
            'seo_description' => 'Seo description',
        ];
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'slug' => [
                'class' => SlugBehaviors::class,
            ],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(Series::className(), ['id' => 'model_id']);
    }
}
