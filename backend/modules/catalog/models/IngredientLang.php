<?php

namespace backend\modules\catalog\models;

use Yii;

/**
 * This is the model class for table "ingredient_lang".
 *
 * @property int $model_id
 * @property string $language
 * @property string $label
 *
 * @property Ingredient $model
 */
class IngredientLang extends \common\models\base\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ingredient_lang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['model_id', 'language', 'label'], 'required'],
            [['model_id'], 'integer'],
            [['language'], 'string', 'max' => 4],
            [['label', 'seo_title'], 'string', 'max' => 255],
            [['seo_keywords', 'seo_description', 'content'], 'safe'],
            [['model_id', 'language'], 'unique', 'targetAttribute' => ['model_id', 'language']],
            [['model_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ingredient::className(), 'targetAttribute' => ['model_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'model_id' => 'Model ID',
            'language' => 'Языки',
            'label' => 'Название',
            'seo_title' => 'Seo title',
            'content' => 'Описание',
            'seo_keywords' => 'Seo keywords',
            'seo_description' => 'Seo description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(Ingredient::className(), ['id' => 'model_id']);
    }
}
