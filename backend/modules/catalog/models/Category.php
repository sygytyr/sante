<?php

namespace backend\modules\catalog\models;

use common\components\Yiit;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string $fon_color
 * @property int $position
 * @property int $published
 * @property int $created_at
 * @property int $updated_at
 *
 * @property CategoryLang[] $categoryLangs
 */
class Category extends \common\models\base\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    public function getTitle()
    {
        return Yiit::tr('back/base', 'Categories');
    }

    public $langModel = CategoryLang::class;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['position', 'published', 'created_at', 'updated_at'], 'integer'],
            [['fon_color'], 'string', 'max' => 255],
            [['published'], 'default', 'value' => true],
            [['alias'], 'unique']
        ];
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'timestamp' => [
                'class' => TimestampBehavior::class,
            ],
        ]);
    }

    public function getIndexConfig()
    {
        return [
    //        ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'editLang.label',
                'label' => 'Название'
            ],
            [
                'attribute' => 'created_at',
                'value' => function ($data) {
                    return Yii::$app->formatter->asDatetime($data->created_at);
                },
                'filter' => false
            ],
            'published:boolean',
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'position',
                'editableOptions' =>
                    [
                        'formOptions' => ['action' => ['editable']],

                    ]
            ],
            [
                'class' => 'lav45\translate\grid\ActionColumn',
                'languages' => \backend\models\Language::getList(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fon_color' => 'Цвет фона',
            'position' => 'Позиция',
            'published' => 'Опубликовано',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryLangs()
    {
        return $this->hasMany(CategoryLang::className(), ['model_id' => 'id']);
    }
    public function getTypesList()
    {
        return $this->hasMany(Type::class, ['id'=>'type_id'])->joinWith(['lang'], false)->andWhere(['type.published'=>1])->orderBy(['position'=>SORT_DESC])->via('types');
    }

    public function getProducts()
    {
        return $this->hasMany(Product::class, ['id'=>'product_id'])->joinWith(['lang', 'series'], false)->andWhere(['product.published'=>1])->limit(12)->orderBy(['product.position'=>SORT_DESC])->viaTable('product_to_category', ['category_id'=>'id']);
    }

    public function getSeries()
    {
        return $this->hasMany(Series::class, ['id'=>'series_id'])->joinWith(['lang'], false)->via('products');
    }

    public function getInfluencesList()
    {
        return $this->hasMany(Influence::class, ['id'=>'influence_id'])->joinWith(['lang'], false)->andWhere(['influence.published'=>1])->orderBy(['position'=>SORT_DESC])->via('influences');
    }

    public function getTypes()
    {
        return $this->hasMany(ProductToType::class, ['product_id'=>'product_id'])->viaTable('product_to_category', ['category_id'=>'id'])->indexBy('type_id');
    }

    public function getInfluences()
    {
        return $this->hasMany(ProductToInfluence::class, ['product_id'=>'product_id'])->viaTable('product_to_category', ['category_id'=>'id'])->indexBy('influence_id');
    }

    public static function getList()
    {
        return self::find()->joinWith('lang')->select('category_lang.label')->indexBy('id')->orderBy(['category_lang.label' => SORT_ASC])->column();
    }

    public static function getListFrontend()
    {
        return self::find()->joinWith(['lang'], false)->select(['category_lang.label', 'position', 'alias', 'id'])->andWhere(['published'=> 1])->orderBy(['position'=>SORT_DESC])->asArray()->all();

    }

}
