<?php

namespace backend\modules\catalog\models;

use Yii;

/**
 * This is the model class for table "banner_lang".
 *
 * @property int $model_id
 * @property string $language
 * @property string $label
 *
 * @property Banner $model
 */
class BannerLang extends \common\models\base\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'banner_lang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['model_id', 'language', 'label'], 'required'],
            [['model_id'], 'integer'],
            [['language'], 'string', 'max' => 4],
            [['label'], 'string', 'max' => 255],
            [['model_id', 'language'], 'unique', 'targetAttribute' => ['model_id', 'language']],
            [['model_id'], 'exist', 'skipOnError' => true, 'targetClass' => Banner::className(), 'targetAttribute' => ['model_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'model_id' => 'Model ID',
            'language' => 'Языки',
            'label' => 'Название',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(Banner::className(), ['id' => 'model_id']);
    }
}
