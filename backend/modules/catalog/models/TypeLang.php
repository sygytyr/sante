<?php

namespace backend\modules\catalog\models;

use backend\components\behaviors\SlugBehaviors;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "type_lang".
 *
 * @property int $model_id
 * @property string $language
 * @property string $label
 * @property string $content
 *
 * @property Type $model
 */
class TypeLang extends \common\models\base\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'type_lang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['model_id', 'language', 'label'], 'required'],
            [['model_id'], 'integer'],
            [['content'], 'string'],
            [['language'], 'string', 'max' => 4],
            [['label'], 'string', 'max' => 255],
            [['model_id', 'language'], 'unique', 'targetAttribute' => ['model_id', 'language']],
            [['model_id'], 'exist', 'skipOnError' => true, 'targetClass' => Type::className(), 'targetAttribute' => ['model_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'model_id' => 'Model ID',
            'language' => 'Языки',
            'label' => 'Название',
            'content' => 'Описание',
        ];
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'slug' => [
                'class' => SlugBehaviors::class,
            ],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(Type::className(), ['id' => 'model_id']);
    }
}
