<?php

namespace backend\modules\catalog\models;

use common\components\Yiit;
use Yii;
use yii\base\Model;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "ingredient".
 *
 * @property int $id
 * @property string $image
 * @property int $template_id
 * @property string $fon_color
 * @property string $color
 * @property int $position
 * @property int $position_home
 * @property int $published
 * @property int $show_on_home
 * @property int $created_at
 * @property int $updated_at
 *
 * @property IngredientImages[] $ingredientImages
 * @property IngredientLang[] $ingredientLangs
 */
class Ingredient extends \common\models\base\BaseModel
{
    CONST POSITION_TOP_LEFT = 1;
    CONST POSITION_TOP_RIGHT = 2;
    CONST POSITION_BOTTOM_LEFT = 3;
    CONST POSITION_BOTTOM_RIGHT = 4;

    public static  function getIngPositions()
    {
        return [
            self::POSITION_TOP_LEFT=>'Вверху Слева',
            self::POSITION_TOP_RIGHT=>'Вверху Справа',
            self::POSITION_BOTTOM_LEFT=>'Внизу Слева',
            self::POSITION_BOTTOM_RIGHT=>'Внизу Справа',

        ];
    }

    CONST TEMPLATE_1 = 1;
    CONST TEMPLATE_2 = 2;
    CONST TEMPLATE_3 = 3;

    public static  function getTemplates()
    {
        return [
            self::TEMPLATE_1=>'Template 1',
            self::TEMPLATE_2=>'Template 2',
            self::TEMPLATE_3=>'Template 3',

        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ingredient';
    }

    public function getTitle()
    {
        return Yiit::tr('back/base', 'Ingredient');
    }

    public $langModel = IngredientLang::class;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['template_id', 'position', 'position_home', 'published', 'show_on_home', 'created_at', 'updated_at'], 'integer'],
            [['image', 'fon_color', 'color'], 'string', 'max' => 255],
            [['published', 'show_on_home'], 'default', 'value' => true],
            [['fon_color', 'color'], 'required']
        ];
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'timestamp' => [
                'class' => TimestampBehavior::class,
            ],
        ]);
    }

    public function getIndexConfig()
    {
        return [
    //        ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'editLang.label',
                'label' => 'Название'
            ],
            [
                'attribute' => 'created_at',
                'value' => function ($data) {
                    return Yii::$app->formatter->asDatetime($data->created_at);
                },
                'filter' => false
            ],
            'published:boolean',
            'show_on_home:boolean',
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'position_home',
                'editableOptions' =>
                    [
                        'formOptions' => ['action' => ['editable']],

                    ]
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'position',
                'editableOptions' =>
                    [
                        'formOptions' => ['action' => ['editable']],

                    ]
            ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{delete}'],
            [
                'class' => 'lav45\translate\grid\ActionColumn',
                'languages' => \backend\models\Language::getList(),
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Картинка',
            'template_id' => 'Тип кляксы',
            'fon_color' => 'Цвет кляксы',
            'color' => 'Цвет названия',
            'position' => 'Позиция',
            'position_home' => 'Позиция на Главной',
            'published' => 'Опубликовано',
            'show_on_home' => 'Отображать на Главной',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredientImages()
    {
        if(count(IngredientImages::findAll(['ingredient_id'=>$this->id])) < 4){
            IngredientImages::deleteAll(['ingredient_id'=>$this->id]);
            $images = [];
            foreach (self::getIngPositions() as $index=>$label){
                $newImg = new IngredientImages();
                $newImg->position = $index;
                $images[] = $newImg;
            }
            return $images;
        }
        return $this->hasMany(IngredientImages::class, ['ingredient_id' => 'id']);
    }

    public function getImages()
    {
        return $this->hasMany(IngredientImages::class, ['ingredient_id' => 'id'])->indexBy('position');
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredientLangs()
    {
        return $this->hasMany(IngredientLang::class, ['model_id' => 'id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        IngredientImages::deleteAll(['ingredient_id'=>$this->id]);
        $images = [];
        foreach (self::getIngPositions() as $index=>$label){
            $newImg = new IngredientImages();
            $newImg->position = $index;
            $images[] = $newImg;
        }
        Model::loadMultiple($images, Yii::$app->request->post());
        foreach ($images as $image){
            $image->ingredient_id = $this->id;
            $image->save();
        }

    }

    public static function getList()
    {
        return self::find()->joinWith('lang')->select('ingredient_lang.label')->indexBy('id')->orderBy(['ingredient_lang.label' => SORT_ASC])->column();
    }

    public static function getListMainPage()
    {
        return self::find()->joinWith(['lang'], false)->select(['ingredient_lang.label', 'position_home', 'image', 'color', 'fon_color', 'id', 'template_id'])->andWhere(['published'=> 1, 'show_on_home'=>1])->orderBy(['position_home'=>SORT_DESC])->asArray()->all();

    }
}
