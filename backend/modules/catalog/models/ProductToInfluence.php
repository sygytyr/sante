<?php

namespace backend\modules\catalog\models;

use Yii;

/**
 * This is the model class for table "product_to_influence".
 *
 * @property int $id
 * @property int $product_id
 * @property int $influence_id
 *
 * @property Influence $influence
 * @property Product $product
 */
class ProductToInfluence extends \common\models\base\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_to_influence';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'influence_id'], 'required'],
            [['product_id', 'influence_id'], 'integer'],
            [['influence_id'], 'exist', 'skipOnError' => true, 'targetClass' => Influence::className(), 'targetAttribute' => ['influence_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'ID продукта',
            'influence_id' => 'ID действия',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInfluence()
    {
        return $this->hasOne(Influence::className(), ['id' => 'influence_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
