<?php

namespace backend\modules\catalog\models;

use common\components\Yiit;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "shop".
 *
 * @property int $id
 * @property string $logo
 * @property int $ecommerce
 * @property int $position
 * @property int $published
 * @property int $created_at
 * @property int $updated_at
 *
 * @property ShopLang[] $shopLangs
 */
class Shop extends \common\models\base\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shop';
    }

    public function getTitle()
    {
        return Yiit::tr('back/base', 'Shop');
    }

    public $langModel = ShopLang::class;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ecommerce', 'position', 'published', 'created_at', 'updated_at'], 'integer'],
            [['logo'], 'string', 'max' => 255],
            [['link'], 'string', 'max' => 255],
            [['published', 'ecommerce'], 'default', 'value' => true],
            [['link'], 'required']
        ];
    }

    public function getIndexConfig()
    {
        return [
    //        ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'editLang.label',
                'label' => 'Название'
            ],
            'link',
            [
                'attribute' => 'created_at',
                'value' => function ($data) {
                    return Yii::$app->formatter->asDatetime($data->created_at);
                },
                'filter' => false
            ],
            'published:boolean',
            'ecommerce:boolean',
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'position',
                'editableOptions' =>
                    [
                        'formOptions' => ['action' => ['editable']],

                    ]
            ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{delete}'],
            [
                'class' => 'lav45\translate\grid\ActionColumn',
                'languages' => \backend\models\Language::getList(),
            ],
        ];
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'timestamp' => [
                'class' => TimestampBehavior::class,
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'logo' => 'Логотип',
            'link' => 'Ссылка',
            'ecommerce' => 'Ecommerce',
            'position' => 'Позиция',
            'published' => 'Опубликовано',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopLangs()
    {
        return $this->hasMany(ShopLang::className(), ['model_id' => 'id']);
    }

    public static function getList()
    {
        return self::find()->joinWith('lang')->select('shop_lang.label')->indexBy('id')->orderBy(['shop_lang.label' => SORT_ASC])->column();
    }
}
