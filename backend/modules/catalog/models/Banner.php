<?php

namespace backend\modules\catalog\models;

use common\components\Yiit;
use common\models\Figures;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "banner".
 *
 * @property int $id
 * @property string $image
 * @property int $figure_type
 * @property string $fon_color
 * @property int $series_id
 * @property int $position
 * @property int $published
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Series $series
 * @property BannerLang[] $bannerLangs
 */
class Banner extends \common\models\base\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'banner';
    }

    public function getTitle()
    {
        return Yiit::tr('back/base', 'Banners');
    }

    public $langModel = BannerLang::class;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['figure_type', 'series_id', 'position', 'published', 'created_at', 'updated_at'], 'integer'],
            [['image', 'fon_color'], 'string', 'max' => 255],
            [
                ['series_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Series::class,
                'targetAttribute' => ['series_id' => 'id']
            ],
            [['published'], 'default', 'value' => true],
            [['series_id'], 'required'],
            [['fon_color'], 'default', 'value'=>Figures::COLOR_SCHEMA_TYPE_1]
        ];
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'timestamp' => [
                'class' => TimestampBehavior::class,
            ],
        ]);
    }

    public function getIndexConfig()
    {
        return [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'editLang.label',
                'label' => 'Название'
            ],
            [
                'attribute' => 'series_id',
                'value' => function ($data) {
                    return $data->series->lang->label ?? null;
                },
                'filter' => Series::getList()
            ],
            [
                'attribute' => 'created_at',
                'value' => function ($data) {
                    return Yii::$app->formatter->asDatetime($data->created_at);
                },
                'filter' => false
            ],
            'published:boolean',
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'position',
                'editableOptions' =>
                    [
                        'formOptions' => ['action' => ['editable']],

                    ]
            ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{delete}'],
            [
                'class' => 'lav45\translate\grid\ActionColumn',
                'languages' => \backend\models\Language::getList(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Картинка',
            'figure_type' => 'Тип кляксы',
            'fon_color' => 'Цветовая схема',
            'series_id' => 'Серия',
            'position' => 'Позиция',
            'published' => 'Опубликовано',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeries()
    {
        return $this->hasOne(Series::className(), ['id' => 'series_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBannerLangs()
    {
        return $this->hasMany(BannerLang::className(), ['model_id' => 'id']);
    }
}
