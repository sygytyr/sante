<?php

namespace backend\modules\catalog\models;

use Yii;

/**
 * This is the model class for table "shop_lang".
 *
 * @property int $model_id
 * @property string $language
 * @property string $label
 *
 * @property Shop $model
 */
class ShopLang extends \common\models\base\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shop_lang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['model_id', 'language', 'label'], 'required'],
            [['model_id'], 'integer'],
            [['language'], 'string', 'max' => 4],
            [['label', 'seo_title'], 'string', 'max' => 255],
            [['seo_keywords', 'seo_description'], 'safe'],
            [['model_id', 'language'], 'unique', 'targetAttribute' => ['model_id', 'language']],
            [['model_id'], 'exist', 'skipOnError' => true, 'targetClass' => Shop::className(), 'targetAttribute' => ['model_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'model_id' => 'Model ID',
            'language' => 'Языки',
            'label' => 'Название',
            'seo_title' => 'Seo title',
            'seo_keywords' => 'Seo keywords',
            'seo_description' => 'Seo description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(Shop::className(), ['id' => 'model_id']);
    }
}
