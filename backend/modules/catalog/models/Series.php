<?php

namespace backend\modules\catalog\models;

use common\components\Yiit;
use common\models\Figures;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use backend\modules\catalog\models\Banner;

/**
 * This is the model class for table "series".
 *
 * @property int $id
 * @property string $image
 * @property int $figure_type
 * @property string $fon_color
 * @property int $position
 * @property int $published
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Banner[] $banners
 * @property SeriesLang[] $seriesLangs
 */
class Series extends \common\models\base\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'series';
    }

    public function getTitle()
    {
        return Yiit::tr('back/base', 'Series');
    }

    public $langModel = SeriesLang::class;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['figure_type', 'position', 'published', 'created_at', 'updated_at'], 'integer'],
            [['image', 'fon_color'], 'string', 'max' => 255],
            [['published'], 'default', 'value' => true],
            [['fon_color'], 'required'],
            [['alias'], 'unique'],
            [['fon_color'], 'default', 'value'=>Figures::COLOR_SCHEMA_TYPE_1]
        ];
    }


    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'timestamp' => [
                'class' => TimestampBehavior::class,
            ],
        ]);
    }

    public function getIndexConfig()
    {
        return [
    //        ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'editLang.label',
                'label' => 'Название'
            ],
            [
                'attribute' => 'created_at',
                'value' => function ($data) {
                    return Yii::$app->formatter->asDatetime($data->created_at);
                },
                'filter' => false
            ],
            'published:boolean',
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'position',
                'editableOptions' =>
                    [
                        'formOptions' => ['action' => ['editable']],

                    ]
            ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{delete}'],
            [
                'class' => 'lav45\translate\grid\ActionColumn',
                'languages' => \backend\models\Language::getList(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Картинка',
            'figure_type' => 'Тип кляксы',
            'fon_color' => 'Цветовая схема',
            'position' => 'Позиция',
            'published' => 'Опубликовано',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanners()
    {
        return $this->hasMany(Banner::className(), ['series_id' => 'id']);
    }

    public function getBanner()
    {
        return $this->hasOne(Banner::className(), ['series_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeriesLangs()
    {
        return $this->hasMany(SeriesLang::className(), ['model_id' => 'id']);
    }

    public function getProducts()
    {
        return $this->hasMany(Product::class, ['series_id'=>'id'])->andWhere(['product.published'=> 1])->joinWith(['lang'], true)->orderBy(['product.position'=>SORT_DESC]);
    }

    public static function getList()
    {
        return self::find()->joinWith('lang')->select('series_lang.label')->indexBy('id')->orderBy(['series_lang.label' => SORT_ASC])->column();
    }

    public static function getListFrontend()
    {
        return self::find()->joinWith(['lang'],false)->select(['series_lang.label', 'position', 'alias', 'id'])->andWhere(['published'=> 1])->orderBy(['position'=>SORT_DESC])->asArray()->all();

    }
}
