<?php

namespace backend\modules\catalog\models;

use backend\components\behaviors\SlugBehaviors;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "product_lang".
 *
 * @property int $model_id
 * @property string $language
 * @property string $label
 * @property string $volume
 * @property string $content
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 *
 * @property Product $model
 */
class ProductLang extends \common\models\base\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_lang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['model_id', 'language', 'label'], 'required'],
            [['model_id'], 'integer'],
            [['content', 'application', 'sklad', 'seo_description', 'seo_keywords'], 'string'],
            [['language'], 'string', 'max' => 4],
            [['label', 'volume', 'seo_title'], 'string', 'max' => 255],
            [['model_id', 'language'], 'unique', 'targetAttribute' => ['model_id', 'language']],
            [['model_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::class, 'targetAttribute' => ['model_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'model_id' => 'Model ID',
            'language' => 'Языки',
            'label' => 'Название',
            'volume' => 'Объем',
            'content' => 'Описание',
            'application' => 'Применение',
            'sklad' => 'Состав',
            'seo_title' => 'Seo title',
            'seo_description' => 'Seo Description',
            'seo_keywords' => 'Seo Keywords',
        ];
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'slug' => [
                'class' => SlugBehaviors::class,
            ],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(Product::class, ['id' => 'model_id']);
    }
}
