<?php

namespace backend\modules\catalog\models;

use common\components\Yiit;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "influence".
 *
 * @property int $id
 * @property int $position
 * @property int $published
 * @property int $created_at
 * @property int $updated_at
 *
 * @property InfluenceLang[] $influenceLangs
 */
class Influence extends \common\models\base\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'influence';
    }

    public function getTitle()
    {
        return Yiit::tr('back/base', 'Influences');
    }

    public $langModel = InfluenceLang::class;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['position', 'published', 'created_at', 'updated_at'], 'integer'],
            [['published'], 'default', 'value' => true],
            [['alias'], 'unique']
        ];
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'timestamp' => [
                'class' => TimestampBehavior::class,
            ],
        ]);
    }

    public function getIndexConfig()
    {
        return [
            'id',
    //        ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'editLang.label',
                'label' => 'Название'
            ],
            [
                'attribute' => 'created_at',
                'value' => function ($data) {
                    return Yii::$app->formatter->asDatetime($data->created_at);
                },
                'filter' => false
            ],
            'published:boolean',
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'position',
                'editableOptions' =>
                    [
                        'formOptions' => ['action' => ['editable']],

                    ]
            ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{delete}'],
            [
                'class' => 'lav45\translate\grid\ActionColumn',
                'languages' => \backend\models\Language::getList(),
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'position' => 'Позиция',
            'published' => 'Опубликовано',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInfluenceLangs()
    {
        return $this->hasMany(InfluenceLang::class, ['model_id' => 'id']);
    }

    public static function getListFrontend()
    {
        return self::find()->joinWith(['lang'], false)->select(['influence_lang.label', 'position', 'alias', 'id'])->andWhere(['published'=> 1])->orderBy(['position'=>SORT_DESC])->asArray()->all();

    }

    public static function getList()
    {
        return self::find()->joinWith('lang')->select('influence_lang.label')->indexBy('id')->orderBy(['influence_lang.label' => SORT_ASC])->column();
    }
}
