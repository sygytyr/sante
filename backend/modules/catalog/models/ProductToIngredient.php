<?php

namespace backend\modules\catalog\models;

use Yii;

/**
 * This is the model class for table "product_to_ingredient".
 *
 * @property int $id
 * @property int $product_id
 * @property int $ingedient_id
 *
 * @property Ingredient $ingedient
 * @property Product $product
 */
class ProductToIngredient extends \common\models\base\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_to_ingredient';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'ingedient_id'], 'required'],
            [['product_id', 'ingedient_id'], 'integer'],
            [['ingedient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ingredient::className(), 'targetAttribute' => ['ingedient_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'ID продукта',
            'ingedient_id' => 'ID ингредиента',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngedient()
    {
        return $this->hasOne(Ingredient::className(), ['id' => 'ingedient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
