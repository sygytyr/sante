<?php
/**
 * Created by PhpStorm.
 * User: viktor
 * Date: 05.11.18
 * Time: 15:09
 */

namespace backend\components;


use yii\base\Widget;

class AdminMenuWidget extends Widget
{
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('menu');
    }
}