<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 02.02.2019
 * Time: 14:46
 */

namespace backend\components\behaviors;


use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\web\BadRequestHttpException;

class SlugBehaviors extends Behavior
{

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'createSlug',
            ActiveRecord::EVENT_AFTER_UPDATE => 'createSlug',
        ];
    }

    public function createSlug()
    {
        $parent = $this->owner->model;


        if (!$parent->alias) {

            $slug = SluggableBehavior::translit(trim($this->owner->label ?? 'slug'));
            $parent->alias = $slug;
            if (!$parent->validate()) {
                $parent->alias = substr($slug, 0, 69 - 2 * strlen($parent->id)) . '-' . $parent->id;
            }

            if (!$parent->save()) {

                throw new BadRequestHttpException(implode(',', $parent->getFirstErrors()));
            }

        }
    }
}