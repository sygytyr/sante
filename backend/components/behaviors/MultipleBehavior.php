<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 25.05.2019
 * Time: 17:02
 */

namespace backend\components\behaviors;


use ReflectionClass;
use yii\base\Behavior;
use yii\db\ActiveRecord;

class MultipleBehavior extends Behavior
{
    public $dataName;
    public $modelClass;
    public $fieldName;

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
        ];
    }

    public function afterSave()
    {
        $nameClass = (new ReflectionClass($this->owner))->getShortName();
        $data = \Yii::$app->request->post()[$nameClass][$this->dataName] ?? [];
        $oldData = $this->modelClass::findAll([$this->fieldName => $this->owner->id]);

            if (is_array($data)) {
                foreach ($data as $row) {

                    $newRow = new $this->modelClass;
                    $newRow->load($row, '');
                    $newRow->{$this->fieldName} = $this->owner->id;
                    $newRow->save();
                }
            }

        foreach ($oldData as $oldRow){
            $oldRow->delete();
        }

    }
}