<?php
/**
 * Created by PhpStorm.
 * User: viktor
 * Date: 05.11.18
 * Time: 16:16
 */

use yii\helpers\Html;
use kartik\grid\GridView;


$this->title = $searchModel->getTitle();
$this->params['breadcrumbs'][] = $this->title;
/*$langItems = is_null($modelSearch->langModel) ? '' : [
    'class' => 'lav45\translate\grid\ActionColumn',
    'languages' => \backend\models\Language::getList(),
];*/

?>
<div class="">
    <h1><?= $this->title ?></h1>
    <div class="row">
    <?php if ($canCreate) { ?>
        <div class="col-sm-6">
        <p>
            <?= Html::a(\common\components\Yiit::tr('back/base', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        </div>
    <?php } ?>
    
    <?php if ($canImport) { ?>
      <div class="col-sm-6 text-right">
        <p>
            <?= Html::a(\common\components\Yiit::tr('back/base', 'Import'), ['import'], ['class' => 'btn btn-danger']) ?>
        </p>
        </div>
    <?php } ?>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $searchModel->getIndexConfig(),
    ]) ?>


</div>


