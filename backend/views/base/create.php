<?php

use common\components\Yiit;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\service\models\ServiceCategory */

$this->title = Yiit::tr('back/base', 'Create') . ' ' . $model->getTitle();
$this->params['breadcrumbs'][] = ['label' => $model->getTitle(), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render($viewPathModule.'_form', [
        'model' => $model,
    ]) ?>

</div>
