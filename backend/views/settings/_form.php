<?php

use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Settings */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'key')->textInput(['maxlength' => true, 'readonly' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>

    <?php switch ($model->type) {
        case \common\models\Settings::TYPE_STRING:
            echo $form->field($model, 'label')->textInput(['maxlength' => true]);
            break;
        case \common\models\Settings::TYPE_FILE:
            echo $form->field($model, 'label')->widget(\mihaildev\elfinder\InputFile::className(), [
                'language' => Yii::$app->language,
                'controller' => 'elfinder',
                'filter' => 'image',
                'template' => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                'options' => ['class' => 'form-control image-change'],
                'buttonOptions' => ['class' => 'btn btn-default'],
                'multiple' => false
            ]);
            break;
        case \common\models\Settings::TYPE_TEXT:
            echo $form->field($model, "label")->widget(CKEditor::className(), [
                'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                    'filebrowserImageBrowseUrl' => yii\helpers\Url::to([
                        '/elfinder/manager',
                        'view-mode' => 'iframe',
                        'select-type' => 'ckeditor'
                    ]),
                ]),
            ]);
            break;
        default:
            echo $form->field($model, 'label')->textInput(['maxlength' => true]);
            break;
    } ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
