<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SourceMessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Source Messages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="source-message-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <!--<?= Html::a('Create Source Message', ['create'], ['class' => 'btn btn-success']) ?>-->
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'category',
                'value' => function ($data) {
                    return $data->category ?? null;
                },
                'filter' => \common\models\SourceMessage::getFilter()
            ],
            [
                'attribute' => 'label',
                'label' => Yii::t('back/language', 'Translation'),
                'format' => 'text',
                'value' => function ($data) {
                    $messages = [];
                    foreach ($data->messages as $message) {

                        if (!!$message->translation) {
                            $messages[] = trim($message->translation, ',');
                        }
                    }
                    return implode(', ', $messages);
                }
            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}'],
        ],
    ]); ?>
</div>
