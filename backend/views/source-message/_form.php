<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SourceMessage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="source-message-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'category')->textInput(['readonly'=>true]) ?>

    <?= $form->field($model, 'message')->textInput(['readonly'=>true]) ?>

    <?php foreach ($messages as $index=>$lang) { ?>

        <div class="hidden">
            <?= $form->field($lang, "[$index]language")->textInput() ?>
        </div>
        <?= $form->field($lang, "[$index]translation", ['template'=>'<div>{label} <strong>'.$lang->language.'</strong></div>{input}{error}'])->textarea(['rows'=>4]) ?>

    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
