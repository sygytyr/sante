<?php

/* @var $this \yii\web\View */

/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use \common\components\Yiit;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="/" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>A</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Admin</b>Module</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigations></span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                            <span class="hidden-xs"><?= Yii::$app->user->identity->username ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">

                                <p>
                                    <?= Yii::$app->user->identity->username ?>
                                    <small>На сайте
                                        с <?= Yii::$app->formatter->asDate(Yii::$app->user->identity->created_at) ?></small>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="<?= \yii\helpers\Url::to([
                                        '/rbac/user/update',
                                        'id' => Yii::$app->user->identity->id
                                    ]) ?>" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="<?= \yii\helpers\Url::to(['/site/logout']) ?>"
                                       class="btn btn-default btn-flat">Logout</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->

        <section class="sidebar">
            <?php if (Yii::$app->user->isGuest) { ?>
            <?php } else { ?>
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">

                    </div>
                    <div class="pull-left info">
                        <p><?= Yii::$app->user->identity->username ?></p>
                        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                    </div>
                </div>

                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">Main navigation</li>

                    <li class="treeview">
                        <a href="#">
                            <i class="fa  fa-flag-o"></i> <span>Переводы</span>
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                        <!--
                            <li>
                                <a href="<?= \yii\helpers\Url::to(['/language']) ?>">
                                    <i class="fa fa-circle-o"></i> <span>Languages</span>
                                </a>
                            </li>
                            -->
                            <li>
                                <a href="<?= \yii\helpers\Url::to(['/source-message']) ?>">
                                    <i class="fa fa-circle-o"></i> <span>Языковые пакеты</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?= \yii\helpers\Url::to(['/catalog/ingredient/index']) ?>">
                            <i class="fa fa-cog"></i> <span>Ингредиенты</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= \yii\helpers\Url::to(['/catalog/shop/index']) ?>">
                            <i class="fa fa-cog"></i> <span>Магазины</span>
                        </a>
                    </li>

                    <li>
                        <a href="<?= \yii\helpers\Url::to(['/catalog/type/index']) ?>">
                            <i class="fa fa-cog"></i> <span>Типы</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= \yii\helpers\Url::to(['/catalog/category/index']) ?>">
                            <i class="fa fa-cog"></i> <span>Категории</span>
                        </a>
                    </li>

                    <li>
                        <a href="<?= \yii\helpers\Url::to(['/catalog/influence/index']) ?>">
                            <i class="fa fa-cog"></i> <span>Действие</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= \yii\helpers\Url::to(['/catalog/series/index']) ?>">
                            <i class="fa fa-cog"></i> <span>Серии</span>
                        </a>
                    </li>

                    <li>
                        <a href="<?= \yii\helpers\Url::to(['/catalog/banner/index']) ?>">
                            <i class="fa fa-cog"></i> <span>Баннер на главной</span>
                        </a>
                    </li>

                    <li>
                        <a href="<?= \yii\helpers\Url::to(['/catalog/product/index']) ?>">
                            <i class="fa fa-cog"></i> <span>Продукты</span>
                        </a>
                    </li>


                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-cogs"></i> <span>Конфигурация</span>
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li>
                                <a href="<?= \yii\helpers\Url::to(['/settings']) ?>">
                                    <i class="fa fa-cog"></i> <span>Настройки</span>
                                </a>
                            </li>
                            <!--
                            <li>
                                <a href="<?= \yii\helpers\Url::to(['/rbac']) ?>">
                                    <i class="fa fa-users"></i> <span>Rbac</span>
                                </a>
                            </li>
                            -->
                            <!--TODO Delete on Prod
                            <li class="">
                                <a href="<?= \yii\helpers\Url::to(['/gii']) ?>">
                                    <i class="fa fa-cog"></i> <span>Gii</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="<?= \yii\helpers\Url::to(['/db-manager']) ?>">
                                    <i class="fa  fa-database"></i> <span>Db manager</span>
                                </a>
                            </li>
                            -->
                        </ul>
                    </li>


                </ul>
            <?php } ?>

        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <section class="content-header">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <section class="content">
                <div class="box box-primary">
                    <?= Alert::widget() ?>
                    <?= $content ?>
                </div>

            </section>
        </section>
    </div>


    <!-- /.content-wrapper -->
    <footer class="main-footer">

        <strong>Copyright &copy; 2018-<?= date("Y") ?>.</strong> All rights
        reserved.
    </footer>
    <div class="control-sidebar-bg"></div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
