-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: localhost    Database: sante
-- ------------------------------------------------------
-- Server version	5.7.13-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_assignment`
--

DROP TABLE IF EXISTS `auth_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  KEY `auth_assignment_user_id_idx` (`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_assignment`
--

LOCK TABLES `auth_assignment` WRITE;
/*!40000 ALTER TABLE `auth_assignment` DISABLE KEYS */;
INSERT INTO `auth_assignment` VALUES ('AdminAccess','1',1558786363);
/*!40000 ALTER TABLE `auth_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item`
--

LOCK TABLES `auth_item` WRITE;
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;
INSERT INTO `auth_item` VALUES ('/*',2,NULL,NULL,NULL,1558786363,1558786363),('AdminAccess',1,NULL,NULL,NULL,1558786363,1558786363);
/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item_child`
--

LOCK TABLES `auth_item_child` WRITE;
/*!40000 ALTER TABLE `auth_item_child` DISABLE KEYS */;
INSERT INTO `auth_item_child` VALUES ('AdminAccess','/*');
/*!40000 ALTER TABLE `auth_item_child` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_rule`
--

LOCK TABLES `auth_rule` WRITE;
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banner`
--

DROP TABLE IF EXISTS `banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `figure_type` int(11) DEFAULT NULL,
  `fon_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `series_id` int(11) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-banner-series` (`series_id`),
  CONSTRAINT `fk-banner-series` FOREIGN KEY (`series_id`) REFERENCES `series` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banner`
--

LOCK TABLES `banner` WRITE;
/*!40000 ALTER TABLE `banner` DISABLE KEYS */;
INSERT INTO `banner` VALUES (1,'/admin/uploads/mrp_a.jpg',1,'1',15,5,1,1558882526,1560170659),(2,'/admin/uploads/mrp_b.png',1,'1',7,0,1,1558882547,1560170673),(3,'/admin/uploads/mrp_c.png',1,'1',13,0,1,1558882569,1560170687),(4,'/admin/uploads/mrp_d.jpg',1,'1',4,0,1,1558882591,1560170700),(5,'/admin/uploads/mrp_e.jpg',1,'1',11,0,1,1558882606,1560170709);
/*!40000 ALTER TABLE `banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banner_lang`
--

DROP TABLE IF EXISTS `banner_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banner_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`model_id`,`language`),
  CONSTRAINT `fk-banner_lang-banner` FOREIGN KEY (`model_id`) REFERENCES `banner` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banner_lang`
--

LOCK TABLES `banner_lang` WRITE;
/*!40000 ALTER TABLE `banner_lang` DISABLE KEYS */;
INSERT INTO `banner_lang` VALUES (1,'ru','Dr. Santé 0%'),(2,'ru','Cucumber<br> Balance Control'),(3,'ru','Cannabis Hair<br> Oil Therapy'),(4,'ru','Detox Hair'),(5,'ru','Aqua Thermal');
/*!40000 ALTER TABLE `banner_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fon_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (3,'#ffffff','lico',4,1,1558801052,1558882249),(4,'#ffffff','volosy',3,1,1558801068,1558801068),(5,'#ffffff','telo',2,1,1558801079,1558801099),(6,'#ffffff','dlya-detey',0,1,1558801091,1558801091);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_lang`
--

DROP TABLE IF EXISTS `category_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8_unicode_ci,
  `seo_keywords` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`model_id`,`language`),
  CONSTRAINT `fk-category_lang-shop` FOREIGN KEY (`model_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_lang`
--

LOCK TABLES `category_lang` WRITE;
/*!40000 ALTER TABLE `category_lang` DISABLE KEYS */;
INSERT INTO `category_lang` VALUES (3,'ru','Лицо','','Уход за лицом','',''),(4,'ru','Волосы','','','',''),(5,'ru','Тело','','','',''),(6,'ru','Для детей','','','','');
/*!40000 ALTER TABLE `category_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `imagemanager`
--

DROP TABLE IF EXISTS `imagemanager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imagemanager` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fileName` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `fileHash` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  `createdBy` int(10) unsigned DEFAULT NULL,
  `modifiedBy` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `imagemanager`
--

LOCK TABLES `imagemanager` WRITE;
/*!40000 ALTER TABLE `imagemanager` DISABLE KEYS */;
/*!40000 ALTER TABLE `imagemanager` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `influence`
--

DROP TABLE IF EXISTS `influence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `influence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `influence`
--

LOCK TABLES `influence` WRITE;
/*!40000 ALTER TABLE `influence` DISABLE KEYS */;
INSERT INTO `influence` VALUES (6,0,1,1558805411,'tonizaciya',1558805411),(7,0,1,1558805417,'matirovanie',1558805417),(8,0,1,1558805422,'vosstanovlenie',1558805422),(9,0,1,1558805426,'demakiyazh',1558805426),(10,0,1,1558805431,'ochischenie',1558805431),(11,0,1,1558805435,'pitanie',1558805435),(12,0,1,1558805440,'uvlazhnenie',1558805440),(13,0,1,1558805444,'antivozrastnoy',1558805444);
/*!40000 ALTER TABLE `influence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `influence_lang`
--

DROP TABLE IF EXISTS `influence_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `influence_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`model_id`,`language`),
  CONSTRAINT `fk-influence_lang-influence` FOREIGN KEY (`model_id`) REFERENCES `influence` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `influence_lang`
--

LOCK TABLES `influence_lang` WRITE;
/*!40000 ALTER TABLE `influence_lang` DISABLE KEYS */;
INSERT INTO `influence_lang` VALUES (6,'ru','Тонизация'),(7,'ru','Матирование'),(8,'ru','Восстановление'),(9,'ru','Демакияж'),(10,'ru','Очищение'),(11,'ru','Питание'),(12,'ru','Увлажнение'),(13,'ru','Антивозрастной');
/*!40000 ALTER TABLE `influence_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredient`
--

DROP TABLE IF EXISTS `ingredient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL,
  `fon_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `position_home` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) DEFAULT '1',
  `show_on_home` tinyint(1) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredient`
--

LOCK TABLES `ingredient` WRITE;
/*!40000 ALTER TABLE `ingredient` DISABLE KEYS */;
INSERT INTO `ingredient` VALUES (3,'/admin/uploads/cocos.png',1,'#ffffff','#C4A0A0',10,10,1,1,1558809293,1558850653),(4,'/admin/uploads/canabis_a.png',2,'#ffffff','#B7C19F',9,9,1,1,1558809414,1558851129),(5,'/admin/uploads/olive_a.png',3,'#ffffff','#A3C1A3',0,8,0,1,1558809449,1558851711),(6,'/admin/uploads/aloe_a.png',2,'#ffffff','#9BB78E',7,7,1,1,1558809503,1558851380),(7,'/admin/uploads/cucumber_a.png',1,'#ffffff','#A1B782',6,6,1,1,1558809558,1558851455),(8,'/admin/uploads/makadamia_a.png',3,'#ffffff','#D6B683',5,5,1,1,1558809592,1558851486),(9,'/admin/uploads/godji_a.png',2,'#ffffff','#CAA2A2',4,4,1,1,1558809634,1558851702),(10,'/admin/uploads/avokado_a.png',1,'#ffffff','#Авокадо',3,3,1,1,1558809667,1558851693),(11,'/admin/uploads/orange_a.png',3,'#ffffff','#ffffff',8,0,1,0,1558851312,1558851312),(12,'/admin/uploads/argan_a.png',3,'#ffffff','#ffffff',2,0,1,0,1558851642,1558851642);
/*!40000 ALTER TABLE `ingredient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredient_images`
--

DROP TABLE IF EXISTS `ingredient_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredient_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ingredient_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk-ingredient_images-ingredient` (`ingredient_id`),
  CONSTRAINT `fk-ingredient_images-ingredient` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredient` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredient_images`
--

LOCK TABLES `ingredient_images` WRITE;
/*!40000 ALTER TABLE `ingredient_images` DISABLE KEYS */;
INSERT INTO `ingredient_images` VALUES (49,3,'/admin/uploads/dop_b.png',1),(50,3,'',2),(51,3,'',3),(52,3,'/admin/uploads/dop_d.png',4),(53,4,'',1),(54,4,'',2),(55,4,'/admin/uploads/dop_e.png',3),(56,4,'',4),(69,11,'',1),(70,11,'/admin/uploads/dop_f.png',2),(71,11,'',3),(72,11,'',4),(73,6,'/admin/uploads/dop_g.png',1),(74,6,'',2),(75,6,'',3),(76,6,'/admin/uploads/dop_i.png',4),(77,7,'',1),(78,7,'/admin/uploads/dop_h.png',2),(79,7,'/admin/uploads/dop_n.png',3),(80,7,'/admin/uploads/dop_m.png',4),(81,8,'',1),(82,8,'',2),(83,8,'/admin/uploads/dop_l.png',3),(84,8,'',4),(93,12,'',1),(94,12,'/admin/uploads/dop_s.png',2),(95,12,'/admin/uploads/dop_x.png',3),(96,12,'',4),(101,10,'',1),(102,10,'',2),(103,10,'/admin/uploads/dop_v.png',3),(104,10,'',4),(105,9,'/admin/uploads/dop_k.png',1),(106,9,'',2),(107,9,'',3),(108,9,'/admin/uploads/dop_j.png',4),(109,5,'',1),(110,5,'',2),(111,5,'',3),(112,5,'',4);
/*!40000 ALTER TABLE `ingredient_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredient_lang`
--

DROP TABLE IF EXISTS `ingredient_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredient_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8_unicode_ci,
  `seo_keywords` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`model_id`,`language`),
  CONSTRAINT `fk-ingredient_lang-ingredient` FOREIGN KEY (`model_id`) REFERENCES `ingredient` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredient_lang`
--

LOCK TABLES `ingredient_lang` WRITE;
/*!40000 ALTER TABLE `ingredient_lang` DISABLE KEYS */;
INSERT INTO `ingredient_lang` VALUES (3,'ru','Кокос','Главная особенность кокосового масла — способность глубоко проникать в структуру волоса. Человеческие волосы могут впитать за 1 час объем кокосового масла в количестве до 15 % от своей массы, а за 6 часов — до 20-25 %. Поэтому, выбирая средство по уходу за волосами, обращайте внимание на продукты с содержанием столь драгоценного компонента.','','',''),(4,'ru','Каннабис','Высокие технологии положены в основу каждой линейки продуктов. Индивидуальный подход к разным типам кожи позволил создать широкий ассортимент для удовлетворения самых требовательных клиентов. Доступная цена является еще одной положительной стороной серий данной марки.','','',''),(5,'ru','Олива','','','',''),(6,'ru','Алоэ вера','Гель, хранящийся в листьях растения алоэ вера, содержит гликопротеины, которые резко повышают способность кожи к восстановлению, уменьшая боль и воспаление. Еще одно мощное целительное вещество в алоэ вера — полисахариды, которые стимулируют регенерацию кожи.','','',''),(7,'ru','Огурец','Высокие технологии положены в основу каждой линейки продуктов. Индивидуальный подход к разным типам кожи позволил создать широкий ассортимент для удовлетворения самых требовательных клиентов. Доступная цена является еще одной положительной стороной серий данной марки.','','',''),(8,'ru','Макадамия','Высокие технологии положены в основу каждой линейки продуктов. Индивидуальный подход к разным типам кожи позволил создать широкий ассортимент для удовлетворения самых требовательных клиентов. Доступная цена является еще одной положительной стороной серий данной марки.','','',''),(9,'ru','Ягоды годжи','Высокие технологии положены в основу каждой линейки продуктов. Индивидуальный подход к разным типам кожи позволил создать широкий ассортимент для удовлетворения самых требовательных клиентов. Доступная цена является еще одной положительной стороной серий данной марки.','','',''),(10,'ru','Авокадо','Высокие технологии положены в основу каждой линейки продуктов. Индивидуальный подход к разным типам кожи позволил создать широкий ассортимент для удовлетворения самых требовательных клиентов. Доступная цена является еще одной положительной стороной серий данной марки.','','',''),(11,'ru','Апельсин','Издавна оливковое масло применяли для лечения разных заболеваний. Также славится оно и мощным увлажняющим воздействием на кожу и волосы. Масло, полученное из свежих зеленых оливок, богато антиоксидантами, особенно полифенолами. Больше всего их в оливковом масле холодного отжима, именно поэтому оно считается самым полезным.','','',''),(12,'ru','Масло арганы','Высокие технологии положены в основу каждой линейки продуктов. Индивидуальный подход к разным типам кожи позволил создать широкий ассортимент для удовлетворения самых требовательных клиентов. Доступная цена является еще одной положительной стороной серий данной марки.','','','');
/*!40000 ALTER TABLE `ingredient_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `language`
--

DROP TABLE IF EXISTS `language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `short_label` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `default` int(1) DEFAULT '0',
  `active` int(1) DEFAULT '1',
  `sort_order` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `language`
--

LOCK TABLES `language` WRITE;
/*!40000 ALTER TABLE `language` DISABLE KEYS */;
INSERT INTO `language` VALUES (1,'Eng','English','en',0,1,2),(2,'Ru','Russian','ru',1,1,1);
/*!40000 ALTER TABLE `language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `route` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `data` blob,
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`),
  CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `menu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `translation` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`,`language`),
  KEY `idx_message_language` (`language`),
  CONSTRAINT `fk_message_source_message` FOREIGN KEY (`id`) REFERENCES `source_message` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` VALUES (1,'ru','Categories'),(2,'ru','Create'),(3,'ru','Types'),(4,'ru','Ingredient'),(5,'ru','Influences'),(6,'ru','Products'),(7,'ru','Series'),(8,'ru','Update'),(9,'en','Philosophy'),(9,'ru','Philosophy'),(10,'en','Ingredients'),(10,'ru','Ingredients'),(11,'en','Where to buy'),(11,'ru','Where to buy'),(12,'ru','Shop'),(13,'ru','Type'),(14,'ru','Influence'),(15,'ru','Seria'),(16,'ru','All products'),(17,'ru','Settings'),(18,'ru','Banners'),(19,'en',''),(19,'ru','Назад'),(20,'en',''),(20,'ru','Вперед'),(21,'en',''),(21,'ru','Скрольте, чтобы узнать больше'),(22,'ru','Natural ingredients'),(23,'en',''),(23,'ru','Вдохновляем женщину быть красивой'),(24,'en',''),(24,'ru','Подробнее про ингредиенты'),(25,'en',''),(25,'ru','Натуральные<br> ингредиенты'),(26,'en',''),(26,'ru','Натуральные<br> ингредиенты'),(27,'en',''),(27,'ru','Высокие технологии положены в основу каждой линейки продуктов. Индивидуальный подход к разным типам кожи позволил создать широкий ассортимент для удовлетворения самых требовательных клиентов. Доступная цена является еще одной положительной стороной серий данной марки.'),(28,'en',''),(28,'ru','Философия<br> Dr. Santé'),(29,'en',''),(29,'ru','Высокие технологии положены в основу каждой линейки продуктов. Индивидуальный подход к разным типам кожи позволил создать широкий ассортимент для удовлетворения самых требовательных клиентов. Доступная цена является еще одной положительной стороной серий данной марки.'),(30,'en',''),(30,'ru','Что есть Красота? Ее эталоны, критерии и идеалы менялись с течением веков. Поэты и художники воспевали ее в своих произведениях, пытаясь показать в ней гармонию всего Мироздания. Еще Мухаммед Физули (1502-1562 гг.) уверенно заявлял: «Весь мир, в мотылька превратившись, летит на свечу Красоты», подразумевая под Красотой величайшее творенье гармоничного мира, в котором мы живем. Не утратило актуальности это изречение и сегодня.'),(31,'en',''),(31,'ru','Красота – это совершенство. Она не только радует глаз и чарует звуками слух. Это и гениальные музыкальные произведения, талантливые архитектурные ансамбли и художественные полотна, блестящие научные открытия. Красота наполняет гармонией все, что нас окружает. Она внешнее отражение внутреннего здоровья. И пусть в веках остались эпохи совершенствования и украшательства, истинная ценность, со времен древней Греции, осталась прежней – сохранение естественной красоты. '),(32,'en',''),(32,'ru','Красота — это плод свободной души и крепкого здоровья.'),(33,'en',''),(33,'ru','Шефер Л.'),(34,'en',''),(34,'ru','Стремительный современный мир создал все условия для созидания, но в тоже время, и разрушения Красоты. Загрязненная окружающая среда, стрессы, некачественные продукты питания и вода, электро-магнитные излучения и т.п. постепенно разрушают данную нам природой Красоту. Поэтому в последние годы, в мире наметился устойчивый тренд «возврата к природе». Использование экологически чистых материалов и технологий, натурального сырья и продуктов приобретают большую популярность. Ведь только находясь в естественной среде, человек сможет оставаться здоровым физически и духовно.'),(35,'en',''),(35,'ru','Среди ценностей Группы компаний «Эльфа» - здоровье, гармония, честность и экологичность, занимают первые позиции. Создавая уникальные рецептуры косметических средств, специалисты научно-исследовательской лаборатории Компании, используют прогрессивные технологии и инновационные ингредиенты, созданные самой Природой. Качественное сырье, уникальная рецептура, высокая культура производства – триада успеха наших косметических средств. Приобретая любой продукт, созданный специалистами нашей компании, можно быть уверенным в получении заявленного эффекта, что подтверждается и многочисленными исследованиями.'),(36,'en',''),(36,'ru','Подчеркните природную красоту и сохраните здоровье кожи и волос с продукцией Группы компаний «Эльфа». '),(37,'en',''),(37,'ru','Где купить'),(38,'en',''),(38,'ru','Вся наша продукция доступна в интернет-магазине elfashop.ua и магазинах партнеров.'),(39,'en',''),(39,'ru','Перейти'),(40,'en',''),(40,'ru','Описание'),(41,'en',''),(41,'ru','Применение'),(42,'en',''),(42,'ru','Состав'),(43,'en',''),(43,'ru','Где купить'),(44,'en',''),(44,'ru','Закрыть'),(45,'en',''),(45,'ru','Купить в интернет-магазине'),(46,'en',''),(46,'ru','Присоединяйся<br> к нашему сообществу'),(47,'en',''),(47,'ru','Дружеские советы, конкурсы и общение. Заходи!'),(48,'en',''),(48,'ru','Присоединиться к сообществу на Facebook'),(49,'ru','Details'),(50,'en',''),(50,'ru','Смотреть'),(51,'en',''),(51,'ru','Новинки'),(52,'en',''),(52,'ru','Откройте для себя новые продукты Dr. Santé'),(53,'en',''),(53,'ru','Тренды<br> этого года'),(54,'en',''),(54,'ru','Другие средства<br> из этой линейки'),(55,'en',''),(55,'ru','Тип средства'),(56,'en',''),(56,'ru','Действие'),(57,'en',''),(57,'ru','Снять все'),(58,'en',''),(58,'ru','Серия'),(59,'en',''),(59,'ru','Смотреть серию'),(60,'ru','Seo title'),(61,'ru','Seo description'),(62,'ru','Seo keywords'),(63,'ru','Seo title'),(64,'ru','Seo description'),(65,'ru','Seo keywords'),(66,'ru','Seo title'),(67,'ru','Seo description'),(68,'ru','Seo keywords'),(69,'ru','Series'),(70,'ru','Next series');
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1558786360),('m130524_201442_init',1558786362),('m180823_113212_add_foto_to_uset_table',1558786362),('m181021_163944_create_menu_table',1558786362),('m181021_164043_create_user',1558786362),('m181021_164412_rbac_init',1558786362),('m181021_164519_rbac_add_index_on_auth_assignment_user_id',1558786362),('m181021_164636_create_ImageManager_table',1558786362),('m181021_164707_addBlameableBehavior',1558786363),('m181021_165251_create_user_role_add_user',1558786363),('m181021_181615_create_translation_table',1558786363),('m181021_182507_create_language_table',1558786363),('m181105_111430_create_settings_table',1558786364),('m181105_135139_insert_default_language',1558786364),('m190222_150827_alter_settings_table',1558786364),('m190512_162842_create_shop_table',1558786364),('m190512_165024_create_type_table',1558786364),('m190512_194448_create_category_table',1558786364),('m190512_194724_create_influence_table',1558786365),('m190512_194936_create_series_table',1558786365),('m190512_195126_create_banner_table',1558786365),('m190512_195415_create_ingredient_table',1558786365),('m190513_141812_add_column_to_shop_table',1558786365),('m190513_195058_create_product_table',1558786366),('m190525_131148_create_product_to_ingredient_table',1558790012);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entity_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `series_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `position_home` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) DEFAULT '1',
  `show_on_home` tinyint(1) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`),
  KEY `fk-product-series` (`series_id`),
  CONSTRAINT `fk-product-series` FOREIGN KEY (`series_id`) REFERENCES `series` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'/admin/uploads/prod_g.png','maska-skrab',NULL,7,0,4,1,1,1558857917,1559636105),(2,'/admin/uploads/prod_d.png','antibakterialnyy-tonik',NULL,7,0,0,1,1,1558868202,1558868202),(3,'/admin/uploads/prod_d.png','antibakterialnyy-tonik-3',NULL,7,0,0,1,1,1558868239,1558868239),(4,'/admin/uploads/prod_d.png','antibakterialnyy-tonik-4',NULL,7,0,0,1,1,1558868276,1558868276);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_lang`
--

DROP TABLE IF EXISTS `product_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `volume` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `application` text COLLATE utf8_unicode_ci,
  `sklad` text COLLATE utf8_unicode_ci,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8_unicode_ci,
  `seo_keywords` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`model_id`,`language`),
  CONSTRAINT `fk-product_lang-product` FOREIGN KEY (`model_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_lang`
--

LOCK TABLES `product_lang` WRITE;
/*!40000 ALTER TABLE `product_lang` DISABLE KEYS */;
INSERT INTO `product_lang` VALUES (1,'ru','Маска-скраб','75 мл','#1 Маска-скраб мягко удаляет ороговевшие частицы вместе с излишками жира, предотвращая закупоривание пор. Разглаживает ее тон.','#2 Маска-скраб мягко удаляет ороговевшие частицы вместе с излишками жира, предотвращая закупоривание пор. Разглаживает ее тон.','#3 Маска-скраб мягко удаляет ороговевшие частицы вместе с излишками жира, предотвращая закупоривание пор. Разглаживает ее тон.','','',''),(2,'ru','Антибактериальный тоник','220 мл','#1 Маска-скраб мягко удаляет ороговевшие частицы вместе с излишками жира, предотвращая закупоривание пор. Разглаживает ее тон.','#2 Маска-скраб мягко удаляет ороговевшие частицы вместе с излишками жира, предотвращая закупоривание пор. Разглаживает ее тон.','#3 Маска-скраб мягко удаляет ороговевшие частицы вместе с излишками жира, предотвращая закупоривание пор. Разглаживает ее тон.','','',''),(3,'ru','Антибактериальный тоник','220 мл','#1 Маска-скраб мягко удаляет ороговевшие частицы вместе с излишками жира, предотвращая закупоривание пор. Разглаживает ее тон.','#2 Маска-скраб мягко удаляет ороговевшие частицы вместе с излишками жира, предотвращая закупоривание пор. Разглаживает ее тон.','#3 Маска-скраб мягко удаляет ороговевшие частицы вместе с излишками жира, предотвращая закупоривание пор. Разглаживает ее тон.','','',''),(4,'ru','Антибактериальный тоник','220 мл','#1 Маска-скраб мягко удаляет ороговевшие частицы вместе с излишками жира, предотвращая закупоривание пор. Разглаживает ее тон.','#2 Маска-скраб мягко удаляет ороговевшие частицы вместе с излишками жира, предотвращая закупоривание пор. Разглаживает ее тон.','#3 Маска-скраб мягко удаляет ороговевшие частицы вместе с излишками жира, предотвращая закупоривание пор. Разглаживает ее тон.','','',''),(5,'ru','ewrerwerw','','',NULL,NULL,'','',''),(6,'ru','ewrerwerw','','',NULL,NULL,'','',''),(7,'ru','ewrerwerw','','',NULL,NULL,'','',''),(8,'ru','ewrerwerw','','',NULL,NULL,'','',''),(9,'ru','ewrerwerw','','',NULL,NULL,'','',''),(10,'ru','ewrerwerw','','',NULL,NULL,'','',''),(11,'ru','ewrerwerw','','',NULL,NULL,'','','');
/*!40000 ALTER TABLE `product_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_to_category`
--

DROP TABLE IF EXISTS `product_to_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_to_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-product_to_category-product` (`product_id`),
  KEY `fk-product_to_category-category` (`category_id`),
  CONSTRAINT `fk-product_to_category-category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk-product_to_category-product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_to_category`
--

LOCK TABLES `product_to_category` WRITE;
/*!40000 ALTER TABLE `product_to_category` DISABLE KEYS */;
INSERT INTO `product_to_category` VALUES (6,2,3),(7,3,3),(9,4,3),(11,1,4),(12,1,3);
/*!40000 ALTER TABLE `product_to_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_to_influence`
--

DROP TABLE IF EXISTS `product_to_influence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_to_influence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `influence_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-product_to_influence-product` (`product_id`),
  KEY `fk-product_to_influence-influence` (`influence_id`),
  CONSTRAINT `fk-product_to_influence-influence` FOREIGN KEY (`influence_id`) REFERENCES `influence` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk-product_to_influence-product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_to_influence`
--

LOCK TABLES `product_to_influence` WRITE;
/*!40000 ALTER TABLE `product_to_influence` DISABLE KEYS */;
INSERT INTO `product_to_influence` VALUES (8,2,13),(9,2,9),(10,2,10),(11,2,6),(12,3,13),(13,3,9),(14,3,10),(15,3,6),(20,4,13),(21,4,9),(22,4,10),(23,4,6),(27,1,13),(28,1,8),(29,1,7);
/*!40000 ALTER TABLE `product_to_influence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_to_ingredient`
--

DROP TABLE IF EXISTS `product_to_ingredient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_to_ingredient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `ingedient_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-product_to_ingredient-product` (`product_id`),
  KEY `fk-product_to_ingredient-ingredient` (`ingedient_id`),
  CONSTRAINT `fk-product_to_ingredient-ingredient` FOREIGN KEY (`ingedient_id`) REFERENCES `ingredient` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk-product_to_ingredient-product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_to_ingredient`
--

LOCK TABLES `product_to_ingredient` WRITE;
/*!40000 ALTER TABLE `product_to_ingredient` DISABLE KEYS */;
INSERT INTO `product_to_ingredient` VALUES (6,2,11),(7,2,4),(8,2,3),(9,3,11),(10,3,4),(11,3,3),(15,4,11),(16,4,4),(17,4,3),(19,1,10);
/*!40000 ALTER TABLE `product_to_ingredient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_to_shop`
--

DROP TABLE IF EXISTS `product_to_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_to_shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-product_to_shop-product` (`product_id`),
  KEY `fk-product_to_shop-shop` (`shop_id`),
  CONSTRAINT `fk-product_to_shop-product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk-product_to_shop-shop` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_to_shop`
--

LOCK TABLES `product_to_shop` WRITE;
/*!40000 ALTER TABLE `product_to_shop` DISABLE KEYS */;
INSERT INTO `product_to_shop` VALUES (25,1,4,'2'),(26,1,6,'3'),(27,1,5,'4'),(28,1,3,'5');
/*!40000 ALTER TABLE `product_to_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_to_type`
--

DROP TABLE IF EXISTS `product_to_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_to_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-product_to_type-product` (`product_id`),
  KEY `fk-product_to_type-type` (`type_id`),
  CONSTRAINT `fk-product_to_type-product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk-product_to_type-type` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_to_type`
--

LOCK TABLES `product_to_type` WRITE;
/*!40000 ALTER TABLE `product_to_type` DISABLE KEYS */;
INSERT INTO `product_to_type` VALUES (10,2,6),(11,3,6),(13,4,6),(17,1,13),(18,1,3),(19,1,10);
/*!40000 ALTER TABLE `product_to_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `series`
--

DROP TABLE IF EXISTS `series`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `series` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `figure_type` int(11) DEFAULT NULL,
  `fon_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) DEFAULT '1',
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `series`
--

LOCK TABLES `series` WRITE;
/*!40000 ALTER TABLE `series` DISABLE KEYS */;
INSERT INTO `series` VALUES (2,'',1,'#ffffff',0,1,'senskin',1558805469,1558805469),(3,'',1,'#ffffff',0,1,'oxygen',1558805475,1558805475),(4,'',1,'#ffffff',0,1,'oily-rich',1558805486,1558805486),(5,'',1,'#ffffff',0,1,'pure-code',1558805494,1558805494),(6,'',1,'#ffffff',0,1,'goji-age-control',1558805507,1558805507),(7,'',1,'#ffffff',0,1,'cucumber-balance-control',1558805520,1558805520),(8,'',1,'#ffffff',0,1,'collagen',1558805528,1558805528),(9,'',1,'#ffffff',0,1,'cc-cream',1558805537,1558805537),(10,'',1,'#ffffff',0,1,'bb-cream',1558805546,1558805546),(11,'',1,'#ffffff',0,1,'aqua-thermal',1558805556,1558805556),(12,'',1,'#ffffff',0,1,'argan-oil',1558805564,1558805564),(13,'',1,'#ffffff',0,1,'age-control',1558805573,1558805574),(15,'',1,'#ffffff',0,1,'0-15',1558805591,1558805591);
/*!40000 ALTER TABLE `series` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `series_lang`
--

DROP TABLE IF EXISTS `series_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `series_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8_unicode_ci,
  `seo_keywords` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`model_id`,`language`),
  CONSTRAINT `fk-series_lang-series` FOREIGN KEY (`model_id`) REFERENCES `series` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `series_lang`
--

LOCK TABLES `series_lang` WRITE;
/*!40000 ALTER TABLE `series_lang` DISABLE KEYS */;
INSERT INTO `series_lang` VALUES (2,'ru','SENSkin','','','',''),(3,'ru','OxyGEN','','','',''),(4,'ru','Oily Rich','','','',''),(5,'ru','Pure code','','','',''),(6,'ru','Goji Age Control','','','',''),(7,'ru','Cucumber Balance Control','','','',''),(8,'ru','Collagen','','','',''),(9,'ru','CC Cream','','','',''),(10,'ru','BB Cream','','','',''),(11,'ru','Aqua Thermal','','','',''),(12,'ru','Argan Oil','','','',''),(13,'ru','Age control','','','',''),(15,'ru','0%','','','','');
/*!40000 ALTER TABLE `series_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `label` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'facebook_link',1,NULL,1558805930,1558805930,''),(2,'philosophy_image',2,'',1558852278,1558852331,'/uploads/philosophy.png'),(3,'followus_image',2,'',1558862065,1558862086,'/uploads/followus.png'),(4,'main_seo_image',2,NULL,1559067834,1559067834,''),(5,'ingredients_seo_image',2,NULL,1559067917,1559067917,''),(6,'philosophy_seo_image',2,NULL,1559067954,1559067954,'');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings_lang`
--

DROP TABLE IF EXISTS `settings_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings_lang` (
  `model_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`model_id`,`language`),
  CONSTRAINT `fk-settings_lang-settings` FOREIGN KEY (`model_id`) REFERENCES `settings` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings_lang`
--

LOCK TABLES `settings_lang` WRITE;
/*!40000 ALTER TABLE `settings_lang` DISABLE KEYS */;
/*!40000 ALTER TABLE `settings_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shop`
--

DROP TABLE IF EXISTS `shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ecommerce` tinyint(1) DEFAULT '1',
  `position` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shop`
--

LOCK TABLES `shop` WRITE;
/*!40000 ALTER TABLE `shop` DISABLE KEYS */;
INSERT INTO `shop` VALUES (3,'/admin/uploads/store_a.png',1,4,1,1558853607,1558853614,'#'),(4,'/admin/uploads/store_b.png',1,3,1,1558853635,1558853635,'#'),(5,'/admin/uploads/store_c.png',1,2,1,1558853660,1558853660,'#'),(6,'/admin/uploads/store_d.png',1,0,1,1558853675,1558853675,'#');
/*!40000 ALTER TABLE `shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shop_lang`
--

DROP TABLE IF EXISTS `shop_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8_unicode_ci,
  `seo_keywords` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`model_id`,`language`),
  CONSTRAINT `fk-shop_lang-shop` FOREIGN KEY (`model_id`) REFERENCES `shop` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shop_lang`
--

LOCK TABLES `shop_lang` WRITE;
/*!40000 ALTER TABLE `shop_lang` DISABLE KEYS */;
INSERT INTO `shop_lang` VALUES (3,'ru','ельфа','','',''),(4,'ru','Eva','','',''),(5,'ru','Parfums','','',''),(6,'ru','make up','','','');
/*!40000 ALTER TABLE `shop_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `source_message`
--

DROP TABLE IF EXISTS `source_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `source_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `idx_source_message_category` (`category`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `source_message`
--

LOCK TABLES `source_message` WRITE;
/*!40000 ALTER TABLE `source_message` DISABLE KEYS */;
INSERT INTO `source_message` VALUES (1,'back/base','Categories'),(2,'back/base','Create'),(3,'back/base','Types'),(4,'back/base','Ingredient'),(5,'back/base','Influences'),(6,'back/base','Products'),(7,'back/base','Series'),(8,'back/base','Update'),(9,'frontend/menu','Philosophy'),(10,'frontend/menu','Ingredients'),(11,'frontend/menu','Where to buy'),(12,'back/base','Shop'),(13,'frontend/menu','Type'),(14,'frontend/menu','Influence'),(15,'frontend/menu','Seria'),(16,'frontend/menu','All products'),(17,'back/base','Settings'),(18,'back/base','Banners'),(19,'frontend/banner_main','Back'),(20,'frontend/banner_main','Forward'),(21,'frontend/banner_main','Scroll to find more'),(22,'frontent/main_ingredients','Natural ingredients'),(23,'frontent/main_ingredients','Inspire woman to be beautifull'),(24,'frontent/main_ingredients','Details about ingredients'),(25,'frontent/main_ingredients','Natural<br> ingredients'),(26,'frontent/ingredients','Natural<br> ingredients'),(27,'frontent/ingredients','Content'),(28,'frontend/philosophy','Main title'),(29,'frontend/philosophy','Main subtitle'),(30,'frontend/philosophy','Subtitle 2'),(31,'frontend/philosophy','Subtitle 3'),(32,'frontend/philosophy','Main title 2'),(33,'frontend/philosophy','Author'),(34,'frontend/philosophy','Subtitle 4'),(35,'frontend/philosophy','Subtitle 5'),(36,'frontend/philosophy','Subtitle 6'),(37,'frontend/where_to_buy','Title'),(38,'frontend/where_to_buy','Sub title'),(39,'frontend/where_to_buy','Button'),(40,'frontent/product','Content'),(41,'frontent/product','Application'),(42,'frontent/product','Composition'),(43,'frontent/product','Where to buy'),(44,'frontent/product','Close'),(45,'frontent/product','Buy in store'),(46,'frontend/followus','Join us title'),(47,'frontend/followus','Join us subtitle'),(48,'frontend/followus','Join us facebook'),(49,'frontend/product','Details'),(50,'frontent/product','Details'),(51,'frontend/main_page','New product'),(52,'frontend/main_page','Open new product'),(53,'frontend/main_page','Trends this year'),(54,'frontent/product','Other products'),(55,'frontend/category','Type'),(56,'frontend/category','Influence'),(57,'frontend/category','Unselect all'),(58,'frontend/category','Series'),(59,'frontend/banner_main','Look series'),(60,'frontend/main','Seo title'),(61,'frontend/main','Seo description'),(62,'frontend/main','Seo keywords'),(63,'frontend/ingredients','Seo title'),(64,'frontend/ingredients','Seo description'),(65,'frontend/ingredients','Seo keywords'),(66,'frontend/philosophy','Seo title'),(67,'frontend/philosophy','Seo description'),(68,'frontend/philosophy','Seo keywords'),(69,'frontend/series','Series'),(70,'frontend/series','Next series');
/*!40000 ALTER TABLE `source_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type`
--

DROP TABLE IF EXISTS `type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `figure_type` int(11) DEFAULT NULL,
  `fon_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type`
--

LOCK TABLES `type` WRITE;
/*!40000 ALTER TABLE `type` DISABLE KEYS */;
INSERT INTO `type` VALUES (3,'',1,'#ffffff','balzam-dlya-gub',0,1,1558805187,1558805187),(4,'',1,'#ffffff','tonik',1,1,1558805259,1558805259),(5,'',1,'#ffffff','skrab',2,1,1558805275,1558805275),(6,'',1,'#ffffff','molochko',3,1,1558805287,1558805288),(7,'',1,'#ffffff','micelyarnyy-gel',4,1,1558805299,1558805299),(8,'',1,'#ffffff','micelyarnaya-voda',5,1,1558805312,1558805312),(9,'',1,'#ffffff','penka',6,1,1558805322,1558805322),(10,'',1,'#ffffff','maska-dlya-lica',7,1,1558805333,1558805333),(11,'',1,'#ffffff','loson',8,1,1558805343,1558805343),(12,'',1,'#ffffff','gel',9,1,1558805353,1558805353),(13,'',1,'#ffffff','antivozrastnoy-krem',10,1,1558805365,1558805365),(14,'',1,'#ffffff','nochnoy-krem',11,1,1558805380,1558805380),(15,'',1,'#ffffff','dnevnoy-krem',12,1,1558805391,1558805391);
/*!40000 ALTER TABLE `type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_lang`
--

DROP TABLE IF EXISTS `type_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`model_id`,`language`),
  CONSTRAINT `fk-type_lang-shop` FOREIGN KEY (`model_id`) REFERENCES `type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_lang`
--

LOCK TABLES `type_lang` WRITE;
/*!40000 ALTER TABLE `type_lang` DISABLE KEYS */;
INSERT INTO `type_lang` VALUES (3,'ru','Бальзам для губ',NULL),(4,'ru','Тоник',NULL),(5,'ru','Скраб',NULL),(6,'ru','Молочко',NULL),(7,'ru','Мицелярный гель',NULL),(8,'ru','Мицелярная вода',NULL),(9,'ru','Пенка',NULL),(10,'ru','Маска для лица',NULL),(11,'ru','Лосьон',NULL),(12,'ru','Гель',NULL),(13,'ru','Антивозрастной крем',NULL),(14,'ru','Ночной крем',NULL),(15,'ru','Дневной крем',NULL);
/*!40000 ALTER TABLE `type_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `foto_id` int(11) DEFAULT NULL COMMENT 'Foto id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','3fCcs6lSno8jhHseibKQHBs8ORWLSp6J','$2y$13$UZ6xItFWeGaGTVcXVO1tceOq22fY12.gYcumZBWZ5tBs9Vqq8dXtK',NULL,'sygytyr@gmail.com',10,1558786363,1558786363,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-10 12:53:07
