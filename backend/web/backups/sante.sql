-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Май 29 2019 г., 20:01
-- Версия сервера: 10.1.37-MariaDB
-- Версия PHP: 7.1.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `sante`
--

-- --------------------------------------------------------

--
-- Структура таблицы `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('AdminAccess', '1', 1558786363);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('/*', 2, NULL, NULL, NULL, 1558786363, 1558786363),
('AdminAccess', 1, NULL, NULL, NULL, 1558786363, 1558786363);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('AdminAccess', '/*');

-- --------------------------------------------------------

--
-- Структура таблицы `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `figure_type` int(11) DEFAULT NULL,
  `fon_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `series_id` int(11) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `banner`
--

INSERT INTO `banner` (`id`, `image`, `figure_type`, `fon_color`, `series_id`, `position`, `published`, `created_at`, `updated_at`) VALUES
(1, '/admin/uploads/philosophy.png', 1, '#ffffff', 15, 5, 1, 1558882526, 1558882526),
(2, '/admin/uploads/philosophy.png', 1, '#ffffff', 7, 0, 1, 1558882547, 1558882547),
(3, '/admin/uploads/philosophy.png', 1, '#ffffff', 13, 0, 1, 1558882569, 1558882569),
(4, '/admin/uploads/philosophy.png', 1, '#ffffff', 4, 0, 1, 1558882591, 1558882591),
(5, '/admin/uploads/philosophy.png', 1, '#ffffff', 11, 0, 1, 1558882606, 1558882606);

-- --------------------------------------------------------

--
-- Структура таблицы `banner_lang`
--

CREATE TABLE `banner_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `banner_lang`
--

INSERT INTO `banner_lang` (`model_id`, `language`, `label`) VALUES
(1, 'ru', 'Dr. Santé 0%'),
(2, 'ru', 'Cucumber<br> Balance Control'),
(3, 'ru', 'Cannabis Hair<br> Oil Therapy'),
(4, 'ru', 'Detox Hair'),
(5, 'ru', 'Aqua Thermal');

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `fon_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `fon_color`, `alias`, `position`, `published`, `created_at`, `updated_at`) VALUES
(3, '#ffffff', 'lico', 4, 1, 1558801052, 1558882249),
(4, '#ffffff', 'volosy', 3, 1, 1558801068, 1558801068),
(5, '#ffffff', 'telo', 2, 1, 1558801079, 1558801099),
(6, '#ffffff', 'dlya-detey', 0, 1, 1558801091, 1558801091);

-- --------------------------------------------------------

--
-- Структура таблицы `category_lang`
--

CREATE TABLE `category_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8_unicode_ci,
  `seo_keywords` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `category_lang`
--

INSERT INTO `category_lang` (`model_id`, `language`, `label`, `content`, `seo_title`, `seo_description`, `seo_keywords`) VALUES
(3, 'ru', 'Лицо', '', 'Уход за лицом', '', ''),
(4, 'ru', 'Волосы', '', '', '', ''),
(5, 'ru', 'Тело', '', '', '', ''),
(6, 'ru', 'Для детей', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `imagemanager`
--

CREATE TABLE `imagemanager` (
  `id` int(10) UNSIGNED NOT NULL,
  `fileName` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `fileHash` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  `createdBy` int(10) UNSIGNED DEFAULT NULL,
  `modifiedBy` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `influence`
--

CREATE TABLE `influence` (
  `id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `influence`
--

INSERT INTO `influence` (`id`, `position`, `published`, `created_at`, `alias`, `updated_at`) VALUES
(6, 0, 1, 1558805411, 'tonizaciya', 1558805411),
(7, 0, 1, 1558805417, 'matirovanie', 1558805417),
(8, 0, 1, 1558805422, 'vosstanovlenie', 1558805422),
(9, 0, 1, 1558805426, 'demakiyazh', 1558805426),
(10, 0, 1, 1558805431, 'ochischenie', 1558805431),
(11, 0, 1, 1558805435, 'pitanie', 1558805435),
(12, 0, 1, 1558805440, 'uvlazhnenie', 1558805440),
(13, 0, 1, 1558805444, 'antivozrastnoy', 1558805444);

-- --------------------------------------------------------

--
-- Структура таблицы `influence_lang`
--

CREATE TABLE `influence_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `influence_lang`
--

INSERT INTO `influence_lang` (`model_id`, `language`, `label`) VALUES
(6, 'ru', 'Тонизация'),
(7, 'ru', 'Матирование'),
(8, 'ru', 'Восстановление'),
(9, 'ru', 'Демакияж'),
(10, 'ru', 'Очищение'),
(11, 'ru', 'Питание'),
(12, 'ru', 'Увлажнение'),
(13, 'ru', 'Антивозрастной');

-- --------------------------------------------------------

--
-- Структура таблицы `ingredient`
--

CREATE TABLE `ingredient` (
  `id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL,
  `fon_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `position_home` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) DEFAULT '1',
  `show_on_home` tinyint(1) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `ingredient`
--

INSERT INTO `ingredient` (`id`, `image`, `template_id`, `fon_color`, `color`, `position`, `position_home`, `published`, `show_on_home`, `created_at`, `updated_at`) VALUES
(3, '/admin/uploads/cocos.png', 1, '#ffffff', '#C4A0A0', 10, 10, 1, 1, 1558809293, 1558850653),
(4, '/admin/uploads/canabis_a.png', 2, '#ffffff', '#B7C19F', 9, 9, 1, 1, 1558809414, 1558851129),
(5, '/admin/uploads/olive_a.png', 3, '#ffffff', '#A3C1A3', 0, 8, 0, 1, 1558809449, 1558851711),
(6, '/admin/uploads/aloe_a.png', 2, '#ffffff', '#9BB78E', 7, 7, 1, 1, 1558809503, 1558851380),
(7, '/admin/uploads/cucumber_a.png', 1, '#ffffff', '#A1B782', 6, 6, 1, 1, 1558809558, 1558851455),
(8, '/admin/uploads/makadamia_a.png', 3, '#ffffff', '#D6B683', 5, 5, 1, 1, 1558809592, 1558851486),
(9, '/admin/uploads/godji_a.png', 2, '#ffffff', '#CAA2A2', 4, 4, 1, 1, 1558809634, 1558851702),
(10, '/admin/uploads/avokado_a.png', 1, '#ffffff', '#Авокадо', 3, 3, 1, 1, 1558809667, 1558851693),
(11, '/admin/uploads/orange_a.png', 3, '#ffffff', '#ffffff', 8, 0, 1, 0, 1558851312, 1558851312),
(12, '/admin/uploads/argan_a.png', 3, '#ffffff', '#ffffff', 2, 0, 1, 0, 1558851642, 1558851642);

-- --------------------------------------------------------

--
-- Структура таблицы `ingredient_images`
--

CREATE TABLE `ingredient_images` (
  `id` int(11) NOT NULL,
  `ingredient_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `ingredient_images`
--

INSERT INTO `ingredient_images` (`id`, `ingredient_id`, `image`, `position`) VALUES
(49, 3, '/admin/uploads/dop_b.png', 1),
(50, 3, '', 2),
(51, 3, '', 3),
(52, 3, '/admin/uploads/dop_d.png', 4),
(53, 4, '', 1),
(54, 4, '', 2),
(55, 4, '/admin/uploads/dop_e.png', 3),
(56, 4, '', 4),
(69, 11, '', 1),
(70, 11, '/admin/uploads/dop_f.png', 2),
(71, 11, '', 3),
(72, 11, '', 4),
(73, 6, '/admin/uploads/dop_g.png', 1),
(74, 6, '', 2),
(75, 6, '', 3),
(76, 6, '/admin/uploads/dop_i.png', 4),
(77, 7, '', 1),
(78, 7, '/admin/uploads/dop_h.png', 2),
(79, 7, '/admin/uploads/dop_n.png', 3),
(80, 7, '/admin/uploads/dop_m.png', 4),
(81, 8, '', 1),
(82, 8, '', 2),
(83, 8, '/admin/uploads/dop_l.png', 3),
(84, 8, '', 4),
(93, 12, '', 1),
(94, 12, '/admin/uploads/dop_s.png', 2),
(95, 12, '/admin/uploads/dop_x.png', 3),
(96, 12, '', 4),
(101, 10, '', 1),
(102, 10, '', 2),
(103, 10, '/admin/uploads/dop_v.png', 3),
(104, 10, '', 4),
(105, 9, '/admin/uploads/dop_k.png', 1),
(106, 9, '', 2),
(107, 9, '', 3),
(108, 9, '/admin/uploads/dop_j.png', 4),
(109, 5, '', 1),
(110, 5, '', 2),
(111, 5, '', 3),
(112, 5, '', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `ingredient_lang`
--

CREATE TABLE `ingredient_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8_unicode_ci,
  `seo_keywords` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `ingredient_lang`
--

INSERT INTO `ingredient_lang` (`model_id`, `language`, `label`, `content`, `seo_title`, `seo_description`, `seo_keywords`) VALUES
(3, 'ru', 'Кокос', 'Главная особенность кокосового масла — способность глубоко проникать в структуру волоса. Человеческие волосы могут впитать за 1 час объем кокосового масла в количестве до 15 % от своей массы, а за 6 часов — до 20-25 %. Поэтому, выбирая средство по уходу за волосами, обращайте внимание на продукты с содержанием столь драгоценного компонента.', '', '', ''),
(4, 'ru', 'Каннабис', 'Высокие технологии положены в основу каждой линейки продуктов. Индивидуальный подход к разным типам кожи позволил создать широкий ассортимент для удовлетворения самых требовательных клиентов. Доступная цена является еще одной положительной стороной серий данной марки.', '', '', ''),
(5, 'ru', 'Олива', '', '', '', ''),
(6, 'ru', 'Алоэ вера', 'Гель, хранящийся в листьях растения алоэ вера, содержит гликопротеины, которые резко повышают способность кожи к восстановлению, уменьшая боль и воспаление. Еще одно мощное целительное вещество в алоэ вера — полисахариды, которые стимулируют регенерацию кожи.', '', '', ''),
(7, 'ru', 'Огурец', 'Высокие технологии положены в основу каждой линейки продуктов. Индивидуальный подход к разным типам кожи позволил создать широкий ассортимент для удовлетворения самых требовательных клиентов. Доступная цена является еще одной положительной стороной серий данной марки.', '', '', ''),
(8, 'ru', 'Макадамия', 'Высокие технологии положены в основу каждой линейки продуктов. Индивидуальный подход к разным типам кожи позволил создать широкий ассортимент для удовлетворения самых требовательных клиентов. Доступная цена является еще одной положительной стороной серий данной марки.', '', '', ''),
(9, 'ru', 'Ягоды годжи', 'Высокие технологии положены в основу каждой линейки продуктов. Индивидуальный подход к разным типам кожи позволил создать широкий ассортимент для удовлетворения самых требовательных клиентов. Доступная цена является еще одной положительной стороной серий данной марки.', '', '', ''),
(10, 'ru', 'Авокадо', 'Высокие технологии положены в основу каждой линейки продуктов. Индивидуальный подход к разным типам кожи позволил создать широкий ассортимент для удовлетворения самых требовательных клиентов. Доступная цена является еще одной положительной стороной серий данной марки.', '', '', ''),
(11, 'ru', 'Апельсин', 'Издавна оливковое масло применяли для лечения разных заболеваний. Также славится оно и мощным увлажняющим воздействием на кожу и волосы. Масло, полученное из свежих зеленых оливок, богато антиоксидантами, особенно полифенолами. Больше всего их в оливковом масле холодного отжима, именно поэтому оно считается самым полезным.', '', '', ''),
(12, 'ru', 'Масло арганы', 'Высокие технологии положены в основу каждой линейки продуктов. Индивидуальный подход к разным типам кожи позволил создать широкий ассортимент для удовлетворения самых требовательных клиентов. Доступная цена является еще одной положительной стороной серий данной марки.', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `language`
--

CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `short_label` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `default` int(1) DEFAULT '0',
  `active` int(1) DEFAULT '1',
  `sort_order` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `language`
--

INSERT INTO `language` (`id`, `short_label`, `label`, `locale`, `default`, `active`, `sort_order`) VALUES
(1, 'Eng', 'English', 'en', 0, 1, 2),
(2, 'Ru', 'Russian', 'ru', 1, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `route` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `data` blob
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `translation` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `message`
--

INSERT INTO `message` (`id`, `language`, `translation`) VALUES
(1, 'ru', 'Categories'),
(2, 'ru', 'Create'),
(3, 'ru', 'Types'),
(4, 'ru', 'Ingredient'),
(5, 'ru', 'Influences'),
(6, 'ru', 'Products'),
(7, 'ru', 'Series'),
(8, 'ru', 'Update'),
(9, 'en', 'Philosophy'),
(9, 'ru', 'Philosophy'),
(10, 'en', 'Ingredients'),
(10, 'ru', 'Ingredients'),
(11, 'en', 'Where to buy'),
(11, 'ru', 'Where to buy'),
(12, 'ru', 'Shop'),
(13, 'ru', 'Type'),
(14, 'ru', 'Influence'),
(15, 'ru', 'Seria'),
(16, 'ru', 'All products'),
(17, 'ru', 'Settings'),
(18, 'ru', 'Banners'),
(19, 'en', ''),
(19, 'ru', 'Назад'),
(20, 'en', ''),
(20, 'ru', 'Вперед'),
(21, 'en', ''),
(21, 'ru', 'Скрольте, чтобы узнать больше'),
(22, 'ru', 'Natural ingredients'),
(23, 'en', ''),
(23, 'ru', 'Вдохновляем женщину быть красивой'),
(24, 'en', ''),
(24, 'ru', 'Подробнее про ингредиенты'),
(25, 'en', ''),
(25, 'ru', 'Натуральные<br> ингредиенты'),
(26, 'en', ''),
(26, 'ru', 'Натуральные<br> ингредиенты'),
(27, 'en', ''),
(27, 'ru', 'Высокие технологии положены в основу каждой линейки продуктов. Индивидуальный подход к разным типам кожи позволил создать широкий ассортимент для удовлетворения самых требовательных клиентов. Доступная цена является еще одной положительной стороной серий данной марки.'),
(28, 'en', ''),
(28, 'ru', 'Философия<br> Dr. Santé'),
(29, 'en', ''),
(29, 'ru', 'Высокие технологии положены в основу каждой линейки продуктов. Индивидуальный подход к разным типам кожи позволил создать широкий ассортимент для удовлетворения самых требовательных клиентов. Доступная цена является еще одной положительной стороной серий данной марки.'),
(30, 'en', ''),
(30, 'ru', 'Что есть Красота? Ее эталоны, критерии и идеалы менялись с течением веков. Поэты и художники воспевали ее в своих произведениях, пытаясь показать в ней гармонию всего Мироздания. Еще Мухаммед Физули (1502-1562 гг.) уверенно заявлял: «Весь мир, в мотылька превратившись, летит на свечу Красоты», подразумевая под Красотой величайшее творенье гармоничного мира, в котором мы живем. Не утратило актуальности это изречение и сегодня.'),
(31, 'en', ''),
(31, 'ru', 'Красота – это совершенство. Она не только радует глаз и чарует звуками слух. Это и гениальные музыкальные произведения, талантливые архитектурные ансамбли и художественные полотна, блестящие научные открытия. Красота наполняет гармонией все, что нас окружает. Она внешнее отражение внутреннего здоровья. И пусть в веках остались эпохи совершенствования и украшательства, истинная ценность, со времен древней Греции, осталась прежней – сохранение естественной красоты. '),
(32, 'en', ''),
(32, 'ru', 'Красота — это плод свободной души и крепкого здоровья.'),
(33, 'en', ''),
(33, 'ru', 'Шефер Л.'),
(34, 'en', ''),
(34, 'ru', 'Стремительный современный мир создал все условия для созидания, но в тоже время, и разрушения Красоты. Загрязненная окружающая среда, стрессы, некачественные продукты питания и вода, электро-магнитные излучения и т.п. постепенно разрушают данную нам природой Красоту. Поэтому в последние годы, в мире наметился устойчивый тренд «возврата к природе». Использование экологически чистых материалов и технологий, натурального сырья и продуктов приобретают большую популярность. Ведь только находясь в естественной среде, человек сможет оставаться здоровым физически и духовно.'),
(35, 'en', ''),
(35, 'ru', 'Среди ценностей Группы компаний «Эльфа» - здоровье, гармония, честность и экологичность, занимают первые позиции. Создавая уникальные рецептуры косметических средств, специалисты научно-исследовательской лаборатории Компании, используют прогрессивные технологии и инновационные ингредиенты, созданные самой Природой. Качественное сырье, уникальная рецептура, высокая культура производства – триада успеха наших косметических средств. Приобретая любой продукт, созданный специалистами нашей компании, можно быть уверенным в получении заявленного эффекта, что подтверждается и многочисленными исследованиями.'),
(36, 'en', ''),
(36, 'ru', 'Подчеркните природную красоту и сохраните здоровье кожи и волос с продукцией Группы компаний «Эльфа». '),
(37, 'en', ''),
(37, 'ru', 'Где купить'),
(38, 'en', ''),
(38, 'ru', 'Вся наша продукция доступна в интернет-магазине elfashop.ua и магазинах партнеров.'),
(39, 'en', ''),
(39, 'ru', 'Перейти'),
(40, 'en', ''),
(40, 'ru', 'Описание'),
(41, 'en', ''),
(41, 'ru', 'Применение'),
(42, 'en', ''),
(42, 'ru', 'Состав'),
(43, 'en', ''),
(43, 'ru', 'Где купить'),
(44, 'en', ''),
(44, 'ru', 'Закрыть'),
(45, 'en', ''),
(45, 'ru', 'Купить в интернет-магазине'),
(46, 'en', ''),
(46, 'ru', 'Присоединяйся<br> к нашему сообществу'),
(47, 'en', ''),
(47, 'ru', 'Дружеские советы, конкурсы и общение. Заходи!'),
(48, 'en', ''),
(48, 'ru', 'Присоединиться к сообществу на Facebook'),
(49, 'ru', 'Details'),
(50, 'en', ''),
(50, 'ru', 'Смотреть'),
(51, 'en', ''),
(51, 'ru', 'Новинки'),
(52, 'en', ''),
(52, 'ru', 'Откройте для себя новые продукты Dr. Santé'),
(53, 'en', ''),
(53, 'ru', 'Тренды<br> этого года'),
(54, 'en', ''),
(54, 'ru', 'Другие средства<br> из этой линейки'),
(55, 'en', ''),
(55, 'ru', 'Тип средства'),
(56, 'en', ''),
(56, 'ru', 'Действие'),
(57, 'en', ''),
(57, 'ru', 'Снять все'),
(58, 'en', ''),
(58, 'ru', 'Серия'),
(59, 'en', ''),
(59, 'ru', 'Смотреть серию'),
(60, 'ru', 'Seo title'),
(61, 'ru', 'Seo description'),
(62, 'ru', 'Seo keywords'),
(63, 'ru', 'Seo title'),
(64, 'ru', 'Seo description'),
(65, 'ru', 'Seo keywords'),
(66, 'ru', 'Seo title'),
(67, 'ru', 'Seo description'),
(68, 'ru', 'Seo keywords'),
(69, 'ru', 'Series'),
(70, 'ru', 'Next series');

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1558786360),
('m130524_201442_init', 1558786362),
('m180823_113212_add_foto_to_uset_table', 1558786362),
('m181021_163944_create_menu_table', 1558786362),
('m181021_164043_create_user', 1558786362),
('m181021_164412_rbac_init', 1558786362),
('m181021_164519_rbac_add_index_on_auth_assignment_user_id', 1558786362),
('m181021_164636_create_ImageManager_table', 1558786362),
('m181021_164707_addBlameableBehavior', 1558786363),
('m181021_165251_create_user_role_add_user', 1558786363),
('m181021_181615_create_translation_table', 1558786363),
('m181021_182507_create_language_table', 1558786363),
('m181105_111430_create_settings_table', 1558786364),
('m181105_135139_insert_default_language', 1558786364),
('m190222_150827_alter_settings_table', 1558786364),
('m190512_162842_create_shop_table', 1558786364),
('m190512_165024_create_type_table', 1558786364),
('m190512_194448_create_category_table', 1558786364),
('m190512_194724_create_influence_table', 1558786365),
('m190512_194936_create_series_table', 1558786365),
('m190512_195126_create_banner_table', 1558786365),
('m190512_195415_create_ingredient_table', 1558786365),
('m190513_141812_add_column_to_shop_table', 1558786365),
('m190513_195058_create_product_table', 1558786366),
('m190525_131148_create_product_to_ingredient_table', 1558790012);

-- --------------------------------------------------------

--
-- Структура таблицы `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entity_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `series_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `position_home` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) DEFAULT '1',
  `show_on_home` tinyint(1) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `product`
--

INSERT INTO `product` (`id`, `image`, `alias`, `entity_id`, `series_id`, `position`, `position_home`, `published`, `show_on_home`, `created_at`, `updated_at`) VALUES
(1, '/admin/uploads/prod_g.png', 'maska-skrab', NULL, 7, 0, 4, 1, 1, 1558857917, 1558867633),
(2, '/admin/uploads/prod_d.png', 'antibakterialnyy-tonik', NULL, 7, 0, 0, 1, 1, 1558868202, 1558868202),
(3, '/admin/uploads/prod_d.png', 'antibakterialnyy-tonik-3', NULL, 7, 0, 0, 1, 1, 1558868239, 1558868239),
(4, '/admin/uploads/prod_d.png', 'antibakterialnyy-tonik-4', NULL, 7, 0, 0, 1, 1, 1558868276, 1558868276);

-- --------------------------------------------------------

--
-- Структура таблицы `product_lang`
--

CREATE TABLE `product_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `volume` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `application` text COLLATE utf8_unicode_ci,
  `sklad` text COLLATE utf8_unicode_ci,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8_unicode_ci,
  `seo_keywords` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `product_lang`
--

INSERT INTO `product_lang` (`model_id`, `language`, `label`, `volume`, `content`, `application`, `sklad`, `seo_title`, `seo_description`, `seo_keywords`) VALUES
(1, 'ru', 'Маска-скраб', '75 мл', '#1 Маска-скраб мягко удаляет ороговевшие частицы вместе с излишками жира, предотвращая закупоривание пор. Разглаживает ее тон.', '#2 Маска-скраб мягко удаляет ороговевшие частицы вместе с излишками жира, предотвращая закупоривание пор. Разглаживает ее тон.', '#3 Маска-скраб мягко удаляет ороговевшие частицы вместе с излишками жира, предотвращая закупоривание пор. Разглаживает ее тон.', '', '', ''),
(2, 'ru', 'Антибактериальный тоник', '220 мл', '#1 Маска-скраб мягко удаляет ороговевшие частицы вместе с излишками жира, предотвращая закупоривание пор. Разглаживает ее тон.', '#2 Маска-скраб мягко удаляет ороговевшие частицы вместе с излишками жира, предотвращая закупоривание пор. Разглаживает ее тон.', '#3 Маска-скраб мягко удаляет ороговевшие частицы вместе с излишками жира, предотвращая закупоривание пор. Разглаживает ее тон.', '', '', ''),
(3, 'ru', 'Антибактериальный тоник', '220 мл', '#1 Маска-скраб мягко удаляет ороговевшие частицы вместе с излишками жира, предотвращая закупоривание пор. Разглаживает ее тон.', '#2 Маска-скраб мягко удаляет ороговевшие частицы вместе с излишками жира, предотвращая закупоривание пор. Разглаживает ее тон.', '#3 Маска-скраб мягко удаляет ороговевшие частицы вместе с излишками жира, предотвращая закупоривание пор. Разглаживает ее тон.', '', '', ''),
(4, 'ru', 'Антибактериальный тоник', '220 мл', '#1 Маска-скраб мягко удаляет ороговевшие частицы вместе с излишками жира, предотвращая закупоривание пор. Разглаживает ее тон.', '#2 Маска-скраб мягко удаляет ороговевшие частицы вместе с излишками жира, предотвращая закупоривание пор. Разглаживает ее тон.', '#3 Маска-скраб мягко удаляет ороговевшие частицы вместе с излишками жира, предотвращая закупоривание пор. Разглаживает ее тон.', '', '', ''),
(5, 'ru', 'ewrerwerw', '', '', NULL, NULL, '', '', ''),
(6, 'ru', 'ewrerwerw', '', '', NULL, NULL, '', '', ''),
(7, 'ru', 'ewrerwerw', '', '', NULL, NULL, '', '', ''),
(8, 'ru', 'ewrerwerw', '', '', NULL, NULL, '', '', ''),
(9, 'ru', 'ewrerwerw', '', '', NULL, NULL, '', '', ''),
(10, 'ru', 'ewrerwerw', '', '', NULL, NULL, '', '', ''),
(11, 'ru', 'ewrerwerw', '', '', NULL, NULL, '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `product_to_category`
--

CREATE TABLE `product_to_category` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `product_to_category`
--

INSERT INTO `product_to_category` (`id`, `product_id`, `category_id`) VALUES
(5, 1, 3),
(6, 2, 3),
(7, 3, 3),
(9, 4, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `product_to_influence`
--

CREATE TABLE `product_to_influence` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `influence_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `product_to_influence`
--

INSERT INTO `product_to_influence` (`id`, `product_id`, `influence_id`) VALUES
(5, 1, 13),
(6, 1, 8),
(7, 1, 7),
(8, 2, 13),
(9, 2, 9),
(10, 2, 10),
(11, 2, 6),
(12, 3, 13),
(13, 3, 9),
(14, 3, 10),
(15, 3, 6),
(20, 4, 13),
(21, 4, 9),
(22, 4, 10),
(23, 4, 6);

-- --------------------------------------------------------

--
-- Структура таблицы `product_to_ingredient`
--

CREATE TABLE `product_to_ingredient` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `ingedient_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `product_to_ingredient`
--

INSERT INTO `product_to_ingredient` (`id`, `product_id`, `ingedient_id`) VALUES
(5, 1, 10),
(6, 2, 11),
(7, 2, 4),
(8, 2, 3),
(9, 3, 11),
(10, 3, 4),
(11, 3, 3),
(15, 4, 11),
(16, 4, 4),
(17, 4, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `product_to_shop`
--

CREATE TABLE `product_to_shop` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `product_to_shop`
--

INSERT INTO `product_to_shop` (`id`, `product_id`, `shop_id`, `link`) VALUES
(17, 1, 4, '2'),
(18, 1, 6, '3'),
(19, 1, 5, '4'),
(20, 1, 3, '5');

-- --------------------------------------------------------

--
-- Структура таблицы `product_to_type`
--

CREATE TABLE `product_to_type` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `product_to_type`
--

INSERT INTO `product_to_type` (`id`, `product_id`, `type_id`) VALUES
(7, 1, 13),
(8, 1, 3),
(9, 1, 10),
(10, 2, 6),
(11, 3, 6),
(13, 4, 6);

-- --------------------------------------------------------

--
-- Структура таблицы `series`
--

CREATE TABLE `series` (
  `id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `figure_type` int(11) DEFAULT NULL,
  `fon_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) DEFAULT '1',
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `series`
--

INSERT INTO `series` (`id`, `image`, `figure_type`, `fon_color`, `position`, `published`, `alias`, `created_at`, `updated_at`) VALUES
(2, '', 1, '#ffffff', 0, 1, 'senskin', 1558805469, 1558805469),
(3, '', 1, '#ffffff', 0, 1, 'oxygen', 1558805475, 1558805475),
(4, '', 1, '#ffffff', 0, 1, 'oily-rich', 1558805486, 1558805486),
(5, '', 1, '#ffffff', 0, 1, 'pure-code', 1558805494, 1558805494),
(6, '', 1, '#ffffff', 0, 1, 'goji-age-control', 1558805507, 1558805507),
(7, '', 1, '#ffffff', 0, 1, 'cucumber-balance-control', 1558805520, 1558805520),
(8, '', 1, '#ffffff', 0, 1, 'collagen', 1558805528, 1558805528),
(9, '', 1, '#ffffff', 0, 1, 'cc-cream', 1558805537, 1558805537),
(10, '', 1, '#ffffff', 0, 1, 'bb-cream', 1558805546, 1558805546),
(11, '', 1, '#ffffff', 0, 1, 'aqua-thermal', 1558805556, 1558805556),
(12, '', 1, '#ffffff', 0, 1, 'argan-oil', 1558805564, 1558805564),
(13, '', 1, '#ffffff', 0, 1, 'age-control', 1558805573, 1558805574),
(15, '', 1, '#ffffff', 0, 1, '0-15', 1558805591, 1558805591);

-- --------------------------------------------------------

--
-- Структура таблицы `series_lang`
--

CREATE TABLE `series_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8_unicode_ci,
  `seo_keywords` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `series_lang`
--

INSERT INTO `series_lang` (`model_id`, `language`, `label`, `content`, `seo_title`, `seo_description`, `seo_keywords`) VALUES
(2, 'ru', 'SENSkin', '', '', '', ''),
(3, 'ru', 'OxyGEN', '', '', '', ''),
(4, 'ru', 'Oily Rich', '', '', '', ''),
(5, 'ru', 'Pure code', '', '', '', ''),
(6, 'ru', 'Goji Age Control', '', '', '', ''),
(7, 'ru', 'Cucumber Balance Control', '', '', '', ''),
(8, 'ru', 'Collagen', '', '', '', ''),
(9, 'ru', 'CC Cream', '', '', '', ''),
(10, 'ru', 'BB Cream', '', '', '', ''),
(11, 'ru', 'Aqua Thermal', '', '', '', ''),
(12, 'ru', 'Argan Oil', '', '', '', ''),
(13, 'ru', 'Age control', '', '', '', ''),
(15, 'ru', '0%', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `label` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`id`, `key`, `type`, `description`, `created_at`, `updated_at`, `label`) VALUES
(1, 'facebook_link', 1, NULL, 1558805930, 1558805930, ''),
(2, 'philosophy_image', 2, '', 1558852278, 1558852331, '/uploads/philosophy.png'),
(3, 'followus_image', 2, '', 1558862065, 1558862086, '/uploads/followus.png'),
(4, 'main_seo_image', 2, NULL, 1559067834, 1559067834, ''),
(5, 'ingredients_seo_image', 2, NULL, 1559067917, 1559067917, ''),
(6, 'philosophy_seo_image', 2, NULL, 1559067954, 1559067954, '');

-- --------------------------------------------------------

--
-- Структура таблицы `settings_lang`
--

CREATE TABLE `settings_lang` (
  `model_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `shop`
--

CREATE TABLE `shop` (
  `id` int(11) NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ecommerce` tinyint(1) DEFAULT '1',
  `position` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `shop`
--

INSERT INTO `shop` (`id`, `logo`, `ecommerce`, `position`, `published`, `created_at`, `updated_at`, `link`) VALUES
(3, '/admin/uploads/store_a.png', 1, 4, 1, 1558853607, 1558853614, '#'),
(4, '/admin/uploads/store_b.png', 1, 3, 1, 1558853635, 1558853635, '#'),
(5, '/admin/uploads/store_c.png', 1, 2, 1, 1558853660, 1558853660, '#'),
(6, '/admin/uploads/store_d.png', 1, 0, 1, 1558853675, 1558853675, '#');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_lang`
--

CREATE TABLE `shop_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8_unicode_ci,
  `seo_keywords` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `shop_lang`
--

INSERT INTO `shop_lang` (`model_id`, `language`, `label`, `seo_title`, `seo_description`, `seo_keywords`) VALUES
(3, 'ru', 'ельфа', '', '', ''),
(4, 'ru', 'Eva', '', '', ''),
(5, 'ru', 'Parfums', '', '', ''),
(6, 'ru', 'make up', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `source_message`
--

CREATE TABLE `source_message` (
  `id` int(11) NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `source_message`
--

INSERT INTO `source_message` (`id`, `category`, `message`) VALUES
(1, 'back/base', 'Categories'),
(2, 'back/base', 'Create'),
(3, 'back/base', 'Types'),
(4, 'back/base', 'Ingredient'),
(5, 'back/base', 'Influences'),
(6, 'back/base', 'Products'),
(7, 'back/base', 'Series'),
(8, 'back/base', 'Update'),
(9, 'frontend/menu', 'Philosophy'),
(10, 'frontend/menu', 'Ingredients'),
(11, 'frontend/menu', 'Where to buy'),
(12, 'back/base', 'Shop'),
(13, 'frontend/menu', 'Type'),
(14, 'frontend/menu', 'Influence'),
(15, 'frontend/menu', 'Seria'),
(16, 'frontend/menu', 'All products'),
(17, 'back/base', 'Settings'),
(18, 'back/base', 'Banners'),
(19, 'frontend/banner_main', 'Back'),
(20, 'frontend/banner_main', 'Forward'),
(21, 'frontend/banner_main', 'Scroll to find more'),
(22, 'frontent/main_ingredients', 'Natural ingredients'),
(23, 'frontent/main_ingredients', 'Inspire woman to be beautifull'),
(24, 'frontent/main_ingredients', 'Details about ingredients'),
(25, 'frontent/main_ingredients', 'Natural<br> ingredients'),
(26, 'frontent/ingredients', 'Natural<br> ingredients'),
(27, 'frontent/ingredients', 'Content'),
(28, 'frontend/philosophy', 'Main title'),
(29, 'frontend/philosophy', 'Main subtitle'),
(30, 'frontend/philosophy', 'Subtitle 2'),
(31, 'frontend/philosophy', 'Subtitle 3'),
(32, 'frontend/philosophy', 'Main title 2'),
(33, 'frontend/philosophy', 'Author'),
(34, 'frontend/philosophy', 'Subtitle 4'),
(35, 'frontend/philosophy', 'Subtitle 5'),
(36, 'frontend/philosophy', 'Subtitle 6'),
(37, 'frontend/where_to_buy', 'Title'),
(38, 'frontend/where_to_buy', 'Sub title'),
(39, 'frontend/where_to_buy', 'Button'),
(40, 'frontent/product', 'Content'),
(41, 'frontent/product', 'Application'),
(42, 'frontent/product', 'Composition'),
(43, 'frontent/product', 'Where to buy'),
(44, 'frontent/product', 'Close'),
(45, 'frontent/product', 'Buy in store'),
(46, 'frontend/followus', 'Join us title'),
(47, 'frontend/followus', 'Join us subtitle'),
(48, 'frontend/followus', 'Join us facebook'),
(49, 'frontend/product', 'Details'),
(50, 'frontent/product', 'Details'),
(51, 'frontend/main_page', 'New product'),
(52, 'frontend/main_page', 'Open new product'),
(53, 'frontend/main_page', 'Trends this year'),
(54, 'frontent/product', 'Other products'),
(55, 'frontend/category', 'Type'),
(56, 'frontend/category', 'Influence'),
(57, 'frontend/category', 'Unselect all'),
(58, 'frontend/category', 'Series'),
(59, 'frontend/banner_main', 'Look series'),
(60, 'frontend/main', 'Seo title'),
(61, 'frontend/main', 'Seo description'),
(62, 'frontend/main', 'Seo keywords'),
(63, 'frontend/ingredients', 'Seo title'),
(64, 'frontend/ingredients', 'Seo description'),
(65, 'frontend/ingredients', 'Seo keywords'),
(66, 'frontend/philosophy', 'Seo title'),
(67, 'frontend/philosophy', 'Seo description'),
(68, 'frontend/philosophy', 'Seo keywords'),
(69, 'frontend/series', 'Series'),
(70, 'frontend/series', 'Next series');

-- --------------------------------------------------------

--
-- Структура таблицы `type`
--

CREATE TABLE `type` (
  `id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `figure_type` int(11) DEFAULT NULL,
  `fon_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `type`
--

INSERT INTO `type` (`id`, `image`, `figure_type`, `fon_color`, `alias`, `position`, `published`, `created_at`, `updated_at`) VALUES
(3, '', 1, '#ffffff', 'balzam-dlya-gub', 0, 1, 1558805187, 1558805187),
(4, '', 1, '#ffffff', 'tonik', 1, 1, 1558805259, 1558805259),
(5, '', 1, '#ffffff', 'skrab', 2, 1, 1558805275, 1558805275),
(6, '', 1, '#ffffff', 'molochko', 3, 1, 1558805287, 1558805288),
(7, '', 1, '#ffffff', 'micelyarnyy-gel', 4, 1, 1558805299, 1558805299),
(8, '', 1, '#ffffff', 'micelyarnaya-voda', 5, 1, 1558805312, 1558805312),
(9, '', 1, '#ffffff', 'penka', 6, 1, 1558805322, 1558805322),
(10, '', 1, '#ffffff', 'maska-dlya-lica', 7, 1, 1558805333, 1558805333),
(11, '', 1, '#ffffff', 'loson', 8, 1, 1558805343, 1558805343),
(12, '', 1, '#ffffff', 'gel', 9, 1, 1558805353, 1558805353),
(13, '', 1, '#ffffff', 'antivozrastnoy-krem', 10, 1, 1558805365, 1558805365),
(14, '', 1, '#ffffff', 'nochnoy-krem', 11, 1, 1558805380, 1558805380),
(15, '', 1, '#ffffff', 'dnevnoy-krem', 12, 1, 1558805391, 1558805391);

-- --------------------------------------------------------

--
-- Структура таблицы `type_lang`
--

CREATE TABLE `type_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `type_lang`
--

INSERT INTO `type_lang` (`model_id`, `language`, `label`, `content`) VALUES
(3, 'ru', 'Бальзам для губ', NULL),
(4, 'ru', 'Тоник', NULL),
(5, 'ru', 'Скраб', NULL),
(6, 'ru', 'Молочко', NULL),
(7, 'ru', 'Мицелярный гель', NULL),
(8, 'ru', 'Мицелярная вода', NULL),
(9, 'ru', 'Пенка', NULL),
(10, 'ru', 'Маска для лица', NULL),
(11, 'ru', 'Лосьон', NULL),
(12, 'ru', 'Гель', NULL),
(13, 'ru', 'Антивозрастной крем', NULL),
(14, 'ru', 'Ночной крем', NULL),
(15, 'ru', 'Дневной крем', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `foto_id` int(11) DEFAULT NULL COMMENT 'Foto id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `foto_id`) VALUES
(1, 'admin', '3fCcs6lSno8jhHseibKQHBs8ORWLSp6J', '$2y$13$UZ6xItFWeGaGTVcXVO1tceOq22fY12.gYcumZBWZ5tBs9Vqq8dXtK', NULL, 'sygytyr@gmail.com', 10, 1558786363, 1558786363, NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD KEY `auth_assignment_user_id_idx` (`user_id`);

--
-- Индексы таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Индексы таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Индексы таблицы `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Индексы таблицы `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-banner-series` (`series_id`);

--
-- Индексы таблицы `banner_lang`
--
ALTER TABLE `banner_lang`
  ADD PRIMARY KEY (`model_id`,`language`);

--
-- Индексы таблицы `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`);

--
-- Индексы таблицы `category_lang`
--
ALTER TABLE `category_lang`
  ADD PRIMARY KEY (`model_id`,`language`);

--
-- Индексы таблицы `imagemanager`
--
ALTER TABLE `imagemanager`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `influence`
--
ALTER TABLE `influence`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`);

--
-- Индексы таблицы `influence_lang`
--
ALTER TABLE `influence_lang`
  ADD PRIMARY KEY (`model_id`,`language`);

--
-- Индексы таблицы `ingredient`
--
ALTER TABLE `ingredient`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `ingredient_images`
--
ALTER TABLE `ingredient_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-ingredient_images-ingredient` (`ingredient_id`);

--
-- Индексы таблицы `ingredient_lang`
--
ALTER TABLE `ingredient_lang`
  ADD PRIMARY KEY (`model_id`,`language`);

--
-- Индексы таблицы `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`);

--
-- Индексы таблицы `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_message_language` (`language`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`),
  ADD KEY `fk-product-series` (`series_id`);

--
-- Индексы таблицы `product_lang`
--
ALTER TABLE `product_lang`
  ADD PRIMARY KEY (`model_id`,`language`);

--
-- Индексы таблицы `product_to_category`
--
ALTER TABLE `product_to_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-product_to_category-product` (`product_id`),
  ADD KEY `fk-product_to_category-category` (`category_id`);

--
-- Индексы таблицы `product_to_influence`
--
ALTER TABLE `product_to_influence`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-product_to_influence-product` (`product_id`),
  ADD KEY `fk-product_to_influence-influence` (`influence_id`);

--
-- Индексы таблицы `product_to_ingredient`
--
ALTER TABLE `product_to_ingredient`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-product_to_ingredient-product` (`product_id`),
  ADD KEY `fk-product_to_ingredient-ingredient` (`ingedient_id`);

--
-- Индексы таблицы `product_to_shop`
--
ALTER TABLE `product_to_shop`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-product_to_shop-product` (`product_id`),
  ADD KEY `fk-product_to_shop-shop` (`shop_id`);

--
-- Индексы таблицы `product_to_type`
--
ALTER TABLE `product_to_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-product_to_type-product` (`product_id`),
  ADD KEY `fk-product_to_type-type` (`type_id`);

--
-- Индексы таблицы `series`
--
ALTER TABLE `series`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`);

--
-- Индексы таблицы `series_lang`
--
ALTER TABLE `series_lang`
  ADD PRIMARY KEY (`model_id`,`language`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key`);

--
-- Индексы таблицы `settings_lang`
--
ALTER TABLE `settings_lang`
  ADD PRIMARY KEY (`model_id`,`language`);

--
-- Индексы таблицы `shop`
--
ALTER TABLE `shop`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `shop_lang`
--
ALTER TABLE `shop_lang`
  ADD PRIMARY KEY (`model_id`,`language`);

--
-- Индексы таблицы `source_message`
--
ALTER TABLE `source_message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_source_message_category` (`category`);

--
-- Индексы таблицы `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`);

--
-- Индексы таблицы `type_lang`
--
ALTER TABLE `type_lang`
  ADD PRIMARY KEY (`model_id`,`language`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `imagemanager`
--
ALTER TABLE `imagemanager`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `influence`
--
ALTER TABLE `influence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT для таблицы `ingredient`
--
ALTER TABLE `ingredient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `ingredient_images`
--
ALTER TABLE `ingredient_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT для таблицы `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `product_to_category`
--
ALTER TABLE `product_to_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `product_to_influence`
--
ALTER TABLE `product_to_influence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT для таблицы `product_to_ingredient`
--
ALTER TABLE `product_to_ingredient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT для таблицы `product_to_shop`
--
ALTER TABLE `product_to_shop`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблицы `product_to_type`
--
ALTER TABLE `product_to_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT для таблицы `series`
--
ALTER TABLE `series`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `shop`
--
ALTER TABLE `shop`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `source_message`
--
ALTER TABLE `source_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT для таблицы `type`
--
ALTER TABLE `type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `banner`
--
ALTER TABLE `banner`
  ADD CONSTRAINT `fk-banner-series` FOREIGN KEY (`series_id`) REFERENCES `series` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `banner_lang`
--
ALTER TABLE `banner_lang`
  ADD CONSTRAINT `fk-banner_lang-banner` FOREIGN KEY (`model_id`) REFERENCES `banner` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `category_lang`
--
ALTER TABLE `category_lang`
  ADD CONSTRAINT `fk-category_lang-shop` FOREIGN KEY (`model_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `influence_lang`
--
ALTER TABLE `influence_lang`
  ADD CONSTRAINT `fk-influence_lang-influence` FOREIGN KEY (`model_id`) REFERENCES `influence` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `ingredient_images`
--
ALTER TABLE `ingredient_images`
  ADD CONSTRAINT `fk-ingredient_images-ingredient` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredient` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `ingredient_lang`
--
ALTER TABLE `ingredient_lang`
  ADD CONSTRAINT `fk-ingredient_lang-ingredient` FOREIGN KEY (`model_id`) REFERENCES `ingredient` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `menu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `fk_message_source_message` FOREIGN KEY (`id`) REFERENCES `source_message` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk-product-series` FOREIGN KEY (`series_id`) REFERENCES `series` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `product_lang`
--
ALTER TABLE `product_lang`
  ADD CONSTRAINT `fk-product_lang-product` FOREIGN KEY (`model_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `product_to_category`
--
ALTER TABLE `product_to_category`
  ADD CONSTRAINT `fk-product_to_category-category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk-product_to_category-product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `product_to_influence`
--
ALTER TABLE `product_to_influence`
  ADD CONSTRAINT `fk-product_to_influence-influence` FOREIGN KEY (`influence_id`) REFERENCES `influence` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk-product_to_influence-product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `product_to_ingredient`
--
ALTER TABLE `product_to_ingredient`
  ADD CONSTRAINT `fk-product_to_ingredient-ingredient` FOREIGN KEY (`ingedient_id`) REFERENCES `ingredient` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk-product_to_ingredient-product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `product_to_shop`
--
ALTER TABLE `product_to_shop`
  ADD CONSTRAINT `fk-product_to_shop-product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk-product_to_shop-shop` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `product_to_type`
--
ALTER TABLE `product_to_type`
  ADD CONSTRAINT `fk-product_to_type-product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk-product_to_type-type` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `series_lang`
--
ALTER TABLE `series_lang`
  ADD CONSTRAINT `fk-series_lang-series` FOREIGN KEY (`model_id`) REFERENCES `series` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `settings_lang`
--
ALTER TABLE `settings_lang`
  ADD CONSTRAINT `fk-settings_lang-settings` FOREIGN KEY (`model_id`) REFERENCES `settings` (`key`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `shop_lang`
--
ALTER TABLE `shop_lang`
  ADD CONSTRAINT `fk-shop_lang-shop` FOREIGN KEY (`model_id`) REFERENCES `shop` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `type_lang`
--
ALTER TABLE `type_lang`
  ADD CONSTRAINT `fk-type_lang-shop` FOREIGN KEY (`model_id`) REFERENCES `type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
