<?php
/**
 * Created by PhpStorm.
 * User: viktor
 * Date: 05.11.18
 * Time: 16:05
 */

namespace backend\models;


use common\components\Yiit;

class Settings extends \common\models\Settings
{
    /**
     * {@inheritdoc}
     */
    public function getTitle()
    {
        return Yiit::tr('back/base', 'Settings');
    }

    public function getIndexConfig()
    {
        return [
            ['class' => 'yii\grid\SerialColumn'],
            'key',
            'label',
            'description',
            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}'],

        ];
    }

}