<?php
/**
 * Created by PhpStorm.
 * User: viktor
 * Date: 05.11.18
 * Time: 16:12
 */

namespace backend\controllers;


use backend\components\BaseController;
use backend\models\Settings;
use backend\models\SettingsSearch;

class SettingsController extends BaseController
{
    public $model = Settings::class;
    public $modelSearch = SettingsSearch::class;
    public $viewPathModule = '@backend/views/settings/';
}