<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 21.02.2019
 * Time: 22:16
 */

namespace common\components;


use common\models\Settings;
use yii\base\Component;

class CustomSettings extends Component
{
    public function get($name, $type, $description = null)
    {
        $model = \Yii::$app->cache->get('settings' . $name);
        if (!$model) {
            $settingModel = Settings::findOne(['key' => $name, 'type' => $type]);
            if (!!$settingModel) {
                \Yii::$app->cache->set('settings' . $name, $settingModel->label ?? false, 60 * 60);
                return $settingModel->label ?? '';
            } else {
                $newModel = new Settings();
                $newModel->type = $type;
                $newModel->key = $name;
                $newModel->description = $description;
                $newModel->save();
                \Yii::$app->cache->set('settings' . $name, $newModel->label ?? false, 60 * 60);
                return $settingModel->label ?? '';
            }

        }
        return $model ?? '';


    }
}
