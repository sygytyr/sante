<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05.06.2019
 * Time: 21:23
 */

namespace common\components;


class BodyClass
{
    private static $listAttr;

    public static function get(){
        return self::$listAttr ?? [];
    }

    public static function set($attrs){
        return self::$listAttr  = $attrs;
    }
}