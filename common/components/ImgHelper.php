<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 25.05.2019
 * Time: 20:50
 */

namespace common\components;


use yii\base\Component;

class ImgHelper extends Component
{
    public function get($url)
    {
        return str_replace('/admin/', '/', $url);
    }
}
