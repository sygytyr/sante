<?php


namespace common\models;


class Figures
{
    CONST FIRST_FIGURE = 1;
    CONST SECOND_FIGURE = 2;
    CONST THIRD_FIGURE = 3;

    CONST COLOR_SCHEMA_TYPE_1 = 1;
    CONST COLOR_SCHEMA_TYPE_2 = 2;
    CONST COLOR_SCHEMA_TYPE_3 = 3;
    CONST COLOR_SCHEMA_TYPE_4 = 4;
    CONST COLOR_SCHEMA_TYPE_5 = 5;
    CONST COLOR_SCHEMA_TYPE_6 = 31;
    CONST COLOR_SCHEMA_TYPE_7 = 32;
    CONST COLOR_SCHEMA_TYPE_8 = 33;
    CONST COLOR_SCHEMA_TYPE_9 = 34;
    CONST COLOR_SCHEMA_TYPE_10 = 35;
    CONST COLOR_SCHEMA_TYPE_11 = 36;



    CONST FIRST_FIGURE_BANNER = 1;
    CONST SECOND_FIGURE_BANNER = 2;
    CONST THIRD_FIGURE_BANNER = 3;
    CONST FOURS_FIGURE_BANNER = 4;
    CONST FIFTH_FIGURE_BANNER = 5;

    public static function getTypes()
    {
        return [
            self::FIRST_FIGURE => 'Type 1',
            self::SECOND_FIGURE => 'Type 2',
            self::THIRD_FIGURE => 'Type 3',
        ];
    }

    public static function getBannerTypes()
    {
        return [
            self::FIRST_FIGURE_BANNER => 'Type 1',
            self::SECOND_FIGURE_BANNER => 'Type 2',
            self::THIRD_FIGURE_BANNER => 'Type 3',
            self::FOURS_FIGURE_BANNER => 'Type 4',
            self::FIFTH_FIGURE_BANNER => 'Type 5',
        ];
    }


    public static function getSchemas()
    {
        return [
            self::COLOR_SCHEMA_TYPE_1 => 'Theme 1',
            self::COLOR_SCHEMA_TYPE_2 => 'Theme 2',
            self::COLOR_SCHEMA_TYPE_3 => 'Theme 3',
            self::COLOR_SCHEMA_TYPE_4 => 'Theme 4',
            self::COLOR_SCHEMA_TYPE_5 => 'Theme 5',
            self::COLOR_SCHEMA_TYPE_6 => 'Theme 6',
            self::COLOR_SCHEMA_TYPE_7 => 'Theme 7',
            self::COLOR_SCHEMA_TYPE_8 => 'Theme 8',
            self::COLOR_SCHEMA_TYPE_9 => 'Theme 9',
            self::COLOR_SCHEMA_TYPE_10 => 'Theme 10',
            self::COLOR_SCHEMA_TYPE_11 => 'Theme 11',
        ];
    }

    public static function getSchemasFront()
    {
        return [
            self::COLOR_SCHEMA_TYPE_1 => [
                'fon'=>'#fff',
                'clausa' => '#E1F1FA',
                'circle' => '#95E2EE'
            ],
            self::COLOR_SCHEMA_TYPE_2 => [
                'fon'=>'#F7FFEB',
                'clausa' => '#C9E79D',
                'circle' => '#C9E79D'
            ],
            self::COLOR_SCHEMA_TYPE_3 => [
                'fon'=>'#FAFAFA',
                'clausa' => '#E1EFBE',
                'circle' => '#C9E79D'
            ],
            self::COLOR_SCHEMA_TYPE_4 => [
                'fon'=>'#184128',
                'clausa' => '#555',
                'circle' => '#fff'
            ],
            self::COLOR_SCHEMA_TYPE_5 => [
                'fon'=>'#FCF3F3',
                'clausa' => '#DCF3F5',
                'circle' => '#95E2EE'
            ],
            self::COLOR_SCHEMA_TYPE_6 => [
                'fon'=>'#fff',
                'clausa' => '#FFEFA6',
                'circle' => '#FFE779'
            ],
            self::COLOR_SCHEMA_TYPE_7 => [
                'fon'=>'#FFF8FA',
                'clausa' => '#FEE2E7',
                'circle' => '#FFC1CC'
            ],
            self::COLOR_SCHEMA_TYPE_8 => [
                'fon'=>'#FCFAFF',
                'clausa' => '#EBE2FA',
                'circle' => '#CEB1FF'
            ],
            self::COLOR_SCHEMA_TYPE_9 => [
                'fon'=>'#FBF9F9',
                'clausa' => '#E5DEDE',
                'circle' => '#D1C1C1'
            ],
            self::COLOR_SCHEMA_TYPE_10 => [
                'fon'=>'#F8FFF5',
                'clausa' => '#D5F3CA',
                'circle' => '#B5E5A4'
            ],
            self::COLOR_SCHEMA_TYPE_11 => [
                'fon'=>'#F7FEFF',
                'clausa' => '#DEF7FF',
                'circle' => '#C3F1FF'
            ]
        ];
    }
}
