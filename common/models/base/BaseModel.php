<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 22.09.18
 * Time: 19:30
 */

namespace common\models\base;


use backend\models\Language;
use common\behaviors\TranslateBehavior;
use yii\helpers\ArrayHelper;

class BaseModel extends \yii\db\ActiveRecord
{
    public function getTitle()
    {
        return '';
    }

    public function getIndexConfig()
    {
        return [];
    }

    public $langModel;

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        return $this->hasMany($this->langModel, ['model_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEditLang()
    {
        if ($this->isNewRecord) {
            return new $this->langModel;
        }
        if (!$this->langModel) {
            return false;
        }

        return !!$this->langModel::findOne([
            'model_id' => $this->id,
            'language' => Language::getEditLang()
        ]) ? $this->hasOne($this->langModel,
            ['model_id' => 'id'])->andWhere(['language' => Language::getEditLang()]) : new $this->langModel;
    }

    /**
     * @param $language
     * @return \yii\db\ActiveQuery
     */
    public function hasTranslate($language)
    {
        return $this->hasOne($this->langModel, ['model_id' => 'id'])->andWhere(['language' => $language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne($this->langModel,
            ['model_id' => 'id'])->andWhere([$this->langModel::tableName().'.language' => \Yii::$app->language]);
        /*return !is_null($this->langModel::findOne([
            'model_id' => $this->id,
            'language' => \Yii::$app->language
        ])) ? $this->hasOne($this->langModel,
            ['model_id' => 'id'])->andWhere(['language' => \Yii::$app->language]) : $this->hasOne($this->langModel,
            ['model_id' => 'id'])->andWhere(['language' => Language::getDefaultLang()->locale]);*/
    }


    public function getTranslateLang()
    {
        return $this->hasOne($this->langModel,
            ['model_id' => 'id'])->andWhere(['language' => \Yii::$app->request->get('lang_id', \Yii::$app->language)]);
    }


    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'translate' => [
                'class' => TranslateBehavior::class,
            ],
        ]);
    }


}
