<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "settings_lang".
 *
 * @property string $model_id
 * @property string $language
 * @property string $label
 *
 * @property Settings $model
 */
class SettingsLang extends \common\models\base\BaseModel
{

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['model_id', 'language', 'label'], 'required'],
            [['model_id', 'label'], 'string', 'max' => 255],
            [['language'], 'string', 'max' => 4],
            [['model_id', 'language'], 'unique', 'targetAttribute' => ['model_id', 'language']],
            [
                ['model_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Settings::className(),
                'targetAttribute' => ['model_id' => 'key']
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'settings_lang';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(Settings::className(), ['key' => 'model_id']);
    }
}
