<?php

namespace common\models;

use backend\models\Language;
use common\behaviors\TranslateBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "settings".
 *
 * @property int $id
 * @property string $key
 * @property int $type
 * @property string $description
 * @property int $created_at
 * @property int $updated_at
 *
 * @property SettingsLang[] $settingsLangs
 */
class Settings extends \common\models\base\BaseModel
{

    CONST TYPE_STRING = 1;
    CONST TYPE_FILE = 2;
    CONST TYPE_TEXT = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'settings';
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'timestamp' => [
                'class' => TimestampBehavior::class,
            ]
        ]);
    }

    public function rules()
    {
        return [
            [['key', 'type'], 'required'],
            [['type', 'created_at', 'updated_at'], 'integer'],
            [['key', 'description'], 'string', 'max' => 255],
            [['key'], 'unique'],
            [['label'], 'safe'],
            [['label'], 'filter', 'filter' => function ($value) {
                return str_replace('/admin/', '/', $value);
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'type' => 'Type',
            'description' => 'Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        Yii::$app->cache->delete('settings' . $this->key);
    }
}
