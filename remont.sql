-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Фев 18 2019 г., 23:15
-- Версия сервера: 10.1.37-MariaDB
-- Версия PHP: 7.1.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `remont`
--

-- --------------------------------------------------------

--
-- Структура таблицы `article`
--

CREATE TABLE `article` (
  `id` int(11) NOT NULL,
  `published` tinyint(1) DEFAULT '1',
  `position` int(11) DEFAULT '0',
  `type_id` int(11) DEFAULT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fotos` text COLLATE utf8_unicode_ci,
  `video` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `article`
--

INSERT INTO `article` (`id`, `published`, `position`, `type_id`, `foto`, `fotos`, `video`, `alias`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 1, '/admin/uploads/slide_1.png', '', '', 'wer-wer', 1549215020, 1550527773);

-- --------------------------------------------------------

--
-- Структура таблицы `article_lang`
--

CREATE TABLE `article_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8_unicode_ci,
  `seo_keywords` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `article_lang`
--

INSERT INTO `article_lang` (`model_id`, `language`, `label`, `content`, `seo_title`, `seo_description`, `seo_keywords`) VALUES
(1, 'en', ' wer wer', '', '', '', ''),
(1, 'ru', 'Как и почему рынок IT больше всех заботитсяо комфортном рабочем месте сотрудников', '<p><strong>Представьте себе картину: вы успешный программист, собрали команду, воплотили задуманную идею в жизнь и заработали немалую сумму денег. Перед вами новая задача! Да, работать в коворкингах или же дома друг у друга возможно, но в первом случае на вас искоса смотрят, когда кто-то что-то чуть громче скажет, а во втором &ndash; мама или жена тонко намекает, что устала готовить и убирать за вашими коллегами. Что вы сделаете? Конечно же, начнете искать офис. В редких случаях вы найдете быстро помещение, где команда будет чувствовать себя комфортно. Во всех остальных, вам придется сразится с его величеством &ndash; Ремонтом!*</strong></p>\r\n\r\n<h2>Но давайте по порядку разберемся, почему так важно обеспечить комфортные условия работы, особенно в IT сфере?</h2>\r\n\r\n<p>Есть несколько решений этой задачи, давайте разбираться:</p>\r\n\r\n<h4>1. Найти офис с хорошим ремонтом.</h4>\r\n\r\n<p>Этот вариант наиболее подходящий, но редко встречающийся. Как правило, аренда таких помещений стоит на порядок дороже и вы не всегда уверенны в качестве и износе коммуникаций.</p>\r\n\r\n<h4>2. Делать ремонт самим. Огромный плюс в значительной экономии денег за работу, но также очень много опасностей.</h4>\r\n\r\n<p>Без специалистов вы не сможете правильно и объективно оценить состояние помещения и затраты на ремонт.</p>\r\n\r\n<p>Подбор стройматериалов для дома и для офиса &ndash; разные вещи. Как правило, для рабочих помещений нужны более износостойкие материалы.</p>\r\n\r\n<p>Практически всегда, ремонт занимает больше времени, чем предполагалось, особенно когда делаешь его сам.</p>\r\n\r\n<p><img alt=\"\" src=\"/images/remont-v-ofise-1.jpg\" /></p>\r\n\r\n<p><em>Офис Facebook в Кэмбридже. Библиотека и место, где можно поиграть в настольные игры</em></p>\r\n', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('AdminAccess', '1', 1548100479);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('/*', 2, NULL, NULL, NULL, 1548100475, 1548100475),
('AdminAccess', 1, NULL, NULL, NULL, 1548100476, 1548100476);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('AdminAccess', '/*');

-- --------------------------------------------------------

--
-- Структура таблицы `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `footer_menu`
--

CREATE TABLE `footer_menu` (
  `id` int(11) NOT NULL,
  `type_link` int(11) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `root` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rgt` int(11) NOT NULL,
  `lvl` smallint(5) NOT NULL,
  `name` varchar(60) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `icon_type` smallint(1) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `selected` tinyint(1) NOT NULL DEFAULT '0',
  `disabled` tinyint(1) NOT NULL DEFAULT '0',
  `readonly` tinyint(1) NOT NULL DEFAULT '0',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `collapsed` tinyint(1) NOT NULL DEFAULT '0',
  `movable_u` tinyint(1) NOT NULL DEFAULT '1',
  `movable_d` tinyint(1) NOT NULL DEFAULT '1',
  `movable_l` tinyint(1) NOT NULL DEFAULT '1',
  `movable_r` tinyint(1) NOT NULL DEFAULT '1',
  `removable` tinyint(1) NOT NULL DEFAULT '1',
  `removable_all` tinyint(1) NOT NULL DEFAULT '0',
  `child_allowed` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `footer_menu_lang`
--

CREATE TABLE `footer_menu_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `imagemanager`
--

CREATE TABLE `imagemanager` (
  `id` int(10) UNSIGNED NOT NULL,
  `fileName` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `fileHash` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  `createdBy` int(10) UNSIGNED DEFAULT NULL,
  `modifiedBy` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `imagemanager`
--

INSERT INTO `imagemanager` (`id`, `fileName`, `fileHash`, `created`, `modified`, `createdBy`, `modifiedBy`) VALUES
(1, 'BCA7F65D-4C81-43DD-8C08-A613D4685098.jpg', 'nkNqcY_gE6AjK99MibowEC4uZYfYZeqF', '2019-02-03 20:11:12', '2019-02-03 20:11:12', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `language`
--

CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `short_label` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `default` int(1) DEFAULT '0',
  `active` int(1) DEFAULT '1',
  `sort_order` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `language`
--

INSERT INTO `language` (`id`, `short_label`, `label`, `locale`, `default`, `active`, `sort_order`) VALUES
(1, 'Eng', 'English', 'en', 0, 0, 2),
(2, 'Рус', 'Russian', 'ru', 1, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `main_menu`
--

CREATE TABLE `main_menu` (
  `id` int(11) NOT NULL,
  `type_link` int(11) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `root` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rgt` int(11) NOT NULL,
  `lvl` smallint(5) NOT NULL,
  `name` varchar(60) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `icon_type` smallint(1) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `selected` tinyint(1) NOT NULL DEFAULT '0',
  `disabled` tinyint(1) NOT NULL DEFAULT '0',
  `readonly` tinyint(1) NOT NULL DEFAULT '0',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `collapsed` tinyint(1) NOT NULL DEFAULT '0',
  `movable_u` tinyint(1) NOT NULL DEFAULT '1',
  `movable_d` tinyint(1) NOT NULL DEFAULT '1',
  `movable_l` tinyint(1) NOT NULL DEFAULT '1',
  `movable_r` tinyint(1) NOT NULL DEFAULT '1',
  `removable` tinyint(1) NOT NULL DEFAULT '1',
  `removable_all` tinyint(1) NOT NULL DEFAULT '0',
  `child_allowed` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `main_menu_lang`
--

CREATE TABLE `main_menu_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `route` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `data` blob
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `translation` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `message`
--

INSERT INTO `message` (`id`, `language`, `translation`) VALUES
(1, 'en', 'Services'),
(2, 'en', 'Service categories'),
(3, 'en', 'Projects'),
(4, 'en', 'Static pages'),
(5, 'en', 'ID'),
(6, 'en', 'Published'),
(7, 'en', 'Position'),
(8, 'en', 'Foto'),
(9, 'en', 'Alias'),
(10, 'en', 'Created At'),
(11, 'en', 'Updated At'),
(12, 'en', 'Create Service Category'),
(13, 'en', 'Save'),
(14, 'en', 'Model ID'),
(15, 'en', 'Language'),
(16, 'en', 'Label'),
(17, 'en', 'Content'),
(18, 'en', 'Seo Title'),
(19, 'en', 'Seo Description'),
(20, 'en', 'Seo Keywords'),
(21, 'en', 'Update Service Category: {name}'),
(22, 'en', 'Update'),
(23, 'en', 'Service category'),
(24, 'en', 'Create'),
(25, 'en', 'Service'),
(26, 'en', 'Category ID'),
(27, 'en', 'Video'),
(28, 'en', 'Fotos'),
(29, 'en', 'Category'),
(30, 'en', 'Project'),
(31, 'en', 'Service ID'),
(32, 'en', 'Posts'),
(33, 'en', 'Article'),
(34, 'en', 'Foto post'),
(35, 'en', 'Video post'),
(36, 'en', 'Type ID'),
(37, 'en', 'Static page'),
(38, 'en', 'Main menu'),
(39, 'en', 'Move Up'),
(40, 'en', 'Move Down'),
(41, 'en', 'Move Left'),
(42, 'en', 'Move Right'),
(43, 'en', 'Type Link'),
(44, 'en', 'Root'),
(45, 'en', 'Lft'),
(46, 'en', 'Rgt'),
(47, 'en', 'Lvl'),
(48, 'en', 'Name'),
(49, 'en', 'Icon'),
(50, 'en', 'Icon Type'),
(51, 'en', 'Active'),
(52, 'en', 'Selected'),
(53, 'en', 'Disabled'),
(54, 'en', 'Readonly'),
(55, 'en', 'Visible'),
(56, 'en', 'Collapsed'),
(57, 'en', 'Movable U'),
(58, 'en', 'Movable D'),
(59, 'en', 'Movable L'),
(60, 'en', 'Movable R'),
(61, 'en', 'Removable'),
(62, 'en', 'Removable All'),
(63, 'en', 'Square'),
(63, 'ru', 'Square'),
(64, 'en', 'Count employers'),
(64, 'ru', 'Count employers'),
(65, 'en', 'Count places'),
(65, 'ru', 'Count places'),
(66, 'en', 'Address'),
(66, 'ru', 'Address'),
(67, 'en', 'Reviews'),
(68, 'en', 'Author'),
(69, 'en', 'Author Position');

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1548100467),
('m130524_201442_init', 1548100470),
('m180823_113212_add_foto_to_uset_table', 1548100470),
('m181021_163944_create_menu_table', 1548100471),
('m181021_164043_create_user', 1548100471),
('m181021_164412_rbac_init', 1548100472),
('m181021_164519_rbac_add_index_on_auth_assignment_user_id', 1548100473),
('m181021_164636_create_ImageManager_table', 1548100474),
('m181021_164707_addBlameableBehavior', 1548100475),
('m181021_165251_create_user_role_add_user', 1548100479),
('m181021_181615_create_translation_table', 1548100480),
('m181021_182507_create_language_table', 1548100481),
('m181105_111430_create_settings_table', 1548100482),
('m181105_135139_insert_default_language', 1548100482),
('m190202_113052_create_service_category_tables', 1549108190),
('m190202_161452_create_service_tables', 1549124326),
('m190202_171124_create_project_table', 1549127686),
('m190203_171038_create_article_table', 1549214549),
('m190203_173848_create_static_page_table', 1549215625),
('m190203_183248_main_menu_table', 1549219216),
('m190203_184118_footer_menu_table', 1549219318),
('m190203_190220_add_column_to_menu', 1549220646),
('m190218_195921_create_review_table', 1550520762);

-- --------------------------------------------------------

--
-- Структура таблицы `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `published` tinyint(1) DEFAULT '1',
  `position` int(11) DEFAULT '0',
  `service_id` int(11) DEFAULT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `project`
--

INSERT INTO `project` (`id`, `published`, `position`, `service_id`, `foto`, `video`, `created_at`, `updated_at`) VALUES
(1, 1, 0, NULL, '', NULL, 1549213787, 1549213787);

-- --------------------------------------------------------

--
-- Структура таблицы `project_lang`
--

CREATE TABLE `project_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `project_lang`
--

INSERT INTO `project_lang` (`model_id`, `language`, `label`, `content`) VALUES
(1, 'en', 'werwe 1', '');

-- --------------------------------------------------------

--
-- Структура таблицы `review`
--

CREATE TABLE `review` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author_position` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `published` tinyint(1) DEFAULT '1',
  `position` int(11) DEFAULT '0',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `review`
--

INSERT INTO `review` (`id`, `label`, `content`, `foto`, `author`, `author_position`, `published`, `position`, `created_at`, `updated_at`) VALUES
(1, 'СО ВКУСОМ И СТОИТ РАЗУМНЫХ ДЕНЕГ', '<p>Когда настало время переводить удаленную команду в живой офис, столкнулся с тем, что понятия не имею, как это должно выглядить, чтобы было продуктивно, комфортно, без простоя времени и по карману развивающемуся стартапу. Оказалось, это вполне реально. Спасибо СтартОфису, всего 6 дней и команда в сборе</p>\r\n', '/admin/uploads/news_img.jpg', 'Александр Антонюк', 'Предпрениматель', 1, 0, 1550523318, 1550523318),
(2, 'СО ВКУСОМ И СТОИТ РАЗУМНЫХ ДЕНЕГ', '<p>Когда настало время переводить удаленную команду в живой офис, столкнулся с тем, что понятия не имею, как это должно выглядить, чтобы было продуктивно, комфортно, без простоя времени и по карману развивающемуся стартапу. Оказалось, это вполне реально. Спасибо СтартОфису, всего 6 дней и команда в сборе</p>\r\n', '/admin/uploads/slide_1.png', 'Александр Антонюк', 'Предпрениматель', 1, 2, 1550523353, 1550523353);

-- --------------------------------------------------------

--
-- Структура таблицы `service`
--

CREATE TABLE `service` (
  `id` int(11) NOT NULL,
  `published` tinyint(1) DEFAULT '1',
  `position` int(11) DEFAULT '0',
  `category_id` int(11) DEFAULT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fotos` text COLLATE utf8_unicode_ci,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `service`
--

INSERT INTO `service` (`id`, `published`, `position`, `category_id`, `foto`, `video`, `fotos`, `alias`, `created_at`, `updated_at`) VALUES
(1, 1, 0, NULL, '', '', '', 'ssd-sdfs-dfsdf', 1549126755, 1549211149);

-- --------------------------------------------------------

--
-- Структура таблицы `service_category`
--

CREATE TABLE `service_category` (
  `id` int(11) NOT NULL,
  `published` tinyint(1) DEFAULT '1',
  `position` int(11) DEFAULT '0',
  `foto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `service_category`
--

INSERT INTO `service_category` (`id`, `published`, `position`, `foto`, `alias`, `created_at`, `updated_at`) VALUES
(3, 1, 0, '/admin/uploads/remont-v-ofise-1.jpg', 'keyptaun', 1550519182, 1550519182),
(4, 1, 0, '/admin/uploads/remont-v-ofise-1.jpg', 'keyptaun-4', 1550519208, 1550519209);

-- --------------------------------------------------------

--
-- Структура таблицы `service_category_lang`
--

CREATE TABLE `service_category_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8_unicode_ci,
  `seo_keywords` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `service_category_lang`
--

INSERT INTO `service_category_lang` (`model_id`, `language`, `label`, `content`, `seo_title`, `seo_description`, `seo_keywords`) VALUES
(3, 'ru', 'Кейптаун', '<p>Эффектный, светлый интерьер позволит сосредоточиться на работе, добавить энергии и контраста. Клиенты оценят Вашу смелость и решительность!</p>\r\n', '', '', ''),
(4, 'ru', 'Кейптаун', '<p>Эффектный, светлый интерьер позволит сосредоточиться на работе, добавить энергии и контраста. Клиенты оценят Вашу смелость и решительность!</p>\r\n', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `service_lang`
--

CREATE TABLE `service_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8_unicode_ci,
  `seo_keywords` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `service_lang`
--

INSERT INTO `service_lang` (`model_id`, `language`, `label`, `content`, `seo_title`, `seo_description`, `seo_keywords`) VALUES
(1, 'en', 'ssd sdfs dfsdf', '', '1', '2', '3');

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `settings_lang`
--

CREATE TABLE `settings_lang` (
  `model_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `source_message`
--

CREATE TABLE `source_message` (
  `id` int(11) NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `source_message`
--

INSERT INTO `source_message` (`id`, `category`, `message`) VALUES
(1, 'back/base', 'Services'),
(2, 'back/base', 'Service categories'),
(3, 'back/base', 'Projects'),
(4, 'back/base', 'Static pages'),
(5, 'back/base', 'ID'),
(6, 'back/base', 'Published'),
(7, 'back/base', 'Position'),
(8, 'back/base', 'Foto'),
(9, 'back/base', 'Alias'),
(10, 'back/base', 'Created At'),
(11, 'back/base', 'Updated At'),
(12, 'back/base', 'Create Service Category'),
(13, 'back/base', 'Save'),
(14, 'back/base', 'Model ID'),
(15, 'back/base', 'Language'),
(16, 'back/base', 'Label'),
(17, 'back/base', 'Content'),
(18, 'back/base', 'Seo Title'),
(19, 'back/base', 'Seo Description'),
(20, 'back/base', 'Seo Keywords'),
(21, 'back/base', 'Update Service Category: {name}'),
(22, 'back/base', 'Update'),
(23, 'back/base', 'Service category'),
(24, 'back/base', 'Create'),
(25, 'back/base', 'Service'),
(26, 'back/base', 'Category ID'),
(27, 'back/base', 'Video'),
(28, 'back/base', 'Fotos'),
(29, 'back/base', 'Category'),
(30, 'back/base', 'Project'),
(31, 'back/base', 'Service ID'),
(32, 'back/base', 'Posts'),
(33, 'back/base', 'Article'),
(34, 'back/base', 'Foto post'),
(35, 'back/base', 'Video post'),
(36, 'back/base', 'Type ID'),
(37, 'back/base', 'Static page'),
(38, 'back/base', 'Main menu'),
(39, 'kvtree', 'Move Up'),
(40, 'kvtree', 'Move Down'),
(41, 'kvtree', 'Move Left'),
(42, 'kvtree', 'Move Right'),
(43, 'back/base', 'Type Link'),
(44, 'back/base', 'Root'),
(45, 'back/base', 'Lft'),
(46, 'back/base', 'Rgt'),
(47, 'back/base', 'Lvl'),
(48, 'back/base', 'Name'),
(49, 'back/base', 'Icon'),
(50, 'back/base', 'Icon Type'),
(51, 'back/base', 'Active'),
(52, 'back/base', 'Selected'),
(53, 'back/base', 'Disabled'),
(54, 'back/base', 'Readonly'),
(55, 'back/base', 'Visible'),
(56, 'back/base', 'Collapsed'),
(57, 'back/base', 'Movable U'),
(58, 'back/base', 'Movable D'),
(59, 'back/base', 'Movable L'),
(60, 'back/base', 'Movable R'),
(61, 'back/base', 'Removable'),
(62, 'back/base', 'Removable All'),
(63, 'front/calc', 'Square'),
(64, 'front/calc', 'Count employers'),
(65, 'front/calc', 'Count places'),
(66, 'front/calc', 'Address'),
(67, 'back/base', 'Reviews'),
(68, 'back/base', 'Author'),
(69, 'back/base', 'Author Position');

-- --------------------------------------------------------

--
-- Структура таблицы `static_page`
--

CREATE TABLE `static_page` (
  `id` int(11) NOT NULL,
  `published` tinyint(1) DEFAULT '1',
  `position` int(11) DEFAULT '0',
  `foto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `static_page`
--

INSERT INTO `static_page` (`id`, `published`, `position`, `foto`, `alias`, `created_at`, `updated_at`) VALUES
(1, 1, 0, '', 'e-rqwe-qwe', 1549216854, 1549216854);

-- --------------------------------------------------------

--
-- Структура таблицы `static_page_lang`
--

CREATE TABLE `static_page_lang` (
  `model_id` int(11) NOT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8_unicode_ci,
  `seo_keywords` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `static_page_lang`
--

INSERT INTO `static_page_lang` (`model_id`, `language`, `label`, `content`, `seo_title`, `seo_description`, `seo_keywords`) VALUES
(1, 'en', 'e rqwe qwe', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `foto_id` int(11) DEFAULT NULL COMMENT 'Foto id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `foto_id`) VALUES
(1, 'admin', 'xjke69O3nSRCGtGJw78bCOJv1nbz96gh', '$2y$13$DvzwtShp7bGz0WoHd90yQ.9zHD0izIHpqpb2cEgYq6Zhse8zXwhr6', NULL, 'sygytyr@gmail.com', 10, 1548100479, 1549217478, 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`);

--
-- Индексы таблицы `article_lang`
--
ALTER TABLE `article_lang`
  ADD PRIMARY KEY (`model_id`,`language`);

--
-- Индексы таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD KEY `auth_assignment_user_id_idx` (`user_id`);

--
-- Индексы таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Индексы таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Индексы таблицы `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Индексы таблицы `footer_menu`
--
ALTER TABLE `footer_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tree_NK1` (`root`),
  ADD KEY `tree_NK2` (`lft`),
  ADD KEY `tree_NK3` (`rgt`),
  ADD KEY `tree_NK4` (`lvl`),
  ADD KEY `tree_NK5` (`active`);

--
-- Индексы таблицы `footer_menu_lang`
--
ALTER TABLE `footer_menu_lang`
  ADD PRIMARY KEY (`model_id`,`language`);

--
-- Индексы таблицы `imagemanager`
--
ALTER TABLE `imagemanager`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `main_menu`
--
ALTER TABLE `main_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tree_NK1` (`root`),
  ADD KEY `tree_NK2` (`lft`),
  ADD KEY `tree_NK3` (`rgt`),
  ADD KEY `tree_NK4` (`lvl`),
  ADD KEY `tree_NK5` (`active`);

--
-- Индексы таблицы `main_menu_lang`
--
ALTER TABLE `main_menu_lang`
  ADD PRIMARY KEY (`model_id`,`language`);

--
-- Индексы таблицы `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`);

--
-- Индексы таблицы `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_message_language` (`language`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-project-service` (`service_id`);

--
-- Индексы таблицы `project_lang`
--
ALTER TABLE `project_lang`
  ADD PRIMARY KEY (`model_id`,`language`);

--
-- Индексы таблицы `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`),
  ADD KEY `fk-service-service_category` (`category_id`);

--
-- Индексы таблицы `service_category`
--
ALTER TABLE `service_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`);

--
-- Индексы таблицы `service_category_lang`
--
ALTER TABLE `service_category_lang`
  ADD PRIMARY KEY (`model_id`,`language`);

--
-- Индексы таблицы `service_lang`
--
ALTER TABLE `service_lang`
  ADD PRIMARY KEY (`model_id`,`language`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key`);

--
-- Индексы таблицы `settings_lang`
--
ALTER TABLE `settings_lang`
  ADD PRIMARY KEY (`model_id`,`language`);

--
-- Индексы таблицы `source_message`
--
ALTER TABLE `source_message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_source_message_category` (`category`);

--
-- Индексы таблицы `static_page`
--
ALTER TABLE `static_page`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`);

--
-- Индексы таблицы `static_page_lang`
--
ALTER TABLE `static_page_lang`
  ADD PRIMARY KEY (`model_id`,`language`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `article`
--
ALTER TABLE `article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `footer_menu`
--
ALTER TABLE `footer_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `imagemanager`
--
ALTER TABLE `imagemanager`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `main_menu`
--
ALTER TABLE `main_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `review`
--
ALTER TABLE `review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `service`
--
ALTER TABLE `service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `service_category`
--
ALTER TABLE `service_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `source_message`
--
ALTER TABLE `source_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT для таблицы `static_page`
--
ALTER TABLE `static_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `article_lang`
--
ALTER TABLE `article_lang`
  ADD CONSTRAINT `fk-article_lang-article` FOREIGN KEY (`model_id`) REFERENCES `article` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `footer_menu_lang`
--
ALTER TABLE `footer_menu_lang`
  ADD CONSTRAINT `fk-footer_menu_lang-footer_menu` FOREIGN KEY (`model_id`) REFERENCES `footer_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `main_menu_lang`
--
ALTER TABLE `main_menu_lang`
  ADD CONSTRAINT `fk-main_menu_lang-main_menu` FOREIGN KEY (`model_id`) REFERENCES `main_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `menu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `fk_message_source_message` FOREIGN KEY (`id`) REFERENCES `source_message` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `fk-project-service` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `project_lang`
--
ALTER TABLE `project_lang`
  ADD CONSTRAINT `fk-project_lang-project` FOREIGN KEY (`model_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `service`
--
ALTER TABLE `service`
  ADD CONSTRAINT `fk-service-service_category` FOREIGN KEY (`category_id`) REFERENCES `service_category` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `service_category_lang`
--
ALTER TABLE `service_category_lang`
  ADD CONSTRAINT `fk-service_category_lang-service_category` FOREIGN KEY (`model_id`) REFERENCES `service_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `service_lang`
--
ALTER TABLE `service_lang`
  ADD CONSTRAINT `fk-service_lang-service` FOREIGN KEY (`model_id`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `settings_lang`
--
ALTER TABLE `settings_lang`
  ADD CONSTRAINT `fk-settings_lang-settings` FOREIGN KEY (`model_id`) REFERENCES `settings` (`key`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `static_page_lang`
--
ALTER TABLE `static_page_lang`
  ADD CONSTRAINT `fk-static_page_lang-static_page` FOREIGN KEY (`model_id`) REFERENCES `static_page` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
