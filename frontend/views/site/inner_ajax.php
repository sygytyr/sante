<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05.06.2019
 * Time: 20:56
 */
?>
<?php foreach ($products as $product) { ?>
    <a href="<?=\yii\helpers\Url::to(['site/product', 'alias'=>$product->alias])?>" class="product_item detectVisibility">
        <div class="proditem_wrap">
            <div class="proditem_img cntr_back" style="background-image: url('<?=Yii::$app->img->get($product->image)?>');"></div>
            <div class="proditem_info">
                <span class="lt"><?=$product->lang->label ?? ''?></span>
                <span class="gt"><?=$product->lang->volume ?? ''?></span>
                <span class="watch_this ease"><span><?=\common\components\Yiit::tr('frontent/product', 'Details')?></span></span>
            </div>
        </div>
    </a>
<?php } ?>
<?php if(!count($products)) { ?>
                        <div class="nodata_message">
                            <span><?=\common\components\Yiit::tr('frontent/product', 'No products')?></span>
                            <span><?=\common\components\Yiit::tr('frontent/product', 'We add them soon')?></span>
                        </div>
                    <?php } ?>
