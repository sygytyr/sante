<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 20.05.2019
 * Time: 22:05
 */
use common\components\Yiit;
use common\models\Settings;
?>
<div class="other_content">
    <div class="innerPage philosophy_page">
        <div class="c_layer back_lr">
            <div class="bubble bub_g">
                <svg width="343px" height="393px" viewBox="0 0 343 393" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g transform="translate(-153.000000, -2672.000000)" fill="#DBEDF9">
                            <g transform="translate(0.000000, 2200.000000)">
                                <path d="M197.928711,540.710802 C158.428711,619.710802 136.928711,678.210802 167.428711,747.210802 C197.928711,816.210802 255.928711,878.710802 323.928711,861.210802 C391.928711,843.710802 500.928711,725.210802 494.928711,668.710802 C488.928711,612.210802 509.428711,525.710802 404.428711,492.210802 C299.428711,458.710802 237.428711,461.710802 197.928711,540.710802 Z"></path>
                            </g>
                        </g>
                    </g>
                </svg>
            </div>
            <div class="bubble bub_h">
                <svg width="343px" height="393px" viewBox="0 0 343 393" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g transform="translate(-1030.000000, -2288.000000)" fill="#FFDCDD">
                            <g transform="translate(0.000000, 2200.000000)">
                                <path d="M1074.92871,156.710802 C1035.42871,235.710802 1013.92871,294.210802 1044.42871,363.210802 C1074.92871,432.210802 1132.92871,494.710802 1200.92871,477.210802 C1268.92871,459.710802 1377.92871,341.210802 1371.92871,284.710802 C1365.92871,228.210802 1386.42871,141.710802 1281.42871,108.210802 C1176.42871,74.710802 1114.42871,77.710802 1074.92871,156.710802 Z"></path>
                            </g>
                        </g>
                    </g>
                </svg>
            </div>
        </div>
        <div class="c_layer">
            <div class="some_wrap">
                <div class="phil_main">
                    <div class="row_align">
                        <div class="follow_info ">
                            <span class="main_title preLine detectVisibility"><span><?=Yiit::tr('frontend/philosophy', 'Main title')?></span></span>
                            <span class="main_subtitle preLine detectVisibility"><?=Yiit::tr('frontend/philosophy', 'Main subtitle')?></span>
                        </div>
                        <div class="follow_poster leafs_branch detectVisibility easeIn2">
                            <div class="cntr_back" style="background-image: url('<?=Yii::$app->img->get(Yii::$app->settings->get('philosophy_image', Settings::TYPE_FILE))?>');"></div>
                        </div>
                    </div>
                </div>
                <div class="phil_descrbox">
                    <span class="main_subtitle detectVisibility ease"><?=Yiit::tr('frontend/philosophy', 'Subtitle 2')?></span>
                    <span class="main_subtitle detectVisibility ease"><?=Yiit::tr('frontend/philosophy', 'Subtitle 3')?></span>
                </div>
                <div class="quote_box">
                    <span class="icon-quotes"></span>
                    <span class="main_title large_title preLine detectVisibility"><span><?=Yiit::tr('frontend/philosophy', 'Main title 2')?></span></span>
                    <span class="main_subtitle detectVisibility ease"><?=Yiit::tr('frontend/philosophy', 'Author')?></span>
                </div>
                <div class="phil_descrbox">
                    <span class="main_subtitle detectVisibility ease"><?=Yiit::tr('frontend/philosophy', 'Subtitle 4')?></span>
                    <span class="main_subtitle detectVisibility ease"><?=Yiit::tr('frontend/philosophy', 'Subtitle 5')?></span>
                    <span class="main_subtitle detectVisibility ease"><?=Yiit::tr('frontend/philosophy', 'Subtitle 6')?></span>
                </div>
                <span class="lea_branch"></span>
            </div>

            <?=$this->render('footer')?>
        </div>
    </div>

</div>
