<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29.05.2019
 * Time: 20:36
 */
?>
<div class="some_wrap">
    <div class="next_prod_item" onClick="openSeries(this, <?=$nextSereis->id?>, '<?=$nextSereis->alias?>');">
        <div class="circle_link_holder">
            <span class="show_item circle_anim">
                <span><?=\common\components\Yiit::tr('frontend/series', 'Next series')?></span>
                <svg width="56px" height="56px" viewBox="0 0 56 56" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <circle cx="27.5" cy="27.5" r="27"></circle>
                </svg>
            </span>
        </div>
        <span class="main_title"><span><?=$nextSereis->lang->label ?? ''?></span></span>
    </div>
</div>
