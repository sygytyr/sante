<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 04.02.2019
 * Time: 20:30
 */
use common\components\Yiit;
?>


            <?=$this->render('components/slider', ['banners'=>$banners])?>

            <div class="other_content">
                <?=$this->render('components/ingredients')?>


                <div class="new_products">
                    <div class="c_layer back_lr">
                        <div class="bubble bub_a">
                            <svg width="343px" height="393px" viewBox="0 0 343 393" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g transform="translate(-153.000000, -2672.000000)" fill="#DEE3D1">
                                        <g transform="translate(0.000000, 2200.000000)">
                                            <path d="M197.928711,540.710802 C158.428711,619.710802 136.928711,678.210802 167.428711,747.210802 C197.928711,816.210802 255.928711,878.710802 323.928711,861.210802 C391.928711,843.710802 500.928711,725.210802 494.928711,668.710802 C488.928711,612.210802 509.428711,525.710802 404.428711,492.210802 C299.428711,458.710802 237.428711,461.710802 197.928711,540.710802 Z"></path>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </div>
                        <div class="bubble bub_b">
                            <svg width="343px" height="393px" viewBox="0 0 343 393" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g transform="translate(-1030.000000, -2288.000000)" fill="#D6D1E3">
                                        <g transform="translate(0.000000, 2200.000000)">
                                            <path d="M1074.92871,156.710802 C1035.42871,235.710802 1013.92871,294.210802 1044.42871,363.210802 C1074.92871,432.210802 1132.92871,494.710802 1200.92871,477.210802 C1268.92871,459.710802 1377.92871,341.210802 1371.92871,284.710802 C1365.92871,228.210802 1386.42871,141.710802 1281.42871,108.210802 C1176.42871,74.710802 1114.42871,77.710802 1074.92871,156.710802 Z"></path>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </div>
                    </div>
                    <div class="c_layer">
                        <div class="some_wrap">
                            <span class="main_title olive_branch cntr_pos preLine detectVisibility"><span><?=Yiit::tr('frontend/main_page', 'New product')?></span></span>
                            <span class="main_subtitle cntr_pos preLine detectVisibility"><?=Yiit::tr('frontend/main_page', 'Open new product')?></span>
                            <div class="new_prods_box">

                                <div class="swiper-container detectVisibility" id="new_swp">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide spcl_slide">
                                            <span class="flip_title detectVisibility ease"><?=Yiit::tr('frontend/main_page', 'Trends this year')?></span>
                                        </div>

                                        <?php foreach ($products as $product) { ?>
                                            <div class="swiper-slide">
                                                <a href="<?=\yii\helpers\Url::to(['site/product', 'alias' => $product->alias])?>" class="product_item swp">
                                                    <div class="proditem_wrap">
                                                        <div class="proditem_img cntr_back" style="background-image: url('<?=Yii::$app->img->get($product->image)?>');"></div>
                                                        <div class="proditem_info">
                                                            <span class="lt"><?=$product->lang->label ?? ''?></span>
                                                            <span class="gt"><?=$product->lang->volume ?? ''?></span>
                                                            <span class="watch_this ease"><span><?=\common\components\Yiit::tr('frontent/product', 'Details')?></span></span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="followus">
                    <div class="c_layer back_lr hide_mobi">
                        <div class="bubble bub_c">
                            <svg width="343px" height="393px" viewBox="0 0 343 393" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g transform="translate(-153.000000, -2672.000000)" fill="#DFF3C3">
                                        <g transform="translate(0.000000, 2200.000000)">
                                            <path d="M197.928711,540.710802 C158.428711,619.710802 136.928711,678.210802 167.428711,747.210802 C197.928711,816.210802 255.928711,878.710802 323.928711,861.210802 C391.928711,843.710802 500.928711,725.210802 494.928711,668.710802 C488.928711,612.210802 509.428711,525.710802 404.428711,492.210802 C299.428711,458.710802 237.428711,461.710802 197.928711,540.710802 Z"></path>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </div>
                        <div class="bubble bub_d">
                            <svg width="343px" height="393px" viewBox="0 0 343 393" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g transform="translate(-1030.000000, -2288.000000)" fill="#FFDCDD">
                                        <g transform="translate(0.000000, 2200.000000)">
                                            <path d="M1074.92871,156.710802 C1035.42871,235.710802 1013.92871,294.210802 1044.42871,363.210802 C1074.92871,432.210802 1132.92871,494.710802 1200.92871,477.210802 C1268.92871,459.710802 1377.92871,341.210802 1371.92871,284.710802 C1365.92871,228.210802 1386.42871,141.710802 1281.42871,108.210802 C1176.42871,74.710802 1114.42871,77.710802 1074.92871,156.710802 Z"></path>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </div>
                    </div>
                    <div class="c_layer">
                        <div class="some_wrap">
                            <div class="row_align">
                                <div class="follow_poster hide_mobi detectVisibility ease">
                                    <div class="follow_part lr_a parex_a"><div class="cntr_back" style="background-image: url('<?=Yii::$app->img->get(Yii::$app->settings->get('fly_img_a', \common\models\Settings::TYPE_FILE))?>');"></div></div>
                                    <div class="follow_part lr_b parex_b"><div class="cntr_back" style="background-image: url('<?=Yii::$app->img->get(Yii::$app->settings->get('fly_img_b', \common\models\Settings::TYPE_FILE))?>');"></div></div>
                                </div>
                                <div class="follow_info">
                                    <span class="main_title leaf_branch preLine detectVisibility"><span><?=Yiit::tr('frontend/followus', 'Join us title')?></span></span>
                                    <span class="main_subtitle preLine detectVisibility"><?=Yiit::tr('frontend/followus', 'Join us subtitle')?></span>
                                    <div class="circle_link_holder">
                                        <a href="<?=Yii::$app->settings->get('facebook_link', \common\models\Settings::TYPE_STRING)?>" class="show_item circle_anim detectVisibility" target="_blank" rel="nofollow">
                                            <span><?=Yiit::tr('frontend/followus', 'Join us facebook')?></span>
                                            <svg width="56px" height="56px" viewBox="0 0 56 56" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                <circle cx="27.5" cy="27.5" r="27"></circle>
                                            </svg>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?=$this->render('footer')?>
                    </div>
                </div>
            </div>

        </div>


