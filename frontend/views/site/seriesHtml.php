<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28.05.2019
 * Time: 21:49
 */
use common\components\Yiit;
?>

<div class="other_content" data-act="" data-thm="<?=$series->fon_color?>">
    <div class="innerPage ajax_content">
        <div class="c_layer back_lr"></div>
        <div class="c_layer">
            <div class="some_wrap">
                <div class="row_align">
                    <div class="info_block ">
                        <span class="main_subtitle preLine"><?=Yiit::tr('frontend/series', 'Series')?></span>
                        <span class="main_title large_title preLine"><span><?=$series->lang->label ?? ''?></span></span>
                        <span class="main_subtitle preLine"><?=$series->lang->content ?? ''?></span>
                        <span class="long_arrow hide_mobi"><span class="icon-long_pointer"></span></span>
                    </div>
                    <div class="poster_block hide_mobi ">
                        <div class="cntr_back" style="background-image: url('<?=Yii::$app->img->get($series->image)?>');"></div>
                    </div>
                </div>

                <div class="products_line">
                    <?php foreach ($series->products as $product) { ?>
                        <a href="<?=\yii\helpers\Url::to(['site/product', 'alias'=>$product->alias])?>" class="product_item detectVisibility">
                            <div class="proditem_wrap">
                                <div class="proditem_img cntr_back" style="background-image: url('<?=Yii::$app->img->get($product->image)?>');"></div>
                                <div class="proditem_info">
                                    <span class="lt"><?=$product->lang->label ?? ''?></span>
                                    <span class="gt"><?=$product->lang->volume ?? ''?></span>
                                    <span class="watch_this ease"><span><?=Yiit::tr('frontent/product', 'Details')?></span></span>
                                </div>
                            </div>
                        </a>
                    <?php } ?>

                </div>
            </div>
        </div>
        
    </div>
    
    <div class="next_product">
        <div class="c_layer back_lr"></div>
        <div class="c_layer">
            <div class="some_wrap">
                <div class="next_prod_item" onClick="openSeries(this, <?=$nextSereis->id?>, '<?=$nextSereis->alias?>');">
                    <div class="circle_link_holder">
                        <span class="show_item circle_anim">
                            <span><?=\common\components\Yiit::tr('frontend/series', 'Next series')?></span>
                            <svg width="56px" height="56px" viewBox="0 0 56 56" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <circle cx="27.5" cy="27.5" r="27"></circle>
                            </svg>
                        </span>
                    </div>
                    <span class="main_title"><span><?=$nextSereis->lang->label ?? ''?></span></span>
                </div>
            </div>
        </div>

        <?=$this->render('footer')?>
    </div>
</div>
