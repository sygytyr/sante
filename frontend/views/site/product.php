<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 20.05.2019
 * Time: 22:07
 */
?>
<div class="other_content">
    <div class="prodPage">
        <div class="some_wrap">
            <div class="prod_infobox">
                <div class="prod_names ">
                    <span class="main_subtitle cntr_pos preLine detectVisibility"><?=$product->series->lang->label ?? ''?></span>
                    <span class="main_title cntr_pos preLine detectVisibility"><span><?=$product->lang->label ?? ''?></span></span>
                    <span class="main_subtitle cntr_pos preLine detectVisibility"><?=$product->lang->volume ?? ''?></span>
                </div>
                <div class="prod_image ">
                    <div class="prod_image_mover ease">
                        <div class="prod_image_holder cntr_back" style="background-image: url('<?=Yii::$app->img->get($product->image)?>');"></div>
                    </div>
                </div>
                <div class="prod_information ">
                    <div class="prod_information_holder">
                        <div class="swp_descr_tags ease detectVisibility">
                            <?php if($product->lang->content ?? false) { ?>
                                <span class="swp_desr_item active" data-item="1"><?=\common\components\Yiit::tr('frontent/product', 'Content')?></span>
                            <?php } ?>

                            <?php if($product->lang->application ?? false) { ?>
                                <span class="swp_desr_item" data-item="2"><?=\common\components\Yiit::tr('frontent/product', 'Application')?></span>
                            <?php } ?>

                            <?php if($product->lang->sklad ?? false) { ?>
                                <span class="swp_desr_item" data-item="3"><?=\common\components\Yiit::tr('frontent/product', 'Composition')?></span>
                            <?php } ?>
                        </div>
                        <div class="swiper-container detectVisibility ease" id="descr_swp">
                            <div class="swiper-wrapper">
                                <?php if($product->lang->content ?? false) { ?>
                                    <div class="swiper-slide" data-idx="1">
                                        <p class="swp_descr"><?=$product->lang->content?></p>
                                    </div>
                                <?php } ?>

                                <?php if($product->lang->application ?? false) { ?>
                                    <div class="swiper-slide" data-idx="2">
                                        <p class="swp_descr"><?=$product->lang->application?></p>
                                    </div>
                                <?php } ?>

                                <?php if($product->lang->sklad ?? false) { ?>
                                    <div class="swiper-slide" data-idx="3">
                                        <p class="swp_descr"><?=$product->lang->sklad?></p>
                                    </div>
                                <?php } ?>

                            </div>
                        </div>
                        <?php if(!!$product->productToShops){ ?>

                        <div class="where_to_buy">
                            <div class="circle_link_holder">
                                <div class="show_item circle_anim switcher detectVisibility" onClick="activeToggler(this);">
                                    <div class="switch_state" data-state="1">
                                        <span><?=\common\components\Yiit::tr('frontent/product', 'Where to buy')?></span>
                                        <svg width="56px" height="56px" viewBox="0 0 56 56" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                            <circle cx="27.5" cy="27.5" r="27"></circle>
                                        </svg>
                                    </div>
                                    <div class="switch_state" data-state="2">
                                        <span><?=\common\components\Yiit::tr('frontent/product', 'Close')?></span>
                                        <span class="spcl close_it"></span>
                                    </div>
                                </div>

                                <div class="where_box">
                                    <div class="where_box_wrap">
                                        <?php foreach ($product->productToShops as $shop) { ?>
                                            <?php if ($shop->shop_id == 3) { ?>
                                                <a href="<?=$shop->link?>" class="buy_link ease like_btn" target="_blank" rel="nofollow"><span><?=\common\components\Yiit::tr('frontent/product', 'Buy in store')?> <?=$shop->shop->lang->label ?? ''?> <span class="icon-bag"></span></span></a>
                                            <?php } else { ?>
                                                <a href="<?=$shop->link?>" class="buy_link ease" target="_blank" rel="nofollow"><span><?=\common\components\Yiit::tr('frontent/product', 'Buy in store')?> <?=$shop->shop->lang->label ?? ''?> <span class="icon-bag"></span></span></a>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="another_prods">
            <div class="c_layer back_lr">
                <div class="bubble bub_e">
                    <svg width="343px" height="393px" viewBox="0 0 343 393" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g transform="translate(-153.000000, -2672.000000)" fill="#DEE5DE">
                                <g transform="translate(0.000000, 2200.000000)">
                                    <path d="M197.928711,540.710802 C158.428711,619.710802 136.928711,678.210802 167.428711,747.210802 C197.928711,816.210802 255.928711,878.710802 323.928711,861.210802 C391.928711,843.710802 500.928711,725.210802 494.928711,668.710802 C488.928711,612.210802 509.428711,525.710802 404.428711,492.210802 C299.428711,458.710802 237.428711,461.710802 197.928711,540.710802 Z"></path>
                                </g>
                            </g>
                        </g>
                    </svg>
                </div>
                <div class="bubble bub_f">
                    <svg width="343px" height="393px" viewBox="0 0 343 393" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g transform="translate(-1030.000000, -2288.000000)" fill="#FFE0E0">
                                <g transform="translate(0.000000, 2200.000000)">
                                    <path d="M1074.92871,156.710802 C1035.42871,235.710802 1013.92871,294.210802 1044.42871,363.210802 C1074.92871,432.210802 1132.92871,494.710802 1200.92871,477.210802 C1268.92871,459.710802 1377.92871,341.210802 1371.92871,284.710802 C1365.92871,228.210802 1386.42871,141.710802 1281.42871,108.210802 C1176.42871,74.710802 1114.42871,77.710802 1074.92871,156.710802 Z"></path>
                                </g>
                            </g>
                        </g>
                    </svg>
                </div>
            </div>
            <div class="c_layer">
                <?php if(!!$otherProducts) { ?>

                <div class="some_wrap ">
                    <span class="main_title cntr_pos small_title preLine detectVisibility">
                        <span><?=\common\components\Yiit::tr('frontent/product', 'Other products')?></span>
                        <div class="slider_directions hide_mobi">
                            <span class="swp_dir prev_dir icon-pointer ease" onClick="goToSlidePrev(this);"></span>
                            <span class="swp_dir next_dir icon-pointer ease" onClick="goToSlideNext(this);"></span>
                        </div>
                    </span>
                    <div class="products_line with_slider">
                        <div class="swiper-container" id="anoth_swp">
                            <div class="swiper-wrapper">
                                <?php foreach ($otherProducts as $product) {  ?>
                                    <div class="swiper-slide">
                                        <a href="<?=\yii\helpers\Url::to(['site/product', 'alias'=>$product->alias])?>" class="product_item detectVisibility">
                                            <div class="proditem_wrap">
                                                <div class="proditem_img cntr_back" style="background-image: url('<?=Yii::$app->img->get($product->image)?>');"></div>
                                                <div class="proditem_info">
                                                    <span class="lt"><?=$product->lang->label ?? ''?></span>
                                                    <span class="gt"><?=$product->lang->volume ?? ''?></span>
                                                    <span class="watch_this ease"><span><?=\common\components\Yiit::tr('frontent/product', 'Details')?></span></span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                <?php } ?>


                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>

                <?=$this->render('footer')?>
            </div>
        </div>
    </div>

</div>
