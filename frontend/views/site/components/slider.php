<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 25.05.2019
 * Time: 21:26
 */
use common\components\Yiit;
?>
<div class="slider_container_wraper">
    <div class="bannerCursor hide_mobi"><div></div><span><?=Yiit::tr('frontend/banner_main', 'Drag')?></span></div>
    <div class="slide_counter"></div>
    <div class="slider_navs hide_mobi detectHover"><span onClick="moveBack(this);"><span><?=Yiit::tr('frontend/banner_main', 'Back')?></span><span><?=Yiit::tr('frontend/banner_main', 'Back')?></span></span><span onClick="moveForward();"><span><?=Yiit::tr('frontend/banner_main', 'Forward')?></span><span><?=Yiit::tr('frontend/banner_main', 'Forward')?></span></span></div>
    <div class="slider_navs mobi"><span class="movePrev icon-long_pointer" onClick="moveBack(this);"></span><span class="moveForw icon-long_pointer" onClick="moveForward();"></span></div>
    <a href="<?=Yii::$app->settings->get('facebook_link', \common\models\Settings::TYPE_STRING)?>" class="to_facebook hide_mobi preLine detectHover" target="_blank" rel="nofollow">Facebook</a>
    <span class="scroll_to hide_mobi preLine"><span class="icon-mouse"></span><br> <?=Yiit::tr('frontend/banner_main', 'Scroll to find more')?></span>

    <div class="swiper-container" id="main_swp">
        <div class="swiper-wrapper">
            <?php foreach ($banners as $banner) { ?>
                <div class="swiper-slide" data-idx="<?=$banner->id?>" data-hash="<?=$banner->series->alias ?? 'main'?>" data-shape="<?=$banner->figure_type?>" data-slide-sheme="<?=(int)$banner->fon_color > 0 ? $banner->fon_color : \common\models\Figures::COLOR_SCHEMA_TYPE_1 ?>" data-history="#<?=$banner->series->alias ?? '#main'?>">
                    <div class="slide_wrap">
                        <span class="slide_name preLine"><?=$banner->lang->label ?? ''?></span>
                        <div class="circle_link_holder">
                            <div class="show_item circle_anim detectHover" data-swiper-parallax="-300" data-swiper-parallax-duration="600" onClick="openSeries(this, <?=$banner->id?>, '<?=$banner->series->alias ?? 'main'?>', true);">
                                <span><?=Yiit::tr('frontend/banner_main', 'Look series')?></span>
                                <svg width="56px" height="56px" viewBox="0 0 56 56" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <circle cx="27.5" cy="27.5" r="27"></circle>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
