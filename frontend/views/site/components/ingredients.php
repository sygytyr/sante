<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 25.05.2019
 * Time: 21:24
 */
use common\components\Yiit;
use backend\modules\catalog\models\Ingredient;
?>
<div class="top_ingredients">
    <div class="some_wrap">
        <span class="main_title leaf_icon preLine detectVisibility"><span><?=Yiit::tr('frontent/main_ingredients', 'Natural<br> ingredients')?></span></span>
        <span class="main_subtitle preLine detectVisibility"><?=Yiit::tr('frontent/main_ingredients', 'Details about ingredients')?></span>
        <a href="<?=\yii\helpers\Url::to(['site/ingredients'])?>" class="show_item circle_anim right_pos hide_mobi detectVisibility">
            <span><?=Yiit::tr('frontent/main_ingredients', 'Details about ingredients')?></span>
            <svg width="56px" height="56px" viewBox="0 0 56 56" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <circle cx="27.5" cy="27.5" r="27"></circle>
            </svg>
        </a>

        <div class="ingredients_box ">
            <?php foreach (Ingredient::getListMainPage() as $ing) { ?>
                <div class="ingredient_item detectVisibility easeIn2">
                    <div class="ingr_shape">
                    <?=$this->render('figures/'.($ing['template_id'] ?? 1 ), ['color'=>$ing['fon_color']] )?>
                    </div>
                    <div class="ingr_img cntr_back easeIn2" style="background-image: url('<?=Yii::$app->img->get($ing['image'])?>');"><span style="color: <?=$ing['color']?>;       "><?=$ing['label']?></span></div>
                </div>
                <br>
            <?php } ?>

        </div>
        <a href="<?=\yii\helpers\Url::to(['site/ingredients'])?>" class="show_item circle_anim mobi">
            <span><?=Yiit::tr('frontent/main_ingredients', 'Details about ingredients')?></span>
            <svg width="56px" height="56px" viewBox="0 0 56 56" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <circle cx="27.5" cy="27.5" r="27"></circle>
            </svg>
        </a>
    </div>
</div>
