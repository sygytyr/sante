<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 20.05.2019
 * Time: 22:27
 */
?>
<a href="<?=\yii\helpers\Url::to(['site/philosophy'])?>"><span><?=\common\components\Yiit::tr('frontend/menu', 'Philosophy')?></span></a>
<a href="<?=\yii\helpers\Url::to(['site/ingredients'])?>"><span><?=\common\components\Yiit::tr('frontend/menu', 'Ingredients')?></span></a>
<a href="<?=\yii\helpers\Url::to(['site/where'])?>" class="loc_icon"><span><span class="icon-location"></span><?=\common\components\Yiit::tr('frontend/menu', 'Where to buy')?></span></a>
