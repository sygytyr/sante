<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 20.05.2019
 * Time: 21:56
 */?>
<footer>
    <div class="footer_title"><span class="cntr_back"></span></div>
    <div class="ft_copy">
        <nav class="ft_nav">
            <?=$this->render('components/links')?>
        </nav>
        <a href="#" class="to_facebook" target="_blank" rel="nofollow">Facebook</a>
        <span class="copyright">© 2019 Эльфа — Группа компаний</span>
    </div>
</footer>
