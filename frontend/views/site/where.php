<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 20.05.2019
 * Time: 22:09
 */
use common\components\Yiit;
?>
<div class="other_content">
    <div class="innerPage">
        <div class="online_stores">
            <div class="c_layer back_lr"></div>
            <div class="c_layer">
                <div class="some_wrap">
                    <span class="main_title some_branch preLine detectVisibility"><span><?=Yiit::tr('frontend/where_to_buy', 'Title')?></span></span>
                    <span class="main_subtitle preLine detectVisibility"><?=Yiit::tr('frontend/where_to_buy', 'Sub title')?></span>
                    <div class="stores_box ">
                        <?php foreach ($shops as $shop) { ?>
                            <a href="<?=$shop->link?>" class="store_item detectVisibility ease" target="_blank" rel="nofollow">
                                <div class="visibHider ease">
                                    <div class="store_logo cntr_back" style="background-image: url('<?=Yii::$app->img->get($shop->logo)?>');"></div>
                                    <span class="go_to_link hide_mobi"><span><?=Yiit::tr('frontend/where_to_buy', 'Button')?> <span class="icon-direction"></span></span></span>
                                </div>
                            </a>
                        <?php } ?>


                    </div>
                </div>

                <?=$this->render('footer')?>
            </div>
        </div>
    </div>

</div>
