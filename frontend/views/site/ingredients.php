<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 20.05.2019
 * Time: 21:58
 */
use backend\modules\catalog\models\Ingredient;
use common\components\Yiit;
?>
<div class="other_content">
    <div class="innerPage ingredients_list">
        <div class="c_layer back_lr"></div>
        <div class="c_layer">
            <div class="some_wrap">
                <span class="main_title leaf_icon preLine detectVisibility"><span><?=Yiit::tr('frontent/ingredients', 'Natural<br> ingredients')?></span></span>
                <span class="main_subtitle preLine detectVisibility"><?=Yiit::tr('frontent/ingredients', 'Content')?></span>

                <div class="ingr_list">
                    <?php foreach ($ingredients as $ing) { ?>
                        <div class="ingr_itm  ease">
                            <div class="ingr_imgs">
                                <div class="imgs_layer">
                                    <?=$this->render('components/figures/'.($ing->template_id ?? 1), ['color'=>$ing->fon_color])?>
                                </div>
                                <div class="imgs_layer">
                                    <div class="imgs_holder cntr_back" style="background-image: url('<?=Yii::$app->img->get($ing->image)?>');">
                                        <?php if(!!$ing->images) { ?>
                                            <?php if(isset($ing->images[Ingredient::POSITION_TOP_LEFT])){ ?>
                                                <div class="dop_img top_left parex_a"><div style="background-image: url('<?=Yii::$app->img->get($ing->images[Ingredient::POSITION_TOP_LEFT]->image)?>');"></div></div>
                                            <?php } ?>
                                        <?php } ?>
                                        <?php if(isset($ing->images[Ingredient::POSITION_BOTTOM_RIGHT])){ ?>
                                            <div class="dop_img bottom_right parex_a"><div style="background-image: url('<?=Yii::$app->img->get($ing->images[Ingredient::POSITION_BOTTOM_RIGHT]->image)?>');"></div></div>
                                        <?php } ?>

                                        <?php if(isset($ing->images[Ingredient::POSITION_TOP_RIGHT])){ ?>
                                            <div class="dop_img top_right parex_b"><div style="background-image: url('<?=Yii::$app->img->get($ing->images[Ingredient::POSITION_TOP_RIGHT]->image)?>');"></div></div>
                                        <?php } ?>

                                        <?php if(isset($ing->images[Ingredient::POSITION_BOTTOM_LEFT])){ ?>
                                            <div class="dop_img bottom_left parex_b"><div style="background-image: url('<?=Yii::$app->img->get($ing->images[Ingredient::POSITION_BOTTOM_LEFT]->image)?>');"></div></div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                            <div class="ingr_descr">
                                <div class="ingr_descr_wrap parex_b">
                                    <span class="inner_title preLine "><?=$ing->lang->label ?? ''?></span>
                                    <span class="main_subtitle preLine "><?=$ing->lang->content ?? ''?></span>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                </div>
            </div>

            <?=$this->render('footer')?>
        </div>
    </div>

</div>
