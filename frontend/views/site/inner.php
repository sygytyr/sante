<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 20.05.2019
 * Time: 22:02
 */
?>
<div class="other_content" data-chapter="<?=$category->alias?>">
    <div class="innerPage">
        <div class="c_layer back_lr"></div>
        <div class="c_layer">
            <div class="some_wrap">
                <span class="main_title cntr_pos preLine detectVisibility"><span><?=$category->lang->seo_title ?? ($category->lang->label ?? '')?></span></span>
                <div class="filter_box ">
                    <div class="filter_wrap">
                        <?php if(!!$category->typesList) { ?>
                            <span class="filter_item ease detectVisibility" data-filter="1"><?=\common\components\Yiit::tr('frontend/category', 'Type')?> <span class="icon-arrow ease"></span></span>
                        <?php } ?>
                        <?php if(!!$category->influencesList) { ?>
                            <span class="filter_item ease detectVisibility" data-filter="2"><?=\common\components\Yiit::tr('frontend/category', 'Influence')?> <span class="icon-arrow ease"></span></span>
                        <?php } ?>
                        <?php if(!!$category->series) { ?>
                        <span class="filter_item ease detectVisibility" data-filter="3"><?=\common\components\Yiit::tr('frontend/category', 'Series')?> <span class="icon-arrow ease"></span></span>
                        <?php } ?>
                        <?php if(!!$category->typesList) { ?>
                        <div class="filter_container" data-target="1">
                            <span class="uncheck_all ease"><?=\common\components\Yiit::tr('frontend/category', 'Unselect all')?></span>
                            <div class="filter_cont_wrapper">
                                <?php foreach ($category->typesList as $type) { ?>
                                    <span class="filter_itm">
														<label class="filter_check"><input type="checkbox" name="filter_type" data-main="type" data-alias="<?=$type->alias?>" onChange="checkFilter();" <?php if (in_array($type->id, array_keys($typesId))) { ?> checked="checked" <?php } ?> ><span><?=$type->lang->label ?? ''?></span></label>
													</span>
                                <?php } ?>

                            </div>
                        </div>
                        <?php } ?>
                        <?php if(!!$category->influencesList) { ?>
                        <div class="filter_container" data-target="2">
                            <span class="uncheck_all"><?=\common\components\Yiit::tr('frontend/category', 'Unselect all')?></span>
                            <div class="filter_cont_wrapper">
                                <?php foreach ($category->influencesList as $influence) { ?>
                                    <span class="filter_itm">
														<label class="filter_check"><input type="checkbox" name="filter_influence" data-main="influence" data-alias="<?=$influence->alias?>" onChange="checkFilter();" <?php if (in_array($influence->id, array_keys($influencesId))) { ?> checked="checked" <?php } ?> ><span><?=$influence->lang->label ?? ''?></span></label>
													</span>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if(!!$category->series) { ?>
                        <div class="filter_container" data-target="3">
                            <span class="uncheck_all"><?=\common\components\Yiit::tr('frontend/category', 'Unselect all')?></span>
                            <div class="filter_cont_wrapper">
                                </span>
                                <?php foreach ($category->series as $series) { ?>
                                    <span class="filter_itm">
														<label class="filter_check"><input type="checkbox" name="filter_series" data-main="series" data-alias="<?=$series->alias?>" onChange="checkFilter();" <?php if (in_array($series->id, array_keys($seriesId))) { ?> checked="checked" <?php } ?> ><span><?=$series->lang->label ?? ''?></span></label>
													</span>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>

                    </div>
                </div>

                <div class="products_line">
                    <?php foreach ($products as $product) { ?>
                        <a href="<?=\yii\helpers\Url::to(['site/product', 'alias'=>$product->alias])?>" class="product_item detectVisibility">
                            <div class="proditem_wrap">
                                <div class="proditem_img cntr_back" style="background-image: url('<?=Yii::$app->img->get($product->image)?>');"></div>
                                <div class="proditem_info">
                                    <span class="lt"><?=$product->lang->label ?? ''?></span>
                                    <span class="gt"><?=$product->lang->volume ?? ''?></span>
                                    <span class="watch_this ease"><span><?=\common\components\Yiit::tr('frontent/product', 'Details')?></span></span>
                                </div>
                            </div>
                        </a>
                    <?php } ?>

                    <?php if(!count($products)) { ?>
                        <div class="nodata_message">
                            <span><?=\common\components\Yiit::tr('frontent/product', 'No products')?></span>
                            <span><?=\common\components\Yiit::tr('frontent/product', 'We add them soon')?></span>
                        </div>
                    <?php } ?>

                </div>
            </div>

            <?=$this->render('footer')?>
        </div>
    </div>

</div>
