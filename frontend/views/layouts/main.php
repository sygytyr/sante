<?php

/* @var $this \yii\web\View */

/* @var $content string */

use backend\modules\catalog\models\Banner;
use yii\helpers\Html;
use frontend\assets\AppAsset;
use common\components\Yiit;
use backend\modules\catalog\models\Category;
use backend\modules\catalog\models\Type;
use backend\modules\catalog\models\Series;
use backend\modules\catalog\models\Influence;

$categories = Category::find()->andWhere(['category.published' => 1])->with([
    'typesList',
    'lang',
    'influencesList',
    'series'
])->orderBy(['category.position' => SORT_DESC])->all();


/*$types = Type::getListFrontend();
$influences = Influence::getListFrontend();
$series = Series::getListFrontend();*/
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    
    <meta name="msapplication-square144x144logo" content="/ms-icon-144x144.png">
    <link rel="android-touch-icon-precomposed" sizes="192x192" href="/android-icon-57x57.png">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon-precomposed" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
    
    <?php $this->head() ?>
</head>
<body data-al="<?= Yii::$app->requestedRoute ?>" data-lang="<?= Yii::$app->language ?>" <?php foreach (\common\components\BodyClass::get() as $key => $value) { ?><?= $key ?>="<?= $value ?>" <?php } ?>
>
<?php $this->beginBody() ?>
<div id="body_mask"></div>


<div class="preloader_box ease">
    <div class="pre_logo"></div>
    <div class="aft_logo"></div>
</div>

<div class="video_pop">
    <div class="close_video"></div>
    <div class="frame_holder">

    </div>
</div>

<div class="pageContent">
    <div class="landing_wrap clear_me hideAll" data-active="1">
        <div class="screen_back ease"></div>

        <header class="sclTop">
            <div class="header_wrap">
                <a href="<?= \yii\helpers\Url::to(['site/index']) ?>" class="logo cntr_back"></a>
                <div class="menuBtn mobi"></div>
                <div class="menu_wrapper">
                    <div class="wrapper_wrapper">
                        <?php foreach ($categories as $category) { ?>
                            <?php if (!!$category->typesList || !!$category->influencesList || !!$category->series) { ?>
                                <span class="nav_item dropdown ease" data-drop="<?= $category->id ?>"><span
                                            class="icon-arrow inline ease"></span><?= $category->lang->label ?? '' ?><span
                                            class="icon-arrow ease"></span></span>            <!-- data-drop = .drop_box[data-box] -->
                            <?php } else { ?>
                                <a href="<?= \yii\helpers\Url::to(['site/inner', 'alias' => $category->alias]) ?>"
                                   class="nav_item ease"><?= $category->lang->label ?? '' ?></a>
                            <?php } ?>
                        <?php } ?>
                        <?php foreach ($categories as $category) { ?>
                            <div class="drop_box" data-box="<?= $category->id ?>">
                                <?php if (!!$category->typesList) { ?>
                                    <div class="drop_box_wrap droplist_holder">
                                        <span class="nav_cat droplist up_case active"><?= Yiit::tr('frontend/menu',
                                                'Type') ?><span class="icon-arrow mobi"></span></span>
                                        <nav class="droplist_wrap">
                                            <?php foreach ($category->typesList as $type) { ?>
                                                <a href="<?= \yii\helpers\Url::to([
                                                    'site/inner',
                                                    'alias' => $category->alias,
                                                    'type' => $type->alias
                                                ]) ?>" class="nav_itm"><?= $type->lang->label ?? '' ?></a>
                                            <?php } ?>
                                            <a href="<?= \yii\helpers\Url::to([
                                                'site/inner',
                                                'alias' => $category->alias
                                            ]) ?>" class="nav_itm"><?= Yiit::tr('frontend/menu', 'All products') ?><span
                                                        class="icon-short_pointer"></span></a>
                                        </nav>
                                    </div>
                                <?php } ?>

                                <?php if (!!$category->influencesList) { ?>
                                    <div class="drop_box_wrap droplist_holder">
                                        <span class="nav_cat droplist up_case"><?= Yiit::tr('frontend/menu',
                                                'Influence') ?> <span class="icon-arrow mobi"></span></span>
                                        <nav class="droplist_wrap">
                                            <?php foreach ($category->influencesList as $influence) { ?>
                                                <a href="<?= \yii\helpers\Url::to([
                                                    'site/inner',
                                                    'alias' => $category->alias,
                                                    'influence' => $influence->alias
                                                ]) ?>" class="nav_itm"><?= $influence->lang->label ?? '' ?></a>
                                            <?php } ?>

                                        </nav>
                                    </div>
                                <?php } ?>

                                <?php if (!!$category->series) { ?>
                                    <div class="drop_box_wrap droplist_holder">
                                        <span class="nav_cat droplist up_case"><?= Yiit::tr('frontend/menu', 'Seria') ?> <span
                                                    class="icon-arrow mobi"></span></span>
                                        <nav class="droplist_wrap">
                                            <?php foreach ($category->series as $seria) { ?>
                                                <a href="<?= \yii\helpers\Url::to([
                                                    'site/inner',
                                                    'alias' => $category->alias,
                                                    'series' => $seria->alias
                                                ]) ?>" class="nav_itm"><?= $seria->lang->label ?? '' ?></a>
                                            <?php } ?>
                                        </nav>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <?= \frontend\components\LanguageWidget::widget() ?>


                <nav class="top_nav">
                    <?= $this->render('../site/components/links') ?>
                </nav>
            </div>
        </header>
        <!--
                <div class="pagePreloader">
                    <svg width="100px"  height="100px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-ball" style="background: none;"><circle cx="50" ng-attr-cy="{{config.cy}}" ng-attr-r="{{config.radius}}" ng-attr-fill="{{config.color}}" cy="44.5448" r="13" fill="#e8b380"><animate attributeName="cy" calcMode="spline" values="23;77;23" keyTimes="0;0.5;1" dur="1" keySplines="0.45 0 0.9 0.55;0 0.45 0.55 0.9" begin="0s" repeatCount="indefinite"></animate></circle></svg>
                </div>
        -->
        <?php $banners = Banner::find()->joinWith(['lang', 'series'],
            'false')->andWhere(['banner.published' => 1])->orderBy(['position' => SORT_DESC])->all(); ?>
        <div class="back_shapes">
            <div class="back_color"></div>
            <div class="back_morph">
                <div class="morph_container">
                    <div class="mask_holder">
                        <div class="mask_wrap">
                            <?php foreach ($banners as $banner) { ?>
                                <div class="mask_img" data-target="<?= $banner->id ?>"
                                     style="background-image: url('<?= Yii::$app->img->get($banner->image) ?>');">
                                    <span></span><span></span><span></span><span></span></div>
                            <?php } ?>
                            <div class="mask_img" data-target="90"><span></span><span></span><span></span><span></span>
                            </div>
                        </div>
                    </div>
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                         x="0px" y="0px" viewBox="0 0 980 980" style="enable-background:new 0 0 980 980;"
                         xml:space="preserve">
                        <defs>
                            <path id="stage1_1" d="M106.9,324.41c30.74-90.32,155.4-156.5,246.3-195S643.09,141.12,711,247.11C821.78,420,721.87,498.8,697.5,561,679,608.3,662.42,680.73,600.3,746.91c-47.69,50.81-131.1,172.1-261,180.4S162.2,847,77.4,737.51s-1.3-322.73,29.5-413.1Z" />
                            <path id="stage1_2" d="M153.4,271.31C211.6,195.71,271.1,215.5,362,177s285.1-77.1,353,29c110.8,172.9-9.6,279.7-34,342-18.5,47.3-15.9,153.8-78,220-47.7,50.8-82.1,136.6-212,145S169.79,835.63,116,708C57,568,95.2,346.91,153.4,271.31Z" />
                            <path id="stage1_3" d="M153.4,271.31C211.6,195.71,258.93,139.42,354,166c93,26,287.1-10.1,355,96,110.8,172.9,5.4,254.7-19,317-18.5,47.3-62.9,85.8-125,152-47.7,50.8-54.1,149.6-184,158S154.32,812.77,127,677C94.22,514.13,95.2,346.91,153.4,271.31Z" />
                            <path id="stage1_4" d="M158.27,229.56C222.7,158.14,394.93,100.42,490,127c93,26,152,16.22,206,130,76,160,10.4,248.7-14,311-18.5,47.3-28.85,102.39-83,196-34.89,60.32-28.1,117.17-158,125.57S171.32,799.77,144,664c-32.78-162.87,24-284,0-324C124,306.6,133.52,257,158.27,229.56Z" />
                            <path id="stage1_5" d="M216,223c86.9-11,182.21-59.69,274-96,91-36,172,8.22,226,122,76,160,9.4,203.23-15,265.53-18.5,47.3,2,162.47-76.19,223.29C569.81,780.61,498,825,379.92,887.69c-115,61-259.87-67.92-287.2-203.69C60,521.13,80,417,98,357,109.19,319.69,137,233,216,223Z" />
                            
                            <path id="stage2_1" d="M516.5,843c82.7,0,149.6-49.3,206-89.5C805.5,694.4,860,623.6,860,502c0-57.4-22.7-105.8-47.5-151-28.2-51.3-61.6-97.5-111.5-132.5C638.3,174.4,564.4,137,482.5,137c-92.9,0-164.2,41.1-227.5,96.5C224.7,260,212.2,303.1,190,337c-33.6,51.2-70,97.9-70,165,0,53.4,12.6,96.1,33,142,24.5,55.2,73,76.9,125,109.5C348.1,797.5,429.9,843,516.5,843Z" />
                            <path id="stage2_2" d="M508.13,837.45c82.7,0,144.6-55.8,201-96,83-59.1,136.9-112.3,136.9-233.9,0-57.4-.1-125.9-24.9-171.1-28.2-51.3-94.1-71-144-106-62.7-44.1-126.7-87.9-208.6-87.9-92.9,0-146.56,12.24-209.86,67.64-30.3,26.5-48.34,104.36-70.54,138.26-33.5,51.2-53,91.9-53,159,0,53.4-7,114.2,13.4,160,24.5,55.2,101.6,45.3,153.6,78C372.23,789.45,421.53,837.45,508.13,837.45Z" />
                            <path id="stage2_3" d="M514.25,853.45c82.7,0,133.6-109.8,190-150,83-59.1,136.9-90.3,136.9-211.9,0-57.4-16.1-120.9-40.9-166.1-28.2-51.3-88.1-56-138-91-62.7-44.1-116.7-107.9-198.6-107.9-92.9,0-136.1,21.5-199.4,76.9-30.3,26.5-40.8,108.1-63,142-33.5,51.2-62,79.9-62,147,0,53.4-4.4,113.2,16,159,24.5,55.2,95,78.3,147,111C372.35,806.45,427.65,853.45,514.25,853.45Z" />
                            <path id="stage2_4" d="M523.82,841.5c82.7,0,163.6-61.8,220-102,83-59.1,57-133.4,57-255,0-57.4,45.8-105.8,21-151-28.2-51.3-76.1-80-126-115-62.7-44.1-127.1-80-209-80-92.9,0-100.7,10.6-164,66-30.3,26.5-89.8,36.1-112,70-33.5,51.2-60,142.9-60,210,0,53.4,10.6,118.2,31,164,24.5,55.2,74,92.3,126,125C377.92,817.5,437.22,841.5,523.82,841.5Z" />
                            <path id="stage2_5" d="M492.5,853.5c82.7,0,151.46-93.8,207.86-134,83-59.1,123-78.4,123-200,0-57.4,23.8-141.8-1-187-28.2-51.3-65.1-98-115-133-62.7-44.1-134.92-73-216.82-73-92.9,0-110.88.6-174.18,56-30.3,26.5-82.8,57.1-105,91-33.5,51.2-65,114.9-65,182,0,53.4,15.6,146.2,36,192,24.5,55.2,83,76.3,135,109C387.46,800.5,405.9,853.5,492.5,853.5Z" />
                            
                            <path id="stage3_1" d="M504.91,898c89.3-2.3,137.6-16.9,179.1-92.9,44.9-82.3,73.2-200.6,59-291.6s-50.3-138.5-90.5-216.9Q611,215.72,482.81,82q-120,123.15-161.4,196.4c-47.8,84.6-88.1,151.7-88.1,245.9,0,112.3,4.4,148.7,72.3,247.4C344,827.37,415.61,900.27,504.91,898Z" />
                            <path id="stage3_2" d="M498.19,897.4c86.7,10.7,118.2-46.2,159.7-122.3,44.9-82.3,64-168.26,67-260.3,3.09-93.7-27.8-150.3-68-228.7-27.7-53.9-95.4-115.7-180.8-204.8q-120,123.15-161.4,196.4c-47.8,84.6-59.8,150.2-59.8,244.4,0,112.3,2,145.25,50,255C337.19,850.9,409.53,886.46,498.19,897.4Z" />
                            <path id="stage3_3" d="M494.2,875.53C576,859,606.66,814.27,663.11,748.53c58.3-68,95.26-154.37,70.09-243-27-95-30.42-156.17-80-229-33.42-49.1-93.6-86.9-179-176-80,82.1-108.61,103.2-163.2,167.6-62.8,74.12-71.8,152.2-71.8,246.4,0,112.3-9.93,182.34,66,275C353.71,848.74,406.65,893.27,494.2,875.53Z" />
                            <path id="stage3_4" d="M497.9,884.11c81.82-16.58,131.56-12.22,188-78,58.31-68,63.29-206,60-298-4-112-62.42-147.17-112-220-33.42-49.1-48.6-107.9-134-197-80,82.1-121,125-169,206-49.54,83.59-75.32,125-88,226-14,111.42-23.44,193.72,52.49,286.39C343.9,868.7,410.35,901.85,497.9,884.11Z" />
                            <path id="stage3_5" d="M502,891.71c80,32,124.9-12.49,178-81,62-80,62.77-165.8,50-257-12.86-91.91-35.42-151.17-85-224-33.42-49.1-69-203-166-252-80,82.1-92,139-121,227C327.54,397,315.34,453.85,264.34,553.85S267,767.71,308,810.71C360.77,866.11,419,858.53,502,891.71Z" />
                            
                            <path id="stage4_1" d="M531,833.1l188.8-69.4L754.3,719l34.9-279.3L763.3,307.9,688,185.6,470.3,146.9,307.2,171,268.8,297.4,209.3,408.9l-18.5,44.8,87.5,283.2,171,96.2Z" />
                            <path id="stage4_2" d="M525.35,839.3l188.8-69.4,34.5-44.7,34.9-279.3-25.9-131.8-75.3-122.3-219.7-51.1-165.1,54.9-24.9,113.1-69,106.4-7.2,81.1,82.1,267.1,160.2,76Z" />
                            <path id="stage4_3" d="M532.7,839.3l188.8-69.4L747.3,722,784,447.3,765,314,690,201.3,470,140.7,304.9,195.6,266,306.7l-66,106-4,92L283.3,772l164,61.3Z" />
                            <path id="stage4_4" d="M532.7,841.3,712,771.4l35.3-47.3L784,449.4,765,316.1,670.3,197.6l-201-58.9L292,197.4,267.3,320.7l-68,109.9L196,506.7l88.7,262,162.7,66.7Z" />
                            <path id="stage4_5" d="M538.35,837.05l172-62.7,42-67.3,30-254.7-24.3-104.8-89.4-147-201-58.9L299,203l-33.3,120.8-68,109.9,4,69.4,72.7,276,171.3,59.3Z" />
                            
                            <path id="stage5_1" d="M492.8,845.4c90.8-2.3,182.8-20.2,225-97.5,45.6-83.7,44.4-138,30-230.5s40.9-134.8,0-214.5c-42.2-82.3-125.3-124.9-223-155.5s-176.5-6.4-218.5,68c-48.6,86-89.5,154.3-89.5,250,0,114.1,4.5,151.2,73.5,251.5C329.3,773.6,402.1,847.8,492.8,845.4Z" />
                            <path id="stage5_2" d="M490,857c90.8-2.35,182.8-49.7,225-127,45.6-83.7,91.65-128.46,88-222-3.8-97.4-32.1-123.3-73-203-42.2-82.3-100-170.52-205.2-157.6C423.18,159.88,363,167,306.3,215.4,231.17,279.53,235,371.3,235,467c0,114.1-20,163.7,49,264C323,787.7,399.3,859.35,490,857Z" />
                            <path id="stage5_3" d="M490,836c90.8-2.35,172.8-41.7,215-119,45.6-83.7,108.65-121.8,105-215.34-3.8-97.4-53.1-106-94-185.66-42.2-82.3-86-181.52-191.2-168.6C423.18,159.88,361,156,304,191c-84.18,51.69-32,168.3-32,264,0,114.1-71.47,187.38,12,276C331.81,781.76,399.3,838.35,490,836Z" />
                            <path id="stage5_4" d="M459,820c90.8-2.35,174.8-35.7,217-113,45.6-83.7,37.65-80.46,34-174-3.8-97.4,45.9-179.3,5-259-42.2-82.3-75.3-113.06-171-139.11C445.21,108,401,159,344,194c-84.18,51.69-136,97.3-136,193,0,114.1,14.53,234.38,98,323C353.81,760.76,368.3,822.35,459,820Z" />
                            <path id="stage5_5" d="M461,843c90.8-2.35,192.8-47.7,235-125,45.6-83.7,57.65-82.46,54-176-3.8-97.4,5.9-188.3-35-268-42.2-82.3-98-148.89-189-156-102.07-8-134,21-191,56-84.18,51.69-142,117.3-142,213,0,114.1,5.53,243.38,89,332C329.81,769.76,370.3,845.35,461,843Z" />
                                    
                            <path id="stage90_1" d="M675.445,867.333c185.822-11,198.353-150.512,283.178-211c124.721-88.938,233-118.994,233-301.949c0-86.306-34.036-159.183-71.378-227.142c-42.369-77.108-113.67-117.716-188.622-170.409c-94.26-66.268-235.973-78.5-359-78.5c-139.55,0-360,94.5-297.5,216.5c41.951,81.889-36.279,253.5-71,361.5c-24.618,76.574-17,205,112.927,276.37C426.237,792.68,489.623,878.333,675.445,867.333z"/>
                            <path id="stage90_2" d="M675.445,867.333C861.266,856.333,895.175,773.488,980,713c124.721-88.938,211.623-175.66,211.623-358.616c0-86.306-34.036-159.183-71.378-227.142c-42.369-77.108-113.67-117.716-188.622-170.409C837.363-109.435,709.027-177,586-177C446.45-177,193.5-55.667,256,66.333c41.951,81.889,26.388,270.333-8.333,378.333C223.049,521.24,284.406,631.63,414.333,703C523.52,762.977,489.623,878.333,675.445,867.333z"/>
                            <path id="stage90_3" d="M591,814.667c185.822-11,226.842-71.179,311.667-131.667c124.721-88.938,288.956-145.66,288.956-328.616c0-86.306-34.036-159.183-71.378-227.142C1077.875,50.135,1090.951,19.027,1016-33.667C921.74-99.935,709.027-177,586-177C446.45-177,220.167-52.333,282.667,69.667c41.951,81.889-1.945,163.667-36.667,271.667c-24.618,76.574-4.927,306.964,125,378.333C480.187,779.644,405.178,825.667,591,814.667z"/>
                            <path id="stage90_4" d="M591,814.667c185.822-11,261.842-139.512,346.667-200c124.721-88.938,198.333-97.045,198.333-280c0-86.306,21.587-139.465-15.755-207.424C1077.875,50.135,1090.951,19.027,1016-33.667C921.74-99.935,709.027-177,586-177C446.45-177,270.167-32.333,332.667,89.667c41.951,81.889-51.945,143.667-86.667,251.667c-24.618,76.574-11.594,186.964,118.333,258.333C473.52,659.644,405.178,825.667,591,814.667z"/>
                            <path id="stage90_5" d="M702.667,776.333c185.822-11,150.175-101.179,235-161.667c124.721-88.938,198.333-97.045,198.333-280c0-86.306,21.587-139.465-15.755-207.424C1077.875,50.135,1090.951,19.027,1016-33.667C921.74-99.935,709.027-177,586-177c-139.55,0-330.833,206.333-268.333,328.333c41.951,81.889-11.945,147-46.667,255c-24.618,76.574-33.26,153.63,96.667,225C476.854,691.31,516.845,787.333,702.667,776.333z"/>

                            <path id="stage92_1" d="M393.5,693.3c132,0,146.9-154.8,229-258c62.5-78.5,166.5-142,166.5-191c0-122.3-28.8-163.6-120-224c-49.5-32.8-266.9-38-329.6-38c-101.3,0-170.7,74.9-229.9,152.1C64.5,193.2,149,273.3,36,379.3C-39,449.6,74.3,530.9,137.9,593.2C195.5,649.8,308.7,693.3,393.5,693.3z"/>
                            <path id="stage92_2" d="M351.3,680.4c132,0,206.6-132.8,288.8-236c62.5-78.5,138.2-187,138.2-236c0-122.3-2-158.7-93.3-219.1c-49.5-32.8-283-7-345.7-7c-101.3,0-170.7,74.9-229.9,152.1c-45,58.7,35.2,85.2-23.3,210c-52.8,94.4-18.6,218.5,44.9,280.9C188.7,681.8,266.6,680.4,351.3,680.4z"/>
                            <path id="stage92_3" d="M396.3,681.3c132,0,151.6-165.2,233.7-268.4c62.5-78.5,136-196,136-244.9c0-122.3-15.5-144.1-106.7-204.5c-49.5-32.8-257.1,18.8-319.9,18.8c-101.4,0-148,34.3-207.2,111.5c-45,58.7-44.2,153.2-36,282C111,474,108,573,171.6,635.4C229.2,692,311.5,681.3,396.3,681.3z"/>
                            <path id="stage92_4" d="M395.2,706.2c132,0,175.2-171,257.3-274.2c62.5-78.5,70.8-187,70.8-236c0-122.3,95-201.4,3.8-261.8C677.5-98.5,410.7-68,348-68C246.7-68,202.6,42.4,143.5,119.7c-45,58.7-16.5,139.3-18,269.7c-7.5,109.6,1.6,185.9,65.2,248.3C248.3,694.2,310.4,706.2,395.2,706.2z"/>
                            <path id="stage92_5" d="M395.2,706.2c132,0,187.5-209.2,269.7-312.4c62.5-78.5,85.4-151,85.4-200c0-122.3-20-166.6-111.2-227C589.4-65.9,410.7-68,348-68C246.7-68,204.9,0.9,145.7,78.1c-45,58.7,2.8,183.1-55.6,307.9c-71.9,116.9,13.4,198.3,76.9,260.7C224.7,703.2,310.4,706.2,395.2,706.2z"/>

                        </defs>

                        <clipPath id="morph_mask">
                            <path id="main_shape" d="" style="fill: #FF33FF"/>
                        </clipPath>

                        <path id="main_shape_mobi" class="mobi"  d="" style="fill: #FF33FF"/>
                     </svg>
                </div>
            </div>
        </div>

        <div class="grids_container">

            <?= $content ?>

        </div>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
