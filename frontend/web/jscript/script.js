var domain = window.location.protocol+'//'+window.location.hostname,
    md = new MobileDetect(window.navigator.userAgent),
	mobileOrNot = (!md.mobile() && !md.tablet())? false : true,
    wH = $(window).height(),
    wW = (!mobileOrNot)? window.innerWidth : $(window).outerWidth(),
    GBL_scTop = 0,
    GBL_positions = new Object(),
    GBL_positions_show = new Object(),
    SLIDERS = new Object();



var clearClassTimer = 0;
$(window).resize(function() {
	wH = $(window).height();
	wW = (!mobileOrNot)? window.innerWidth : $(window).outerWidth();
	
    
    if(!$('body').hasClass('waDelay')) {$('body').addClass('waDelay');}
    
    clearTimeout(clearClassTimer);
    clearClassTimer = setTimeout(function() {
        $('body').removeClass('waDelay');

    }, 300);
    
	
});

var checkTimer = 0,
    savePosition = 0;
$(window).scroll(function() {
    GBL_scTop = Math.round($(document).scrollTop());

    clearTimeout(checkTimer);

//    checkTimer = setTimeout(function() {
        checkVisibilities();
//    }, 30);
});


if(wW < 1000) {$('.spcl_slide').remove();}

var changeTimer = 0;
var glSwiper = new Swiper('#main_swp', {
    slidesPerView: 'auto',
    spaceBetween: 0,
    loop: true,
    hashNavigation: {
        watchState: true,
        replaceState: true
    },
    mousewheel: false,
    on: {
        init: function() {
            var amount = $('#main_swp .swiper-slide:not(.swiper-slide-duplicate)').length,
                resAmount = (amount < 10)? '0'+amount : amount,
                getActive = $('#main_swp .swiper-slide-active').attr('data-shape'),
                theme = $('#main_swp .swiper-slide-active').attr('data-slide-sheme'),
                slds = '';

            $('.landing_wrap').attr('data-theme', theme);

            for(var s = 1; s <= amount; s++) {
                var sldT = (s < 10)? '0'+s : s;
                slds += '<span data-sld="'+s+'">'+sldT+'</span>';
            }

            $('.slide_counter').attr('data-active', 1).html('<span class="curr">'+slds+'</span>-<span class="all_of">'+resAmount+'</span>');

            morphMe(getActive);
        },
        slideChange: function () {
            clearTimeout(changeTimer);

            changeTimer = setTimeout(function() {
                var curIdx = $('#main_swp .swiper-slide-active').attr('data-idx'),
                    resCurr = (curIdx < 10)? '0'+curIdx : curIdx,
                    getActive = $('#main_swp .swiper-slide-active').attr('data-shape'),
                    theme = $('#main_swp .swiper-slide-active').attr('data-slide-sheme');

                $('.landing_wrap').attr('data-active', curIdx);
                $('.landing_wrap').attr('data-theme', theme);
            //    $('.slide_counter .curr').text(resCurr);
                $('.slide_counter').attr('data-active', curIdx);

                morphMe(getActive);
            }, 200);
        },
        touchStart: function(e) {
            checkPressed(true);
            checkPositions(e);
        },
        touchMove: function(e) {
            checkPositions(e);
            $('body').addClass('touchMoved');
            $('body').addClass('hideTip');
        },
        touchEnd: function(e) {
            checkPressed(false);
            checkPositions(e);
            $('body').removeClass('touchMoved');
        }
    },
    parallax: true,
    slideToClickedSlide: false
});

var newSwiper = new Swiper('#new_swp', {
    slidesPerView: 'auto',
    spaceBetween: 24,
    breakpoints: {
        700: {
            spaceBetween: 8
        }
    }
});

var anothSwiper = new Swiper('#anoth_swp', {
    slidesPerView: 3,
    spaceBetween: 24,
    breakpoints: {
        700: {
            spaceBetween: 8,
            slidesPerView: 'auto'
        },
        1000: {
            spaceBetween: 12,
            slidesPerView: 'auto'
        }
    }
});

var descrSwiper = new Swiper('#descr_swp', {
    slidesPerView: 1,
    effect: 'fade',
    simulateTouch: false,
    on: {
        slideChange: function () {
            var curIdx = descrSwiper.activeIndex;
            $('.swp_desr_item.active').removeClass('active');
            $('.swp_desr_item[data-item="'+(curIdx+1)+'"]').addClass('active');
        }
    }
});



// -------- document ready --------
$(document).ready(function() {
    GBL_scTop = Math.round($(document).scrollTop());
    
    if($('.other_content').attr('data-thm')) {
        $('.landing_wrap').attr('data-theme', $('.other_content').attr('data-thm'));
        $('.landing_wrap').attr('data-active', '91');
        morphMe(90);
    }

    commonFunction();

    setTimeout(function() {
        checkVisibilities();
        $('.preloader_box').addClass('active');
        $('html, body').animate({scrollTop: 0}, 100);
        
        setTimeout(function() {
            $('.preloader_box').addClass('hideMe');
            setTimeout(function() {
                $('body').addClass('startAnim');
                $('.landing_wrap').removeClass('hideAll');
                $('.preloader_box').remove();
            }, 200);
        }, 2000);
    }, 200);
});





// -------- functions --------
function checkVisibilities() {
    $('.detectVisibility').each(function() {
        if(!$(this).hasClass('thisVisible')) {
            if($(this).offset().top < Math.round($(window).scrollTop() + $(window).height() * 0.8)) {
                $(this).addClass('thisVisible');
            }
        }
        else {
            if($(this).offset().top > Math.round($(window).scrollTop() + $(window).height())) {
                $(this).removeClass('thisVisible');
            }
        }
    });
    
}

if(!mobileOrNot) {
    var scrollPos = 0;
    window.addEventListener('scroll', function () {
        if ((document.body.getBoundingClientRect()).top > scrollPos) {
            if (!$('header').hasClass('sclTop')) {
                $('header').addClass('sclTop');
            }
        }
        else {
            if ($('header').hasClass('sclTop')) {
                $('header').removeClass('sclTop active');
                $('header .nav_item.dropdown').removeClass('active');
            }
        }
        scrollPos = (document.body.getBoundingClientRect()).top;
    });
}


function commonFunction() {
    if($('.other_content .ingr_list').length > 0) {
  //      $('.ingr_list .ingr_itm:even').addClass('parex_a');
    }

    preLiner();

    var headerOver = false,
        langsOver = false,
        filterOver = false;

    $('header').on('mouseenter', function() {headerOver = true;}).on('mouseleave', function() {headerOver = false;});
    $('.langs_wrap').on('mouseenter', function() {langsOver = true;}).on('mouseleave', function() {langsOver = false;});
    $('.filter_wrap').on('mouseenter', function() {filterOver = true;}).on('mouseleave', function() {filterOver = false;});
    
    if(wW >= 1200) {
        $('.parex_a').paroller({
            factor: -0.1,
            type: 'foreground',
            direction: 'vertical'
        });
        $('.parex_b').paroller({
            factor: -0.2,
            type: 'foreground',
            direction: 'vertical'
        });
        $('.parex_c').paroller({
            factor: -0.3,
            type: 'foreground',
            direction: 'vertical'
        });
    }
    else {
        $('.parex_a:not(.follow_poster), .parex_b:not(.follow_poster), .parex_c:not(.follow_poster)').removeAttr('style');
    }
    
    var checkChapter = $('.other_content').attr('data-chapter');
    if(checkChapter) {
        $('.landing_wrap').attr('data-active', 91);
        morphMe(92);
    }

    $('a[data-fancybox]').on('click', function(e) {
        e.preventDefault();
        var link = $(this).attr('href'),
            newLink = link.replace(/watch\?v=/gi, 'embed/');

        $('.video_pop .frame_holder').html('<iframe src="'+newLink+'"></iframe>');
        $('.video_pop').fadeIn(150);
    });

    $('.close_video').on('click', function() {
        $('.video_pop').fadeOut(150);
        $('.video_pop .frame_holder').html('');
    });

    var clickTime = 0,
        outTimer = 0;
    $('.nav_item.dropdown:not(.active)').on('mouseenter click', function() {
        var isActive = $('.nav_item.dropdown.active').length,
            obj = $(this);
        clearTimeout(clickTime);
        clearTimeout(outTimer);

        if(isActive) {
            if(obj.hasClass('active')) {
                if(wW > 1000) {
                //    obj.removeClass('active');
                //    $('header').removeClass('active');
                }
                else {
                    $('header').removeAttr('data-choosed');
                    obj.removeClass('active');
                }
                
                if(!mobileOrNot) {
                    var curr = obj.attr('data-drop'),
                        getHeight = $('.drop_box[data-box="'+curr+'"]').height() + 166;
                    $('header').css('height', getHeight+'px');
                }
            }
            else {
                $('.nav_item.dropdown.active').removeClass('active');
                clickTime = setTimeout(function() {
                    obj.addClass('active');
                    $('header').attr('data-choosed', '1');
                    
                    if(!mobileOrNot) {
                        var curr = obj.attr('data-drop'),
                            getHeight = $('.drop_box[data-box="'+curr+'"]').height() + 166;
                        $('header').css('height', getHeight+'px');
                    }
                }, 350);
            }
        }
        else {
            obj.addClass('active');
            $('header').addClass('active').attr('data-choosed', '1');
            
            if(!mobileOrNot) {
                var curr = obj.attr('data-drop'),
                    getHeight = $('.drop_box[data-box="'+curr+'"]').height() + 166;
                $('header').css('height', getHeight+'px');
            }
        }
    }).on('mouseleave', function() {
        outTimer = setTimeout(function() {
            if(!headerOver) {
                $('header').removeClass('active').removeAttr('style');
                $('.nav_item.active').removeClass('active');
            }
        }, 500);
    });
    
    $('.nav_item').on('mouseenter', function() {
        if(!$(this).hasClass('dropdown')) {
            $('header').removeClass('active').removeAttr('style');
            $('.nav_item.active').removeClass('active');
        }
    });

    var clickTimer = 0;
    $('.filter_item:not(.active)').on('mouseenter click', function() {
        var isActive = $('.filter_item.active').length,
            obj = $(this);
        clearTimeout(clickTimer);

        if(isActive) {
            if(obj.hasClass('active')) {obj.removeClass('active');}
            else {
                $('.filter_item.active').removeClass('active');
                clickTimer = setTimeout(function() {
                    obj.addClass('active');
                }, 350);
            }
        }
        else {obj.addClass('active');}
    });

    $('.uncheck_all').on('click', function() {
        $('~ .filter_cont_wrapper input[type="checkbox"]', this).prop('checked', false);
        checkFilter();
    });

    $('.swp_desr_item').on('click', function() {
        var idx = parseInt($(this).attr('data-item'));

        if(idx) {descrSwiper.slideTo(idx-1);}
    });

    $('.menuBtn').on('click', function() {
        if(!$(this).hasClass('active')) {
            $(this).addClass('active');
            $('header').addClass('active');
            $('body').addClass('activated');
        }
        else {
            $(this).removeClass('active');
            $('header').removeClass('active').removeAttr('data-choosed');
            $('.nav_item').removeClass('active');
            $('body').removeClass('activated');
        }
    });

    $('.langs_wrap > span').on('mouseenter click', function() {
        $(this).addClass('active');
    });


    


    $(document).on('click', function() {
        if(!headerOver) {$('header, .nav_item.active').removeClass('active');}
        if(!langsOver) {$('.langs_wrap span').removeClass('active');}
        if(!filterOver) {$('.filter_item').removeClass('active');}
    });

    if(wW < 1000) {
        $(document).on('click', '.nav_cat:not(.active)', function() {
            $('.nav_cat').removeClass('active');
            $(this).addClass('active');
        });
    }
    
    if($('.filter_container').length > 0) {
        checkUncheckAll();
    }
    
    
    if(wW > 1000) {
        $('.slider_container_wraper').on('mousemove', function(e) {
            checkPositions(e);
        });
        
        $('.detectHover').on('mouseenter', function() {
            $('body').addClass('hoverOver');
        }).on('mouseleave', function() {
            $('body').removeClass('hoverOver clickedOver');
        }).on('click', function() {
            $('body').addClass('clickedOver');
        });
        
        $('header').on('mouseenter', function() {
            $('body').addClass('hoverHeader');
        }).on('mouseleave', function() {
            $('body').removeClass('hoverHeader');
            $('header, .nav_item.dropdown').removeClass('active');
        });
    }
    
    $('.drop_box_wrap').each(function() {
        var count = $('.nav_itm', this).length;
        
        if(count > 8) {$(this).addClass('cols');}
    });
}

function checkPressed(is) {
    if(is) {
        if(!$('body').hasClass('hoverOver')) {$('body').addClass('pressed');}
    }
    else {
        $('body').removeClass('pressed');
    }
}

function checkPositions(e) {
    var body = $('body'),
        pX = (window.Event)? e.pageX : event.clientX,
        pY = (window.Event)? e.pageY : event.clientY,
        posX = 0,
        posY = 0;
    
    if(!body.hasClass('hoverOver')) {
        posX = (body.hasClass('pressed'))? pX - 21 : pX - 27;
        posY = (body.hasClass('pressed'))? pY - 22 : pY - 28;
    }
    else {
        posX = (body.hasClass('pressed'))? pX - 21 : pX - 3;
        posY = (body.hasClass('pressed'))? pY - 22 : pY - 4;
    }
    
    $('.bannerCursor').css({'top': posY+'px', 'left': posX+'px'});
}


function activeToggler(obj) {
    if(obj) {
        $(obj).toggleClass('active');
    }
}

function moveBack(obj) {
    if(!$(obj).hasClass('disabled')) {
        glSwiper.slidePrev();
    }
}
function moveForward(obj) {
    if(!$(obj).hasClass('disabled')) {
        glSwiper.slideNext();
    }
}

function goToSlidePrev(obj) {
    if(!$(obj).hasClass('disabled')) {
        anothSwiper.slidePrev();
    }
}
function goToSlideNext(obj) {
    if(!$(obj).hasClass('disabled')) {
        anothSwiper.slideNext();
    }
}

var clickedSeries = 0;
function openSeries(obj, idx, alias, isMain) {
    if(obj && idx && alias) {
        clearTimeout(clickedSeries);
        $('html, body').animate({scrollTop: 0}, 300);
        
        var prevs = window.location.href;
        window.history.replaceState('', 'New URL: '+prevs, prevs);
        window.history.replaceState('', 'New URL: /series/'+alias, '/series/'+alias);
        
        window.onpopstate = function(event) {
            
        };
        

        clickedSeries = setTimeout(function() {
            hideBanners();

            $.ajax({
                type: "GET",
                dataType: "json",
                url: '/series/'+alias,
                success: function(data) {
                    setTimeout(function() {
                        $('.landing_wrap').attr('data-active', '9'+idx).attr('data-theme', data.schemaId);
                        morphMe(90);

                        $('.slider_container_wraper').remove();
                        var getFooter = $('footer').html();
                        $('.other_content').html('<div class="innerPage ajax_content">'+data.content+'</div>' +
                            '<div class="next_product">' +
                                '<div class="c_layer back_lr"></div>' +
                                '<div class="c_layer">'+data.footer+'</div>' +
                                '<footer>'+getFooter+'</footer>' +
                            '</div>');
                        preLiner();

                        setTimeout(function() {
                            $('.landing_wrap').removeClass('hideAll').addClass('startAnim');
                        }, 300);

                        $('body').attr('data-al', 'site/series');
                    }, 650);
                }
            });
        }, 320);
    }
}

function preLiner() {
    $('.preLine:not(.made)').each(function() {
        var dta = $(this).html().split('<br>'),
            res = '';

        for(var i = 0; i < dta.length; i++) {
            res += '<span class="prLine"><span>'+dta[i]+'</span></span>';
        }

        $(this).html(res).addClass('made');
    });
}

function hideBanners() {
    $('.landing_wrap').addClass('hideAll');

}

function checkFilter() {
    var alis = {},
        filterURL = '/category/'+$('.other_content').attr('data-chapter');
    
    $('.filter_wrap input[type="checkbox"]').each(function() {
        if($(this).is(':checked')) {
            var cat = $(this).attr('data-main'),
                alias = $(this).attr('data-alias');
            
            if(!(cat in alis)) {
                alis[cat] = new Array();
                alis[cat].push(alias);
            }
            else {alis[cat].push(alias);}
        }
    });
    
    if('type' in alis) {filterURL += '/type-'+alis['type'].join('-and-');}
    if('influence' in alis) {filterURL += '/influence-'+alis['influence'].join('-and-');}
    if('series' in alis) {filterURL += '/series-'+alis['series'].join('-and-');}
    
    $.ajax({
        type: "GET",
        dataType: "html",
        url: filterURL,
        success: function(data) {
            $('.products_line .product_item').removeClass('thisVisible');
            setTimeout(function() {
                $('.products_line').html(data);
                checkVisibilities();
                
                history.pushState({'page_alias': 'filter_page'}, '', filterURL);
                
                window.onpopstate = function(event) {
                    history.back();
                };
            }, 500);
            
            checkUncheckAll();
        }
    });
}

function checkUncheckAll() {
    $('.filter_container').each(function() {
        var checked = 0;
        
        $('input[type="checkbox"]:checked', this).each(function() {
            checked++;
        });
        
        $(this).attr('data-chkd', checked);
    });
}




var shape = document.querySelector("#main_shape"),
    mainMorph = new Object();
if(shape) {TweenLite.set(".morph_container", { autoAlpha: 1 });}

var idxs = 1,
    gbl_needle = 0;
function morphMe(needle) {
 //   if(!mobileOrNot) {
        if(needle && shape) {
            var time = 0;
            gbl_needle = needle;

            if(mainMorph[idxs]) {
                TweenLite.killTweensOf(shape);

                var targetD = $('#stage'+needle+'_1').attr('d');
                $('#main_shape').attr('data-original', targetD);

                if(!mobileOrNot) {
                    var change = new TimelineMax({ repeat: 0, delay: 0, repeatDelay: 0, id: "morphing_start", paused: false })
                        .to(shape, .5, { morphSVG: "#stage"+needle+"_1", onComplete: createMorph });
                }
                else {
                    var change = new TimelineMax({ repeat: 0, delay: 0, repeatDelay: 0, id: "morphing_start", paused: false })
                        .to(shape, .5, { morphSVG: "#stage"+needle+"_1" });
                }

                change.progress(0.001);
            }
            else {
                if(!mobileOrNot) {createMorph(true);}
                else {
                    var change = new TimelineMax({ repeat: 0, delay: 0, repeatDelay: 0, id: "morphing_start", paused: false })
                        .to(shape, .5, { morphSVG: "#stage"+needle+"_1" });

                    change.progress(0.001);
                }
            }
        }
  //  }
}

function createMorph(init) {
 //   if(!mobileOrNot) {
        if(shape && gbl_needle > 0) {
            if(init) {
                mainMorph[idxs] = new TimelineMax({ repeat: -1, delay: 0, repeatDelay: 0, id: "morphing_"+idxs, paused: false })
                    .to(shape, 1.5, { morphSVG: "#stage"+gbl_needle+"_1", ease:Power0.easeNone })
                    .to(shape, 1.5, { morphSVG: "#stage"+gbl_needle+"_2", ease:Power0.easeNone })
                    .to(shape, 1.5, { morphSVG: "#stage"+gbl_needle+"_3", ease:Power0.easeNone })
                    .to(shape, 1.5, { morphSVG: "#stage"+gbl_needle+"_4", ease:Power0.easeNone })
                    .to(shape, 1.5, { morphSVG: "#stage"+gbl_needle+"_5", ease:Power0.easeNone })
                    .to(shape, 1.5, { morphSVG: "#stage"+gbl_needle+"_1", ease:Power0.easeNone });
            }
            else {
                mainMorph[idxs] = new TimelineMax({ repeat: -1, delay: 0, repeatDelay: 0, id: "morphing_"+idxs, paused: false })
                    .to(shape, 1.5, { morphSVG: "#stage"+gbl_needle+"_2", ease:Power0.easeNone })
                    .to(shape, 1.5, { morphSVG: "#stage"+gbl_needle+"_3", ease:Power0.easeNone })
                    .to(shape, 1.5, { morphSVG: "#stage"+gbl_needle+"_4", ease:Power0.easeNone })
                    .to(shape, 1.5, { morphSVG: "#stage"+gbl_needle+"_5", ease:Power0.easeNone })
                    .to(shape, 1.5, { morphSVG: "#stage"+gbl_needle+"_1", ease:Power0.easeNone });
            }

            mainMorph[idxs].progress(0.001);
        }
 //   }
}



function getMouseButton(event) {
    var buttonPressed = "none";
    if (event) {
        if ((event.which === null) || (event.which === 'undefined') || (!event.hasOwnProperty("which"))) {
            buttonPressed = (event.button < 2) ? 'left' :
                ((event.button === 4) ? 'middle' : 'right');
        } else {
            buttonPressed = (event.which < 2) ? 'left' :
                ((event.which === 2) ? 'middle' : 'right');
        }
    }
    return buttonPressed;
}
