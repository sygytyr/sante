<?php

namespace frontend\assets;

use yii\web\AssetBundle;


/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/swiper.css',
        'css/sante_icons.css',
        'css/style.css',

    ];
    public $js = [
        'jscript/jquery_lib/jquery-lib.min.js',
        'jscript/jquery_lib/mobile-detect.min.js',
        'jscript/jquery_lib/swiper.min.js',
        'jscript/jquery_lib/jquery.paroller.min.js',
        'jscript/jquery_lib/jquery.gsap.min.js',
        'jscript/jquery_lib/TweenLite.min.js',
        'jscript/jquery_lib/TimelineMax.min.js',
        'jscript/jquery_lib/mrph.min.js',
        'jscript/script.js',

    ];
    public $depends = [

    ];
}
