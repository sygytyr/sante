<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 04.02.2019
 * Time: 20:15
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class MainAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        /*'https://fonts.googleapis.com/css?family=Montserrat:400,700&amp;subset=cyrillic" rel="stylesheet',
        'css/bootstrap.min.css',
        //'https://start-office.pro/Content/themes/base/jquery-ui.css',
        'css/reset.css',
        'css/autocomplete.css',
        'css/swiper.css',
        'css/main.css',*/
    ];
    public $js = [
        /*'js/background.js',
        'js/swiper.js',
        'js/ui.js',
        'js/scrolloverflow.js',
        'js/fullpage.js',
        'js/popper.js',
        //'js/validator.js',
        'js/inputmask.js',
        'https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/js/bootstrap.min.js',
        'js/script.js'*/

    ];
    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapPluginAsset',
    ];
}
