<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language'=>'ru',
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl' => '',
        ],
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
            'siteKey' => '6LfIEJQUAAAAAMKbSiZxbTkGlakLkPmHDBOYZnWf',
            'secret' => '6LfIEJQUAAAAAD92GVpnb29Eu4bZvJX5zdAbcMBV',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
            'cachePath' => '@frontend/runtime/cache'
        ],

        'urlManager' => [
            'class' => 'common\components\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                /*'calc' => 'site/calculator',
                'blog' => 'site/blog',
                'privacy' => 'site/privacy',
                'about' => 'site/about',
                'contact' => 'site/contact',
                'services' => 'site/services',
                'blog/<alias:[\w_\/-]+>'=>'blog/blog',
                'service/<alias:[\w_\/-]+>'=>'service/service',*/
                'product/<alias:[\w_\/-]+>'=>'site/product',
                '/' => 'site/index',
                'ingredients' => 'site/ingredients',
                'category/<alias:[\w_\/-]+>/type-<type:[\w_\/-]+>/influence-<influence:[\w_\/-]+>/series-<series:[\w_\/-]+>' => 'site/inner',
                'category/<alias:[\w_\/-]+>/type-<type:[\w_\/-]+>/influence-<influence:[\w_\/-]+>' => 'site/inner',
                'category/<alias:[\w_\/-]+>/influence-<influence:[\w_\/-]+>/series-<series:[\w_\/-]+>' => 'site/inner',
                'category/<alias:[\w_\/-]+>/type-<type:[\w_\/-]+>/series-<series:[\w_\/-]+>' => 'site/inner',
                'category/<alias:[\w_\/-]+>/type-<type:[\w_\/-]+>' => 'site/inner',
                'category/<alias:[\w_\/-]+>/influence-<influence:[\w_\/-]+>' => 'site/inner',
                'category/<alias:[\w_\/-]+>/series-<series:[\w_\/-]+>' => 'site/inner',
                'category/<alias:[\w_\/-]+>' => 'site/inner',
                'series/<alias:[\w_\/-]+>' => 'site/series',
                'philosophy' => 'site/philosophy',
                'product' => 'site/product',
                'where' => 'site/where',

            ],
        ],

    ],
    'params' => $params,
];
