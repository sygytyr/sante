<?php

namespace frontend\controllers;

use backend\modules\article\models\Article;
use backend\modules\article\models\ArticleSearch;
use backend\modules\article\models\Review;
use backend\modules\catalog\models\Banner;
use backend\modules\catalog\models\Category;
use backend\modules\catalog\models\Influence;
use backend\modules\catalog\models\Ingredient;
use backend\modules\catalog\models\Product;
use backend\modules\catalog\models\Series;
use backend\modules\catalog\models\Shop;
use backend\modules\catalog\models\Type;
use backend\modules\landing\models\News;
use backend\modules\landing\models\SecurityItem;
use backend\modules\landing\models\Team;
use backend\modules\landing\models\YoutubeVideo;
use backend\modules\service\models\Service;
use backend\modules\service\models\ServiceCategory;
use common\components\BodyClass;
use common\components\Yiit;
use common\models\Figures;
use common\models\Settings;
use frontend\components\FooterClass;
use frontend\components\HeaderClass;
use frontend\models\CalcBaseModel;
use frontend\models\CallBackForm;
use frontend\models\FurnitureStruct;
use frontend\models\MainCalc;
use frontend\models\MeetingRoomStruct;
use frontend\models\PostCalcModelView;
use Yii;
use yii\base\InvalidParamException;
use yii\data\Pagination;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function setMetaTag(array $list)
    {
        foreach ($list as $key => $value) {
            \Yii::$app->view->registerMetaTag([
                'name' => $key,
                'content' => $value
            ]);
        }
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        BodyClass::set(['data-static' => '/']);
        $title = Yiit::tr('frontend/main', 'Seo title');
        $meta = [
            'description' => Yiit::tr('frontend/main', 'Seo description'),
            'og:description' => Yiit::tr('frontend/main', 'Seo description'),
            'keywords' => Yiit::tr('frontend/main', 'Seo keywords'),
            'og:url' => Yii::$app->request->hostInfo . Url::to(['site/index']),
            'og:type' => 'website',
            'og:title' => $title,
            'og:image' => Yii::$app->settings->get('main_seo_image', Settings::TYPE_FILE)
        ];
        $this->view->title = $title;
        $this->setMetaTag($meta);
        $products = Product::find()->andWhere(['show_on_home' => 1])->joinWith(['lang', 'series'],
            false)->orderBy(['position_home' => SORT_DESC])->all();
        $banners = Banner::find()->joinWith(['lang', 'series'],
            'false')->andWhere(['banner.published' => 1])->orderBy(['position' => SORT_DESC])->all();
        return $this->render('index', ['products' => $products, 'banners' => $banners]);
    }


    public function actionIngredients()
    {
        BodyClass::set(['data-static' => 'ingredients']);
        $title = Yiit::tr('frontend/ingredients', 'Seo title');
        $meta = [
            'description' => Yiit::tr('frontend/ingredients', 'Seo description'),
            'og:description' => Yiit::tr('frontend/ingredients', 'Seo description'),
            'keywords' => Yiit::tr('frontend/ingredients', 'Seo keywords'),
            'og:url' => Yii::$app->request->hostInfo . Url::to(['site/ingredients']),
            'og:type' => 'website',
            'og:title' => $title,
            'og:image' => Yii::$app->settings->get('ingredients_seo_image', Settings::TYPE_FILE)
        ];
        $this->view->title = $title;
        $this->setMetaTag($meta);
        $ingredients = Ingredient::find()->with([
            'lang',
            'images'
        ])->andWhere(['published' => 1])->orderBy(['position' => SORT_DESC])->all();
        return $this->render('ingredients', ['ingredients' => $ingredients]);
    }

    public function actionPhilosophy()
    {
        BodyClass::set(['data-static' => 'philosophy']);
        $title = Yiit::tr('frontend/philosophy', 'Seo title');
        $meta = [
            'description' => Yiit::tr('frontend/philosophy', 'Seo description'),
            'og:description' => Yiit::tr('frontend/philosophy', 'Seo description'),
            'keywords' => Yiit::tr('frontend/philosophy', 'Seo keywords'),
            'og:url' => Yii::$app->request->hostInfo . Url::to(['site/philosophy']),
            'og:type' => 'website',
            'og:title' => $title,
            'og:image' => Yii::$app->settings->get('philosophy_seo_image', Settings::TYPE_FILE)
        ];
        $this->view->title = $title;
        $this->setMetaTag($meta);
        return $this->render('philosophy');
    }


    public function actionInner($alias, $type = '', $influence = '', $series = '', $offset = 0)
    {


        $category = Category::find()->andWhere([
            'category.alias' => $alias,
            'category.published' => 1
        ])->with(['typesList', 'lang', 'influencesList', 'series'])->one();
        if (!$category) {
            return $this->redirect('/');
        }
        BodyClass::set(['data-cat' => $category->id]);
        $typesId = [];
        if (!!$type) {
            $types = explode('-and-', $type);
            $typesId = Type::find()->andWhere(['in', 'alias', $types])->select('alias')->indexBy('id')->column();
        }

        $influencesId = [];
        if (!!$influence) {
            $influences = explode('-and-', $influence);
            $influencesId = Influence::find()->andWhere([
                'in',
                'alias',
                $influences
            ])->select('alias')->indexBy('id')->column();
        }

        $seriesId = [];
        if (!!$series) {
            $seriesList = explode('-and-', $series);
            $seriesId = Series::find()->andWhere([
                'in',
                'alias',
                $seriesList
            ])->select('alias')->indexBy('id')->column();
        }

        $title = $category->lang->seo_title ?? false;
        $title = !!$title ? $title : ($category->lang->label ?? '');


        $products = Product::find()->joinWith([
            'productToCategories',
            'productToTypes',
            'productToInfluences',
            'lang'
        ])->andWhere(['product_to_category.category_id' => $category->id])->andFilterWhere([
            'in',
            'product_to_type.type_id',
            array_keys($typesId)
        ])->andFilterWhere([
            'in',
            'product_to_influence.influence_id',
            array_keys($influencesId)
        ])->andFilterWhere([
            'in',
            'product.series_id',
            array_keys($seriesId)
        ])->andWhere(['product.published' => 1])->orderBy(['product.position' => SORT_DESC])->limit(12)->offset($offset)->all();

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('inner_ajax', ['products' => $products]);
        }

        $meta = [
            'description' => $category->lang->seo_description ?? '',
            'og:description' => $category->lang->seo_description ?? '',
            'keywords' => $category->lang->seo_keywords ?? '',
            'og:url' => Yii::$app->request->hostInfo . Url::to(['site/inner', 'alias' => $alias]),
            'og:type' => 'website',
            'og:title' => $title,
            'og:image' => Yii::$app->settings->get('main_seo_image', Settings::TYPE_FILE)
        ];
        $this->view->title = $title;
        $this->setMetaTag($meta);
        return $this->render('inner', [
            'category' => $category,
            'products' => $products,
            'influencesId' => $influencesId,
            'typesId' => $typesId,
            'seriesId' => $seriesId
        ]);


    }

    public function actionSeries($alias)
    {

        
        $series = Series::find()->andWhere(['series.alias' => $alias, 'series.published' => 1])->with([
            'lang',
            'products'
        ])->one();

        if (!$series) {
            return $this->redirect('/');
        }
        $nextSereis = Series::find()->joinWith(['banner'])->with(['lang'])->andWhere(['series.published' => 1, 'banner.published'=>1])->andWhere([
            '>',
            'series.id',
            $series->id
        ])->orderBy(['id' => SORT_ASC])->limit(1)->one();
        if (is_null($nextSereis)) {
            $nextSereis = Series::find()->joinWith(['banner'])->with(['lang'])->andWhere(['series.published' => 1, 'banner.published'=>1])->orderBy(['series.id' => SORT_ASC])->limit(1)->one();
        }
        if (Yii::$app->request->isAjax) {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => $this->renderPartial('series', ['series' => $series]),
            'schemaId' => (int)$series->fon_color > 0 ? $series->fon_color : Figures::COLOR_SCHEMA_TYPE_1,
            'footer' => $this->renderPartial('ajax_footer', ['nextSereis' => $nextSereis]),

            ];
        }
        $title = $series->lang->seo_title ?? false;
        $title = !!$title ? $title : ($series->lang->label ?? '');  
        $this->view->title = $title;
        
        $meta = [
            'description' => $series->lang->seo_description ?? '',
            'og:description' => $series->lang->seo_description ?? '',
            'keywords' => $series->lang->seo_keywords ?? '',
            'og:url' => Yii::$app->request->hostInfo . Url::to(['site/inner', 'alias' => $alias]),
            'og:type' => 'website',
            'og:title' => $title,
            'og:image' => Yii::$app->img->get($series->image)
        ];
        $this->setMetaTag($meta);
        return $this->render('seriesHtml', ['series' => $series, 'schemaId' => (int)$series->fon_color > 0 ? $series->fon_color : Figures::COLOR_SCHEMA_TYPE_1, 'nextSereis' => $nextSereis]);

    }

    public function actionProduct($alias)
    {
        $product = Product::find()->andWhere(['alias' => $alias, 'published' => 1])->with([
            'lang',
            'productToShops',
            'productToCategories'
        ])->one();
        if (!$product) {
            return $this->redirect('/');
        }
        $title = $product->lang->seo_title ?? ($product->lang->label ?? '');
        $title = !!$title ? $title : ($product->lang->label ?? '');
        $category_id = false;
        foreach ($product->productToCategories as $key => $index) {
            $category_id = $key;
            continue;
        }

        BodyClass::set(['data-cat' => $category_id]);
        $meta = [
            'description' => $product->lang->seo_description ?? '',
            'og:description' => $product->lang->seo_description ?? '',
            'keywords' => $product->lang->seo_keywords ?? '',
            'og:url' => Yii::$app->request->hostInfo . Url::to(['site/product', 'alias' => $alias]),
            'og:type' => 'website',
            'og:title' => $title,
            'og:image' => Yii::$app->img->get($product->image)
        ];


        $this->view->title = $title;
        $this->setMetaTag($meta);
        $otherProducts = Product::find()->andWhere([
            'published' => 1,
            'series_id' => $product->series_id
        ])->andWhere(['<>', 'id', $product->id])->with(['lang'])->orderBy(['position' => SORT_DESC])->all();
        return $this->render('product', ['product' => $product, 'otherProducts' => $otherProducts]);
    }


    public function actionAbout()
    {

        return $this->render('about');
    }

    public function actionWhere()
    {
        BodyClass::set(['data-static' => 'where']);
        $shops = Shop::find()->andWhere(['published' => 1])->orderBy(['position' => SORT_DESC])->all();
        $title = Yiit::tr('frontend/where', 'Seo title');
        $meta = [
            'description' => Yiit::tr('frontend/where', 'Seo description'),
            'og:description' => Yiit::tr('frontend/where', 'Seo description'),
            'keywords' => Yiit::tr('frontend/where', 'Seo keywords'),
            'og:url' => Yii::$app->request->hostInfo . Url::to(['site/where']),
            'og:type' => 'website',
            'og:title' => $title,
            'og:image' => Yii::$app->settings->get('main_seo_image', Settings::TYPE_FILE)
        ];
        $this->view->title = $title;
        $this->setMetaTag($meta);
        return $this->render('where', ['shops' => $shops]);
    }


    public function actionContact()
    {

        return $this->render('contact');
    }


}
