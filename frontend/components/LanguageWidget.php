<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 01.11.18
 * Time: 21:35
 */

namespace frontend\components;


use backend\models\Language;
use Yii;
use yii\base\Widget;

class LanguageWidget extends Widget
{
    public $type;
    /**
     *
     */
    public function init(){}

    /**
     * @return string
     */
    public function run() {
        $languages = Yii::$app->cache->get('langall');

        if(!$languages){
            $languages = Language::find()->andWhere(['active'=>1])->all();
            Yii::$app->cache->set('langall', $languages, 60*60*24);
        }
        return $this->render('language', [
            'current' => Language::getCurrent(),
            'defaultLang' =>Language::getDefaultLang()->locale,
            'langs' => $languages,
            'type'=>$this->type,
        ]);
    }

}