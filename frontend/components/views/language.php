<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 01.11.18
 * Time: 21:36
 */
?>
<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29.09.2018
 * Time: 21:15
 */

use yii\helpers\Html;

?>

<div class="langs_box">
    <div class="langs_wrap">
        <?php foreach ($langs as $lang): ?>
            <?php if ($lang->locale == $current) { ?>
                <span class="up_case ease"><?= strtoupper($lang->short_label) ?></span>

            <?php }?>
        <?php endforeach; ?>

        <?php foreach ($langs as $lang): ?>
            <?php if ($lang->locale != $current) { ?>
                <?= Html::a( strtoupper($lang->short_label), '/' . $lang->locale, ['class' => 'up_case']) ?>
            <?php } ?>
        <?php endforeach; ?>
        <span class="icon-arrow ease"></span>
    </div>
</div>